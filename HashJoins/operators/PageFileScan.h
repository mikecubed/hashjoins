/*
FILE:			pagefilescan.h
PROVIDES: 		Contains code for performing a file scan in iterator form.  File is assumed to be on local hard drive in binary page form.
C CODE BY:		Michael Henderson
CREATION DATE:	February 2008
*/
#ifndef pagefilescan_h__
#define pagefilescan_h__

#include "Operator.h"
#include "../relational/Relation.h"
#include "../relational/Tuple.h"
#include "../io/Page.h"

#include <cstdio>
#include <string>


namespace db
{

	class PageFileScan : public Operator
	{
	public:
		PageFileScan(const std::string& inName, std::shared_ptr<Relation> rel, short pageSize);
		virtual ~PageFileScan();

		virtual bool init();
		virtual std::shared_ptr<Tuple> next(std::shared_ptr<Tuple> tuple);
		virtual bool close();
	private:
		std::string inputFileName;					///< Name of input file to scan (Max filename length of 255 characters)
		FILE *inputFile;							///< File pointer for input file
		std::shared_ptr<Relation> inputRelation;	///< Schema of file being scanned
		short pageSize;								///< Size of page on disk
		std::shared_ptr<Page> page;					///< buffer for page currently being processed
	};

}
#endif // pagefilescan_h__

