/*
FILE:			pagehashtable.h
PROVIDES: 		A hash table data structure that is used for algorithms such as dynamic hash join where the
				buckets can be flushed to disk.
C CODE BY:		Michael Henderson
CREATION DATE:	January 2008
BASED ON:		unity.operators.PageHashTable.java
PROGRAM BY:     Ramon Lawrence (ramon.lawrence@ubc.ca)
*/
#ifndef pagehashtable_h__
#define pagehashtable_h__

#include "HashBucket.h"
#include "../relational/Relation.h"
#include "../relational/Tuple.h"

#include <vector>
#include <memory>
#include <string>

namespace db
{

	class PageHashTable
	{
	public:
		PageHashTable(int tableSize, short pageSize, int maxPages, std::shared_ptr<Relation> r);
		~PageHashTable();
		int getTupleIOs();
		int getPageIOs();
		void clear();
		void flush(BucketState state); ///< flush all buckets with state: state
		void close(BucketState state);
		void SHARPClose();				///< flush in memory buckets to disk and close the files for SHARP Join
		void purge(int bucket);
		std::shared_ptr<ChainedHashtable> buildHashTable(int bucketIdx, PredicateKeyType keyType, const std::vector<int>& keyIdx);
		int getHashLocation(int key);
		int getHashLocation(const std::string& key);
		int getHashLocation(std::shared_ptr<Tuple> key, const std::vector<int>& keyIdx);
		
		int getUsedPages();
		int getPageCount(int bucket);
		int getTupleCount(int bucket);
		int getProbeCount(int bucket);
		int getSearchCount(int bucket);
		int getAvoidedProbeCount(int bucket);
		std::string getFileName(int bucket);
		// int db_pagehashtable_insertArray(db_pagehashtable *pht, void *keys, db_tuple *t);
		/**
		insert the tuple (it is its own key)
		*/
		bool insert(std::shared_ptr<Tuple> key, const std::vector<int>& keyIdx);
		bool insert(std::string key, std::shared_ptr<Tuple> t);
		bool insert(int key, std::shared_ptr<Tuple> t);
		// for indirect partitioning we need to add tuples to a specific bucket
		bool insert(std::shared_ptr<Tuple> tuple, int bucket);

		std::list<std::shared_ptr<Page>> getPageList(int bucket);
		void setBucketState(int bucket, BucketState state);
		void initBucket(int bucket, BucketState state);
		
		std::vector<std::shared_ptr<Tuple>> probeHashTable(int bucket, int key);
		std::vector<std::shared_ptr<Tuple>> probeHashTable(int bucket, const std::string& key);
		std::vector<std::shared_ptr<Tuple>> probeHashTable(int bucket, std::shared_ptr<Tuple> key, const std::vector<int>& keyIdx);

		// performance var functions
		void incrementTupleIOs();
		void addTupleIOs(int numTuples);
		void incrementPageIOs();

	private:
		std::vector<std::shared_ptr<HashBucket>> buckets;
		std::size_t tupleSize;						// the length of the relation.
		int tuplesPerPage;					// # of tuples per page (page size/tuple size)
		short pageSize;						// size of page in bytes
		int usedPages;						// # of pages currently used by the hash table
		int maxPages;						// Maximum # of pages that the hash table can use
		std::shared_ptr<Relation> relation;

		// performance variables
		int tupleIOs;
		int pageIOs;

		friend class HashBucket;
	};

	
// functions
//TODO: Implement the array functions

}

#endif // pagehashtable_h__
