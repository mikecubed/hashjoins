#ifndef SHARPJOIN_H
#define SHARPJOIN_H

#include "Operator.h"
#include "../predicates/EquijoinPredicate.h"
#include "../relational/relation.h"
#include "pagehashtable.h"
#include "chainedhashtable.h"
#include "../relational/tuple.h"
#include "../io/page.h"
#include "AttributeMap.h"

#include <vector>
#include <cstdio>
#include <ctime>
#include <deque>

namespace db
{
	class SHARPJoin :
		public Operator
	{
	public:

		/**
		Designated Initializer

		SHARP Join is a multy-way join that is restricted to star-joins

		\param inputs The inputs to this join. 2 or more inputs must be provided.  The last should be the fact table.
		\param predicates The equijoin predicates for this join.  The join expects n - 1 predicates for n inputs.  Example: For A, B, and C
							the predicates should be a.a = c.a and b.b = c.b.  Join is undefined if this format is not followed.
		*/
		SHARPJoin(std::vector<std::shared_ptr<Operator>> inputs,
			std::vector<std::shared_ptr<EquijoinPredicate>> predicates,
			int bufferSize,
			int numBuckets);

		virtual ~SHARPJoin(void);

		virtual bool init();
		virtual std::shared_ptr<Tuple> next(std::shared_ptr<Tuple> tuple);
		virtual bool close();
		std::vector<int> getNumPartitions() { return numPartitions; }
		std::vector<int> getFrozenPartitions() { return frozenPartitions; }

	private:
		int factTableBucketIndex(std::vector<int> &bucketIndexes);
		std::vector<int> dimBucketsForFactIndex(int i);
		int bucketNum; // current fact bucket for processing frozen;
		int numBuckets;											///< # of hash buckets
		std::vector<std::shared_ptr<EquijoinPredicate>> preds;	///< equi-join comparison class
		std::vector<std::shared_ptr<Relation>> relations;		///< schema of input relations

		// Hash tables
		std::vector<std::shared_ptr<PageHashTable>> inputHashTables; ///< all our hash tables for our inputs
		std::vector<int> numPartitions;			///< The number of partitions for each input
		std::vector<int> curPartitionIdexes;	///< The partitions that are currently in memory for each input// assumes a max of 1 each
		std::vector<int> frozenPartitions;
		int usedPages;

		std::vector<PredicateKeyType> keyType;	///< Type of hash: int, string, etc
		std::vector<std::vector<int>> keyIdx;	///< Index of key attributes for input relations
		std::vector<std::size_t> numAttrs;		///< The number of attributes in each predicate

		// Iterator state variables
		bool processingFrozen;					///< True if current processing frozen buckets (on disk) All of probe input table must have been read.
		//int bucketNum;							///< Stores the frozen bucket number currently processing
		std::vector<int> partitionNumber;		///< the current set of partitions we are joining.
		bool processingProbe;					///< true if processing tuples created by probing build table with a probe tuple
		//std::deque<std::vector<std::shared_ptr<Tuple>>> matches;	///< stores the build table tuples that matched a probe
		std::vector<std::shared_ptr<Tuple>> matches;
		bool hasMatches;
		std::vector<int> curLoc;								///< current tuple start processing at in matches
		int curMatchVec;						///< current matches vector we are currently testing against
		std::shared_ptr<Tuple> probeTuple;		///< Current probe tuple that we are processing
		std::shared_ptr<Page> probePage;		///< Current probe page that we are processing

		// Used for processing frozen buckets
		FILE *probeFile;
		bool leftBuild;							// True if left input is build input (when processing frozen disk bucket)
		std::vector<std::shared_ptr<ChainedHashtable>> bucketHashTables;	// Hash tables for build inputs of frozen bucket

		// timing related variables
		clock_t savedTime;						// Used to time outer partition and frozen partition steps
		int insertsAvoided;

		// private functions
		void purge(int curIdx);
		bool partitionInput(std::size_t index);
		bool setupBucket();
		void cleanup();
		std::vector<std::shared_ptr<Tuple>> getCurrentMatches();
	};

}

#endif // !SHARPJOIN_H
