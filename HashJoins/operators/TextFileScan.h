/**
\file TextFileScan.cpp

\todo document this file

\author Michael Henderson
\copyright 2012 Michael Henderson
*/

#ifndef hashjoins_TextFileScan_h
#define hashjoins_TextFileScan_h

#include "Operator.h"
#include "../relational/Relation.h"
#include "../relational/Tuple.h"

#include <string>
#include <cstdio>

namespace db 
{
	/**
	Scans a text document with a specific schema and outputs tuples
	*/
	class TextFileScan : public Operator
	{
	public:
		TextFileScan(const std::string &inName, std::shared_ptr<Relation> rel, std::string separators = "\0");
		virtual ~TextFileScan();

		virtual bool init();
		virtual std::shared_ptr<Tuple> next(std::shared_ptr<Tuple> tuple);
		virtual bool hasNext();
		virtual bool close();

	private:
		std::string inFileName;						// Name of input file to scan
		FILE *inFile;								// File pointer for input file
		std::shared_ptr<Relation> inputRelation;	// Schema of file being scanned
		std::string fieldSeparators;				// characters used to separate fields
	};
}

#endif
