/*
FILE:			chainedhashtable.h
PROVIDES: 		The hash entry data structure used by the chained hash table
C CODE BY:		Michael Henderson
CREATION DATE:	January 2008
BASED ON:		unity.operators.ChainedHashTable.java
PROGRAM BY:     Ramon Lawrence (ramon.lawrence@ubc.ca)
*/
#ifndef hashentry_h__
#define hashentry_h__

namespace db
{
	template <typename K, typename D>
	class HashEntry
	{
	public:
		HashEntry(K key, D data);
		~HashEntry();

		K getKey();
		D getData();

	private:
		K key;
		D data;
	};

	template <typename K, typename D>
	HashEntry<K,D>::HashEntry(K theKey, D theData)
		:key(theKey),
		data(theData)
	{

	}

	template <typename K, typename D>
	HashEntry<K,D>::~HashEntry()
	{

	}

	template <typename K, typename D>
	K HashEntry<K,D>::getKey()
	{
		return key;
	}

	template <typename K, typename D>
	D HashEntry<K,D>::getData()
	{
		return data;
	}
}

#endif // hashentry_h__
