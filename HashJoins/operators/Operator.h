#ifndef DB_OPERATOR_H
#define DB_OPERATOR_H

#include <vector>
#include <ctime>
#include <memory>

#include "../relational/Relation.h"
#include "../relational/Tuple.h"

namespace db
{

	class Operator
	{
	public:
		Operator(const std::vector<std::shared_ptr<Operator>> &inputs, int bufferSize);
		virtual ~Operator(void);

		// iterator methods
		virtual bool init() = 0;
		virtual std::shared_ptr<Tuple> next(std::shared_ptr<Tuple> tuple) = 0; // we can pass in a tuple struct if we want
		virtual bool close() = 0;
		void setStartTime(clock_t tm);
		void setOutputRelation(std::shared_ptr<Relation> rel);
		std::shared_ptr<Relation> getOutputRelation(); 

		// Tuple and page counter methods
		void incrementTuplesOutput();
		void incrementTuplesRead();
		void incrementPagesRead();
		void incrementTupleIOs();
		void incrementPageIOs();

		void addTuplesRead(int i);
		void addPagesRead(int i);
		void addTuplesOutput(int i);
		void addTupleIOs(int i);
		void addPageIOs(int i);

		int getTuplesOutput();
		int getTuplesRead();
		int getPagesRead();
		int getTupleIOs();
		int getPageIOs();

	protected:
		std::vector<std::shared_ptr<Operator>> input; ///< input operators, pointers because polymorphism
		std::shared_ptr<Relation> outputRelation;

		// I/O related properties
		int BUFFER_SIZE;				// # of pages that can be buffered by this operation
		int tuplesOutput;				// Total # of tuples output of this operator
		int tuplesRead;					// Total # of tuples read by this operator from its inputs only
		int pagesRead;					// Total # of pages read by this operator from its inputs only
		int internalTupleIOs;			// Total # of tuples read/written by this operator for its own processing (not including read from its sources)
		int internalPageIOs;			// Total # of pages read/written by this operator for its own processing (not including read from its sources)

		clock_t startTime;			// used for tracking times/performance
	};

}

#endif
