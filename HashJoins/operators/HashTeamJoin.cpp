#include "HashTeamJoin.h"
#include "../io/FileManager.h"

#include <cstdlib>
#include <cstdio>
#include <cassert>

namespace db
{
	HashTeamJoin::HashTeamJoin(std::vector<std::shared_ptr<Operator>> inputs,
		std::vector<std::shared_ptr<EquijoinPredicate>> predicates,
		int theBufferSize,
		int theNumBuckets)
		:Operator(inputs, theBufferSize),
		numBuckets(theNumBuckets),
		preds(predicates),
		probeFile(nullptr),
		keyType(PredicateKeyTypeUnknown),
		processingFrozen(false),
		bucketNum(0),
		processingProbe(false),
		savedTime(0),
		insertsAvoided(0),
		usedPages(0)
	{

	}

	HashTeamJoin::~HashTeamJoin()
	{

	}

	bool HashTeamJoin::init()
	{
		assert(input.size() > 1);

		for (auto &in : input)
		{
			if (!in->init())
			{
				printf("failed to initialize input to hash team join");
				return false;
			}

			relations.push_back(in->getOutputRelation());
			inputHashTables.push_back(std::shared_ptr<PageHashTable>(new PageHashTable(numBuckets,DB_PAGE_SIZE_DEFAULT,BUFFER_SIZE,in->getOutputRelation())));
		}

		frozen = 0;
		usedPages = 0;

		// first grab the left side of the first predicate
		int firstNumAttrs = preds[0]->getNumAttributes();
		keyIdx.push_back(preds[0]->getRelation1Locs());

		std::shared_ptr<Relation> outputRel (new Relation(relations));
		setOutputRelation(outputRel);

		if (firstNumAttrs > 1)
		{
			keyType.push_back(PredicateKeyTypeObject);
		}
		else if (relations[0]->getAttributeType(keyIdx[0][0]) == DB_TYPE_INT)
		{
			keyType.push_back(PredicateKeyTypeInt);
		} 
		else if (relations[0]->getAttributeType(keyIdx[0][0]) == DB_TYPE_STRING)
		{
			keyType.push_back(PredicateKeyTypeString);
		}


		// and now grab the right side of all the predicates
		for (int ii = 0; ii < preds.size(); ++ii)
		{
			auto &pred = preds[ii];
			auto &rel = relations[ii+1];
			int curNumAttrs = pred->getNumAttributes();
			numAttrs.push_back(curNumAttrs);
			keyIdx.push_back(pred->getRelation2Locs());

			if (curNumAttrs > 1)
			{
				keyType.push_back(PredicateKeyTypeObject);
			}
			else if (rel->getAttributeType(keyIdx[ii+1][0]) == DB_TYPE_INT)
			{
				keyType.push_back(PredicateKeyTypeInt);
			}
			else if (rel->getAttributeType(keyIdx[ii+1][0]) == DB_TYPE_STRING)
			{
				keyType.push_back(PredicateKeyTypeString);
			}
		}

		// Build innerHashTables
		//printf("Reading and partitioning build tables.\n");

		for (std::size_t ii = 0; ii < input.size() - 1; ++ii)
		{
			clock_t tmpTime = clock();
			partitionInput(ii);

			clock_t endTime = clock();
			//printf("Finished partitioning build table %ld at %ldms\n", ii,(endTime - tmpTime));
		}

		// flush all frozen buckets
		for (std::size_t ii = 0; ii < input.size() - 1; ++ii)
		{
		
			inputHashTables[ii]->close(BucketStateFrozen);

			addTupleIOs(inputHashTables[ii]->getTupleIOs());
			addPageIOs(inputHashTables[ii]->getPageIOs());
		}

		// Initialize the buckets in the outerHashTable that are frozen
		for (int idx = 0; idx < frozen; ++idx) {
			inputHashTables.back()->initBucket(idx, BucketStateFrozen);
		}

		// Initialize iterator state variables
		processingFrozen = false;
		processingProbe = false;
		leftBuild = true; // always true?


		// alloc a tuple
		std::shared_ptr<Relation> &outer = relations.back();

		std::size_t dataSize = outer->computeTupleSize() + outer->getNumAttributes() * 2 + 4;
		char *data = new char[dataSize];

		probeTuple = std::shared_ptr<Tuple>(new Tuple(outer, data));


		// alloc a page
		probePage = std::shared_ptr<Page>(new Page(DB_PAGE_SIZE_DEFAULT, outer));

		savedTime = clock();
		insertsAvoided = 0;

		for (int ii = 0; ii < input.size() - 1; ++ ii)
		{
			curLoc.push_back(0);
		}
		

		return true;
	}


	bool HashTeamJoin::close()
	{
		//printf("Inserts avoided: %d\n", insertsAvoided);
		cleanup();
		for (auto &in : input)
		{
			in->close();
		}
		bucketHashTables.clear();
		return true;
	}

	/**
	Grabs the current tuples at the current indices and increments the indices
	*/
	std::vector<std::shared_ptr<Tuple>> HashTeamJoin::getCurrentMatches()
	{
		std::vector<std::shared_ptr<Tuple>> tuples;
		return tuples;
		//int nMatchVec = matches.size();
		//for (int ii = nMatchVec - 1; ii >= 0; --ii)
		//{
		//	tuples.push_back(matches[ii][curLoc[ii]]);
		//}
		//tuples.push_back(probeTuple);

		//for (int ii = nMatchVec - 1; ii >= 0; --ii)
		//{
		//	curLoc[ii] ++;
		//	if (matches[ii].size() -1 > curLoc[ii])
		//	{
		//		break;
		//	}
		//	else
		//	{
		//		// match
		//		curLoc[ii] = 0;
		//	}

		//	if (ii == 0)
		//	{
		//		// done condition
		//		bool done = true;

		//		for (auto &i : curLoc)
		//		{
		//			if (i != 0) {
		//				done = false;
		//				break;
		//			}

		//		}

		//		if (done)
		//			hasMatches = false; 
		//	}
		//}

		//return tuples;
	}

	std::shared_ptr<Tuple> HashTeamJoin::next(std::shared_ptr<Tuple> tuple)
	{
		while (true)
		{
outer:
			while (processingProbe)
			{
				if (hasMatches)
				{
					// Processing join tuples from a previous probe into the build tables
					std::vector<std::shared_ptr<Tuple>> tuples = matches;// getCurrentMatches();
					hasMatches = false;
					incrementTuplesOutput();
					return Tuple::join(tuples, getOutputRelation() ,tuple);
				}
				else
				{
					// we have processed all possible matches
					processingProbe = false;
					matches.clear();
				}
				
			} // end processingProbe
// outer:
			if (processingFrozen) {
				while (probePage->hasNext())
				{
					probeTuple = probePage->next(probeTuple);
					std::vector<int> probeIdx = keyIdx.back();

					for (int ii = 0; ii < input.size()-1; ++ii)
					{
						
						std::shared_ptr<ChainedHashtable> hashTable = bucketHashTables[ii];
					

						PredicateKeyType curType = keyType[ii];

						std::vector<std::shared_ptr<Tuple>> curMatches;
						if (curType == PredicateKeyTypeInt)
						{
							curMatches = hashTable->find(probeTuple->getInt(probeIdx[0]));
						}
						else if (curType == PredicateKeyTypeString)
						{
							curMatches = hashTable->find(probeTuple->getString(probeIdx[0]));
						} 
						else
						{
							curMatches = hashTable->find(probeTuple, probeIdx);
						}

						if (curMatches.size() == 0)
						{
							matches.clear();
							break;
						}
						matches.push_back(curMatches[0]);
						curLoc[ii] = 0;
					}
					
					if (matches.size() > 0)
					{
						processingProbe = true;
						hasMatches = true;
						goto outer; 
					}
				}

				// finished processing a page of probe tuples - read next one if possible
				if (probePage->read(probeFile) > 0) { 
					probePage->initIterator();
					incrementPageIOs();
					addTupleIOs(probePage->getTupleCount());
				} else {
					closeFile(probeFile);
					probeFile = nullptr;
					if (bucketNum == frozen) {// Process all frozen buckets done join!
						cleanup();// Also called in close() but done here to make sure temp files are deleted ASAP
						return NULL;
					}
					bucketNum++;// Process next bucket
					if (!setupBucket()) {
						cleanup(); // Also called in close() but done here to make sure temp files are deleted ASAP
						return NULL;
					}
				}
				// end processingFrozen
			} else {
				// Processing input tuples from probe relation
				std::shared_ptr<Tuple> tmp(nullptr);
				int probeRel = input.size() - 1;
				while ((tmp = input[probeRel]->next(probeTuple))) {
					int bucket;
					incrementTupleIOs();

					PredicateKeyType curType = keyType.back();
					std::shared_ptr<PageHashTable> &outerHashTable = inputHashTables.back();
					if (curType == PredicateKeyTypeInt) {
						bucket = outerHashTable->getHashLocation(probeTuple->getInt(keyIdx.back()[0]));
					} else if (curType == PredicateKeyTypeString) {
						bucket = outerHashTable->getHashLocation(probeTuple->getString(keyIdx.back()[0]));
					} else {
						bucket = outerHashTable->getHashLocation(probeTuple, keyIdx.back());
					}

					if (bucket < frozen) {
						// Joining to a frozen bucket

						if (curType == PredicateKeyTypeInt) {
							outerHashTable->insert(probeTuple->getInt(keyIdx.back()[0]), probeTuple);
						} else if (curType == PredicateKeyTypeString) {
							outerHashTable->insert(probeTuple->getString(keyIdx.back()[0]), probeTuple);
						} else {
							outerHashTable->insert(probeTuple, keyIdx.back());
						}

						// As insert may cause a frozen page to now be flushed
						addTupleIOs(outerHashTable->getTupleIOs());
						addPageIOs(outerHashTable->getPageIOs());
					} else {
						// Joining with an in-memory bucket
						// Probe hash table to determine matches
						insertsAvoided++;

						std::vector<int> probeIdx = keyIdx.back();
						for (int ii = 0; ii < input.size()-1; ++ii)
						{
							
							std::shared_ptr<PageHashTable> hashTable = inputHashTables[ii];


							PredicateKeyType curType = keyType[ii];
							std::vector<std::shared_ptr<Tuple>> curMatches;

							if (curType == PredicateKeyTypeInt)
							{
								curMatches = hashTable->probeHashTable(bucket, probeTuple->getInt(probeIdx[0]));
							}
							else if (curType == PredicateKeyTypeString)
							{
								curMatches = hashTable->probeHashTable(bucket, probeTuple->getString(probeIdx[0]));
							} 
							else
							{
								curMatches = hashTable->probeHashTable(bucket, probeTuple, probeIdx);
							}

							if (curMatches.size() == 0)
							{
								matches.clear();
								break;
							}
							matches.push_back(curMatches[0]);
							curLoc[ii] = 0;
						}

						if (matches.size() > 0)
						{
							processingProbe = true;
							hasMatches = true;
							goto outer; 
						}
					}
				}

				//printf("Completed outer partition with %d results at time: %lu\n", getTuplesOutput(),clock() - savedTime);

				// Processed all input tuples from probe relation - now switch to processing frozen
				//printf("Joining frozen buckets.\n");
				input.back()->close();
				inputHashTables.back()->close(BucketStateFrozen); // Flush and close all frozen buckets to disk

				addTupleIOs(inputHashTables.back()->getTupleIOs());
				addPageIOs(inputHashTables.back()->getPageIOs());
				
				processingFrozen = true;
				bucketNum = 0;
				
				if (!setupBucket()) {		// Determines correct build/probe inputs
					return NULL;								// Join finished as no more non-empty buckets left to join
				}
			} // end processing tuple from probe relation
		} // end while loop
		return NULL;
	}


	void HashTeamJoin::purge(int curIdx)
	{

		if (frozen >= numBuckets - 1)
		{
			//printf("WARNING: Tried to flush last partition.  Need to repartition inputs");
			return;
		}

		usedPages = 0;
		for (int ii = 0; ii < input.size() - 1; ++ii)
		{
			inputHashTables[ii]->purge(frozen);
			if (ii != curIdx)
				usedPages += inputHashTables[ii]->getUsedPages();
		}

		frozen++;
	}


	bool HashTeamJoin::partitionInput(std::size_t index)
	{
		assert(index < input.size());

		std::shared_ptr<Operator> theInput = input[index];

		if (!theInput)
			return false;

		std::shared_ptr<Relation> inputRel = theInput->getOutputRelation();
		char *tdata = new char[(inputRel->computeTupleSize() + 2 * inputRel->getNumAttributes() + DB_TUPLE_FIRST_OFFSET)];

		std::shared_ptr<Tuple> tuple(new Tuple(inputRel));
		tuple->setData(tdata, true);
		
		std::shared_ptr<Tuple> tmp(nullptr);
		while ((tmp = theInput->next(tuple)))
		{
			incrementTuplesRead();
			bool mustPurge = false;

			switch (keyType[index])
			{
			case PredicateKeyTypeInt:
				mustPurge = inputHashTables[index]->insert(tuple->getInt(keyIdx[index][0]), tuple);
				break;
			case PredicateKeyTypeString:
				mustPurge = inputHashTables[index]->insert(tuple->getString(keyIdx[index][0]), tuple);
				break;
			case PredicateKeyTypeObject:
				mustPurge = inputHashTables[index]->insert(tuple,keyIdx[index]);
				break;
			default:
				printf("error: unknown key type %d", keyType[index]);
				break;
			}	

			// As insert may cause a frozen page to now be flushed
			addTupleIOs(inputHashTables[index]->getTupleIOs());
			addPageIOs(inputHashTables[index]->getPageIOs());

			if (inputHashTables[index]->getUsedPages() + usedPages > BUFFER_SIZE)
			{
				purge(index);

				for (auto &ht : inputHashTables)
				{
					addTupleIOs(ht->getTupleIOs());
					addPageIOs(ht->getPageIOs());
				}
			}
		}

		usedPages += inputHashTables[index]->getUsedPages();
		theInput->close();
		

		//printf("Building hash tables for memory resident buckets.  # of frozen buckets: %d # of buckets: %d  Tuples: %d\n", frozen, numBuckets,getTuplesRead());
		for (int ii=frozen; ii < numBuckets; ii++)
		{
			inputHashTables[index]->buildHashTable(ii,keyType[index],keyIdx[index]);
		}
		return true;
	}

	bool HashTeamJoin::setupBucket()
	{
		// Find a pair of buckets which both have some tuples in them
		while (true)
		{
			if (bucketNum >= frozen)
				// Could not find a pair of non-empty frozen buckets to join
					return false;

			for (auto &hashTable : inputHashTables)
			{
				if (hashTable->getTupleCount(bucketNum) < 1)
				{
					bucketNum++;
					continue;
				}
			}
			
			break;
		}

		//printf("Performing join of bucket: %d\n",bucketNum);
		bucketHashTables.clear();
		// to simplify we will always probe the same way for hash teams
		

		// loop over the build tables
		for (int ii = 0; ii < input.size() - 1; ++ii)
		{
			int numTuples = 0;

			std::shared_ptr<PageHashTable> &inputHashTable = inputHashTables[ii];
			FILE *buildFile = openInputFile(inputHashTable->getFileName(bucketNum).c_str());
			numTuples += inputHashTable->getTupleCount(bucketNum);

			std::shared_ptr<Relation> &tRel = relations[ii];
			std::shared_ptr<Page> p(new Page(DB_PAGE_SIZE_DEFAULT, tRel));
			std::shared_ptr<Tuple> t(nullptr);

			bucketHashTables.push_back(std::shared_ptr<ChainedHashtable>(new ChainedHashtable(numTuples, keyIdx[ii])));

			int numPagesRead = 0;
			while (p->read(buildFile) > 0)
			{
				numPagesRead++;

				p->initIterator();
				bucketHashTables[ii]->addPage(p);
				while (p->hasNext())
				{
					t = std::shared_ptr<Tuple>(new Tuple(tRel));
					p->next(t);
					bucketHashTables[ii]->insert(t);
				}

				// create a new page for reading into
				p = std::shared_ptr<Page>(new Page(DB_PAGE_SIZE_DEFAULT, p->getRelation()));
			}

			closeFile(buildFile);
			buildFile = nullptr;

			addTupleIOs(numTuples);
			addPageIOs(numPagesRead);

			//printf("Created hash table: %d Size: %d\n", bucketNum, numTuples);
		}

		probeFile = openInputFile(inputHashTables.back()->getFileName(bucketNum).c_str());
		probePage->read(probeFile);
		probePage->initIterator();

		addTupleIOs(probePage->getTupleCount());
		incrementPageIOs();

		return true;
	}

	void HashTeamJoin::cleanup()
	{
		for (auto &hashTable : inputHashTables)
		{
			hashTable->clear();
		}

		//inputHashTables.clear();
	}

}