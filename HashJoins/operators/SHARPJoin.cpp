#include "SHARPJoin.h"
#include "../io/FileManager.h"
#include "AttributeBitMap.h"
#include "../util/hashfunc.h"

#include <cstdlib>
#include <cstdio>
#include <cassert>

namespace db
{
	SHARPJoin::SHARPJoin(std::vector<std::shared_ptr<Operator>> inputs,
		std::vector<std::shared_ptr<EquijoinPredicate>> predicates,
		int theBufferSize,
		int theNumBuckets)
		:Operator(inputs, theBufferSize),
		numBuckets(theNumBuckets),
		preds(predicates),
		probeFile(nullptr),
		keyType(PredicateKeyTypeUnknown),
		processingFrozen(false),
		processingProbe(false),
		savedTime(0),
		insertsAvoided(0),
		usedPages(0),
		numPartitions(std::vector<int>(inputs.size(),0))
	{

	}

	SHARPJoin::~SHARPJoin()
	{

	}

	bool SHARPJoin::init()
	{
		assert(input.size() > 1);

		//for (auto &in : input)
		for (int ii = 0; ii < input.size()-1; ++ii)
		{
			auto &in = input[ii];
			if (!in->init())
			{
				printf("failed to initialize input to sharp join\n");
				return false;
			}
			std::shared_ptr<Relation> curRelation = in->getOutputRelation();

			relations.push_back(curRelation);

			long long bytesAvailable = BUFFER_SIZE * static_cast<long long>(DB_PAGE_SIZE_DEFAULT);
			long long relBytes = curRelation->getRelationByteSize();
			int curNumBuckets = numBuckets;

			if (relBytes > 0)
			{
				// we have an estimate for the size of this relation so lets be smart about the number of partitions

				curNumBuckets = static_cast<int>(ceil(relBytes / static_cast<double>(bytesAvailable)));
			}
			numPartitions[ii] = curNumBuckets;
			inputHashTables.push_back(std::shared_ptr<PageHashTable>(new PageHashTable(curNumBuckets,DB_PAGE_SIZE_DEFAULT,BUFFER_SIZE,curRelation)));
		}

		// now do the fact table
		auto &in = input.back();
		if (!in->init())
		{
			printf("failed to initialize input to sharp join\n");
			return false;
		}
		std::shared_ptr<Relation> curRelation = in->getOutputRelation();

		relations.push_back(curRelation);

		// this one is partitioned in multiple dimensions
		int probeNumPartitions = 1;

		for (int ii = 0; ii < input.size() -1; ++ii)
		{
			probeNumPartitions *= numPartitions[ii];
		}

		numPartitions[input.size() - 1] = probeNumPartitions;
		inputHashTables.push_back(std::shared_ptr<PageHashTable>(new PageHashTable(probeNumPartitions,DB_PAGE_SIZE_DEFAULT,BUFFER_SIZE,curRelation)));

		usedPages = 0;

		// first grab the left side of the first predicate
		int firstNumAttrs = preds[0]->getNumAttributes();
		keyIdx.push_back(preds[0]->getRelation1Locs());

		std::shared_ptr<Relation> outputRel (new Relation(relations));
		setOutputRelation(outputRel);

		if (firstNumAttrs > 1)
		{
			keyType.push_back(PredicateKeyTypeObject);
		}
		else if (relations[0]->getAttributeType(keyIdx[0][0]) == DB_TYPE_INT)
		{
			keyType.push_back(PredicateKeyTypeInt);
		} 
		else if (relations[0]->getAttributeType(keyIdx[0][0]) == DB_TYPE_STRING)
		{
			keyType.push_back(PredicateKeyTypeString);
		}

		// and now grab the right side of all the predicates
		for (int ii = 0; ii < preds.size(); ++ii)
		{
			auto &pred = preds[ii];
			auto &rel = relations[ii+1];
			int curNumAttrs = pred->getNumAttributes();
			numAttrs.push_back(curNumAttrs);
			keyIdx.push_back(pred->getRelation2Locs());

			if (curNumAttrs > 1)
			{
				keyType.push_back(PredicateKeyTypeObject);
			}
			else if (rel->getAttributeType(keyIdx[ii+1][0]) == DB_TYPE_INT)
			{
				keyType.push_back(PredicateKeyTypeInt);
			}
			else if (rel->getAttributeType(keyIdx[ii+1][0]) == DB_TYPE_STRING)
			{
				keyType.push_back(PredicateKeyTypeString);
			}
		}

		// we need to partition all the build tables
		// each build table needs to be partitioned based on the amount of that table that will fit in memory
		// we then partition the probe table in multiple dimensions
		// we can output join tuples in memory

		// Question?
		// How do we decide how much memory to give each table?  Should we split it equally? 
		bucketHashTables = std::vector<std::shared_ptr<ChainedHashtable>>(input.size(), nullptr);
		// Build innerHashTables
		//printf("Reading and partitioning build tables.\n");
		curPartitionIdexes = std::vector<int>(input.size(), 0);
		for (std::size_t ii = 0; ii < input.size() - 1; ++ii)
		{
			clock_t tmpTime = clock();
			partitionInput(ii);

			clock_t endTime = clock();
			//printf("Finished partitioning build table %ld at %ldms\n", ii,(endTime - tmpTime));
		}

		// Now partition the probe ???

		// flush all frozen buckets // 
		for (std::size_t ii = 0; ii < input.size() - 1; ++ii)
		{
		
			inputHashTables[ii]->close(BucketStateFrozen);

			addTupleIOs(inputHashTables[ii]->getTupleIOs());
			addPageIOs(inputHashTables[ii]->getPageIOs());
		}

		// Now flush the in memory buckets if they are needed later

		bool needFlushInMemory = false;
		for (int inputPartitions : numPartitions) {
			if (inputPartitions > 1) {
				needFlushInMemory = true;
				break;
			}
		}

		//printf("Flush in memory partitions: %s\n", (needFlushInMemory?"Y":"N"));
		//TODO: figure out when we don't need to do this.
		if (needFlushInMemory) {
			for (std::size_t ii = 0; ii < input.size() - 1; ++ii)
			{

				inputHashTables[ii]->SHARPClose();
				addTupleIOs(inputHashTables[ii]->getTupleIOs());
				addPageIOs(inputHashTables[ii]->getPageIOs());
			}
		}

		// Initialize the buckets in the outerHashTable that are frozen
		for (int idx = 0; idx < probeNumPartitions; ++idx) {
			inputHashTables.back()->initBucket(idx, BucketStateFrozen);
		}

		// Initialize iterator state variables
		processingFrozen = false;
		processingProbe = false;
		leftBuild = true; // always true?


		// alloc a tuple
		std::shared_ptr<Relation> &outer = relations.back();

		std::size_t dataSize = outer->computeTupleSize() + outer->getNumAttributes() * 2 + 4;
		char *data = new char[dataSize];

		probeTuple = std::shared_ptr<Tuple>(new Tuple(outer, data));


		// alloc a page
		probePage = std::shared_ptr<Page>(new Page(DB_PAGE_SIZE_DEFAULT, outer));

		savedTime = clock();
		insertsAvoided = 0;

		for (int ii = 0; ii < input.size() - 1; ++ ii)
		{
			curLoc.push_back(0);
		}
		

		return true;
	}


	bool SHARPJoin::close()
	{
		//printf("Inserts avoided: %d\n", insertsAvoided);
		cleanup();
		return true;
	}

	/**
	Grabs the current tuples at the current indices and increments the indices
	*/
	std::vector<std::shared_ptr<Tuple>> SHARPJoin::getCurrentMatches()
	{
		std::vector<std::shared_ptr<Tuple>> tuples;
		return tuples;
		
		//int nMatchVec = matches.size();
		//for (int ii = nMatchVec - 1; ii >= 0; --ii)
		//{
		//	tuples.push_back(matches[ii][curLoc[ii]]);
		//}
		//tuples.push_back(probeTuple);

		//for (int ii = nMatchVec - 1; ii >= 0; --ii)
		//{
		//	curLoc[ii] ++;
		//	if (matches[ii].size() -1 > curLoc[ii])
		//	{
		//		break;
		//	}
		//	else
		//	{
		//		// match
		//		curLoc[ii] = 0;
		//	}

		//	if (ii == 0)
		//	{
		//		// done condition
		//		bool done = true;

		//		for (auto &i : curLoc)
		//		{
		//			if (i != 0) {
		//				done = false;
		//				break;
		//			}

		//		}

		//		if (done)
		//			hasMatches = false; 
		//	}
		//}

		//return tuples;
	}

	std::shared_ptr<Tuple> SHARPJoin::next(std::shared_ptr<Tuple> tuple)
	{
		while (true)
		{
outer:
			while (processingProbe)
			{
				if (hasMatches)
				{
					// Processing join tuples from a previous probe into the build tables
					std::vector<std::shared_ptr<Tuple>> tuples = matches;// getCurrentMatches();
					incrementTuplesOutput();
					hasMatches = false;
					return Tuple::join(tuples, getOutputRelation() ,tuple);
				}
				else
				{
					// we have processed all possible matches
					processingProbe = false;
					matches.clear();
				}
				
			} // end processingProbe
// outer:
			if (processingFrozen) {
				while (probePage->hasNext())
				{
					
					probeTuple = probePage->next(probeTuple);
					std::vector<int> probeIdx = keyIdx.back();

					PredicateKeyType curType = keyType.back();
//					std::shared_ptr<PageHashTable> &outerHashTable = inputHashTables.back();

					
					for (int ii = input.size() - 2; ii >= 0 ; --ii)
					{
						probeIdx = preds[ii]->getRelation2Locs();
						
						std::shared_ptr<ChainedHashtable> hashTable = bucketHashTables[ii];

						PredicateKeyType curType = keyType[ii];
						std::vector<std::shared_ptr<Tuple>> curMatches;

						if (curType == PredicateKeyTypeInt)
						{
							int probeInt = probeTuple->getInt(probeIdx[0]);
							//printf("Probing %d with key: %d\n", ii, probeInt);
							curMatches = hashTable->find(probeTuple->getInt(probeIdx[0]));
						}
						else if (curType == PredicateKeyTypeString)
						{
							curMatches = hashTable->find(probeTuple->getString(probeIdx[0]));
						} 
						else
						{
							curMatches = hashTable->find(probeTuple, probeIdx);
						}

						if (curMatches.size() == 0)
						{
							matches.clear();
							break;
						}
						matches.push_back(curMatches[0]);
						curLoc[ii] = 0;
					}

					if (matches.size() > 0)
					{
						processingProbe = true;
						hasMatches = true;
						goto outer; 
					}
				}

				// finished processing a page of probe tuples - read next one if possible
				if (probePage->read(probeFile) > 0) { 
					probePage->initIterator();
					incrementPageIOs();
					addTupleIOs(probePage->getTupleCount());
				} else {
					closeFile(probeFile);
					probeFile = nullptr;
					if (bucketNum == numPartitions.back() - 1) {// Process all frozen buckets done join!
						cleanup();// Also called in close() but done here to make sure temp files are deleted ASAP
						return NULL;
					}
					bucketNum++;// Process next bucket
					if (!setupBucket()) {
						cleanup(); // Also called in close() but done here to make sure temp files are deleted ASAP
						return NULL;
					}
				}
				// end processingFrozen
			} else {
				// Processing input tuples from probe relation
				std::shared_ptr<Tuple> tmp(nullptr);
				int probeRel = input.size() - 1;
				std::shared_ptr<PageHashTable> &outerHashTable = inputHashTables.back();
				while ((tmp = input[probeRel]->next(probeTuple))) {
					int bucket;
					incrementTupleIOs();

					std::vector<int> dimBucket;

					for (int ii = 0; ii < input.size() - 1; ++ii)
					{
						std::vector<int> probeIdx = preds[ii]->getRelation2Locs();

						PredicateKeyType curType = preds[ii]->getKeyType();//keyType.back(); // FIXME: assuming int for all for now
						std::shared_ptr<PageHashTable> &probeHashTable = inputHashTables[ii];
					
						if (curType == PredicateKeyTypeInt) {
							bucket = probeHashTable->getHashLocation(probeTuple->getInt(probeIdx[0]));
						} else if (curType == PredicateKeyTypeString) {
							bucket = probeHashTable->getHashLocation(probeTuple->getString(probeIdx[0]));
						} else {
							bucket = probeHashTable->getHashLocation(probeTuple, probeIdx);
						}

						dimBucket.push_back(bucket);
					}

					int bucketIndex = factTableBucketIndex(dimBucket);
					//printf("Current probe tuple index: %d\n", bucketIndex);
					if (bucketIndex < numPartitions.back() - 1) {
						// Joining to a frozen bucket
						outerHashTable->insert(probeTuple,bucketIndex);
						
						// As insert may cause a frozen page to now be flushed
						addTupleIOs(outerHashTable->getTupleIOs());
						addPageIOs(outerHashTable->getPageIOs());
					} else {
						// Joining with an in-memory bucket set
						// Probe hash table to determine matches
						insertsAvoided++;

						std::vector<int> probeIdx = keyIdx.back();
						for (int ii = 0; ii < input.size()-1; ++ii)
						{

							std::shared_ptr<PageHashTable> hashTable = inputHashTables[ii];
							std::vector<int> probeIdx = preds[ii]->getRelation2Locs();

							PredicateKeyType curType = preds[ii]->getKeyType();// keyType[ii];
							std::vector<std::shared_ptr<Tuple>> curMatches;

							if (curType == PredicateKeyTypeInt)
							{
								int probeInt = probeTuple->getInt(probeIdx[0]);
								//printf("Probing %d with key: %d\n", ii, probeInt);
								curMatches = hashTable->probeHashTable(dimBucket[ii], probeTuple->getInt(probeIdx[0]));
							}
							else if (curType == PredicateKeyTypeString)
							{
								curMatches = hashTable->probeHashTable(dimBucket[ii], probeTuple->getString(probeIdx[0]));
							} 
							else
							{
								curMatches = hashTable->probeHashTable(dimBucket[ii], probeTuple, probeIdx);
							}

							if (curMatches.size() == 0)
							{
								matches.clear();
								break;
							}
							matches.push_back(curMatches[0]);
							curLoc[ii] = 0;
						}

						if (matches.size() > 0)
						{
							//insertsAvoided++;
							processingProbe = true;
							hasMatches = true;
							goto outer; 
						}
					}
				}

				//printf("Completed outer partition with %d results at time: %lu\n", getTuplesOutput(),clock() - savedTime);

				// Processed all input tuples from probe relation - now switch to processing frozen
				//printf("Joining frozen buckets.\n");
				input.back()->close();
				inputHashTables.back()->close(BucketStateFrozen); // Flush and close all frozen buckets to disk

				addTupleIOs(inputHashTables.back()->getTupleIOs());
				addPageIOs(inputHashTables.back()->getPageIOs());
				
				processingFrozen = true;
				bucketNum = 0;
				
				if (!setupBucket()) {		// Determines correct build/probe inputs
					return NULL;								// Join finished as no more non-empty buckets left to join
				}
			} // end processing tuple from probe relation
		} // end while loop
		return NULL;
	}


	//void SHARPJoin::purge(int curIdx)
	//{

	//	if (frozen >= numBuckets - 1)
	//	{
	//		printf("WARNING: Tried to flush last partition.  Need to repartition inputs\n");
	//		return;
	//	}

	//	usedPages = 0;
	//	for (int ii = 0; ii < input.size() - 1; ++ii)
	//	{
	//		inputHashTables[ii]->purge(frozen);
	//		if (ii != curIdx)
	//			usedPages += inputHashTables[ii]->getUsedPages();
	//	}

	//	frozen++;
	//}

	// FIXME: assumes 2 dim tables
	int SHARPJoin::factTableBucketIndex(std::vector<int> &bucketIndexes)
	{
		int aNum = numPartitions[1];
		return bucketIndexes[1] + (aNum * bucketIndexes[0]);
	}
	
	std::vector<int> SHARPJoin::dimBucketsForFactIndex(int i)
	{
		int aNum = numPartitions[1];
		std::vector<int> indexes;
		int first = i / aNum;
		int second = i - (first * aNum);
		indexes.push_back(first);
		indexes.push_back(second);
		return indexes;
	}

	/// For now assume we have three inputs and need a map for the second join....
	bool SHARPJoin::partitionInput(std::size_t index)
	{
		assert(index < input.size());

		std::shared_ptr<Operator> theInput = input[index];

		if (!theInput)
			return false;

		std::shared_ptr<Relation> inputRel = theInput->getOutputRelation();
		char *tdata = new char[(inputRel->computeTupleSize() + 2 * inputRel->getNumAttributes() + DB_TUPLE_FIRST_OFFSET)];

		std::shared_ptr<Tuple> tuple(new Tuple(inputRel));
		tuple->setData(tdata, true);

		int frozen = 0;
		std::vector<int> keyLocs = preds[index]->getRelation1Locs();
		std::shared_ptr<Tuple> tmp(nullptr);
		while ((tmp = theInput->next(tuple)))
		{
			incrementTuplesRead();
			bool mustPurge = false;

			switch (keyType[index])
			{
			case PredicateKeyTypeInt:
				mustPurge = inputHashTables[index]->insert(tuple->getInt(keyIdx[index][0]), tuple);
				break;
			case PredicateKeyTypeString:
				mustPurge = inputHashTables[index]->insert(tuple->getString(keyIdx[index][0]), tuple);
				break;
			case PredicateKeyTypeObject:
				mustPurge = inputHashTables[index]->insert(tuple,keyLocs);
				break;
			default:
				printf("error: unknown key type %d", keyType[index]);
				break;
			}	

			// As insert may cause a frozen page to now be flushed
			addTupleIOs(inputHashTables[index]->getTupleIOs());
			addPageIOs(inputHashTables[index]->getPageIOs());

			if (mustPurge && frozen < numPartitions[index] - 1)
			{
				inputHashTables[index]->purge(frozen);
				frozen++;

				addTupleIOs(inputHashTables[index]->getTupleIOs());
				addPageIOs(inputHashTables[index]->getPageIOs());
			}
		}

		usedPages += inputHashTables[index]->getUsedPages();
		theInput->close();


		//printf("Building hash tables for memory resident buckets.  # of frozen buckets: %d # of buckets: %d  Tuples: %d\n", frozen, numPartitions[index],getTuplesRead());
		for (int ii=frozen; ii < numPartitions[index]; ii++)
		{
			bucketHashTables[index] = inputHashTables[index]->buildHashTable(ii,keyType[index],keyLocs);
			//bucketHashTables[index]->printBuckets();
		}
		frozenPartitions.push_back(frozen);
		curPartitionIdexes[index] = frozen;
		return true;
	}

	bool SHARPJoin::setupBucket()
	{
		// Find a pair of buckets which both have some tuples in them
		std::vector<int> indexes;

		while (true)
		{
			if (bucketNum >= numPartitions.back() - 1)
				// Could not find a pair of non-empty frozen buckets to join
					return false;

			indexes = dimBucketsForFactIndex(bucketNum);
			
			for (int ii = 0; ii < input.size() - 1; ++ii)
			{
				auto &hashTable = inputHashTables[ii];

				if (hashTable->getTupleCount(indexes[ii]) < 1)
				{
					bucketNum++;
					continue;
				}
			}

			// now the fact table
			if (inputHashTables.back()->getTupleCount(bucketNum) < 1)
			{
				bucketNum ++;
				continue;
			}
			
			break;
		}

		//printf("Performing join of bucket: %d\n",bucketNum);

		// to simplify we will always probe the same way for hash teams
		//bucketHashTables.clear();

		// loop over the build tables
		for (int ii = 0; ii < input.size() - 1; ++ii)
		{
			int numTuples = 0;

			int curBucketIdx = indexes[ii];
			// check if this partition is already in memory
			if (curPartitionIdexes[ii] == curBucketIdx)
			{
				//bucketHashTables[ii]->printBuckets();
			//	continue;
			}
			
			std::shared_ptr<PageHashTable> &inputHashTable = inputHashTables[ii];
			FILE *buildFile = openInputFile(inputHashTable->getFileName(curBucketIdx).c_str());
			numTuples += inputHashTable->getTupleCount(curBucketIdx);

			std::shared_ptr<Relation> &tRel = relations[ii];
			std::shared_ptr<Page> p(new Page(DB_PAGE_SIZE_DEFAULT, tRel));
			std::shared_ptr<Tuple> t(nullptr);
			const std::vector<int>&curKeyIdx = preds[ii]->getRelation1Locs();
			bucketHashTables[ii] = (std::shared_ptr<ChainedHashtable>(new ChainedHashtable(numTuples, curKeyIdx)));

			int numPagesRead = 0;
			while (p->read(buildFile) > 0)
			{
				numPagesRead++;

				p->initIterator();
				bucketHashTables[ii]->addPage(p);
				while (p->hasNext())
				{
					t = std::shared_ptr<Tuple>(new Tuple(tRel));
					p->next(t);
					bucketHashTables[ii]->insert(t);
				}

				// create a new page for reading into
				p = std::shared_ptr<Page>(new Page(DB_PAGE_SIZE_DEFAULT, p->getRelation()));
			}

			closeFile(buildFile);
			buildFile = nullptr;

			addTupleIOs(numTuples);
			addPageIOs(numPagesRead);


			//printf("Created hash table: %d Size: %d for intput %d\n", curBucketIdx, numTuples, ii);
			//bucketHashTables[ii]->printBuckets();
		}

		probeFile = openInputFile(inputHashTables.back()->getFileName(bucketNum).c_str());
		probePage->read(probeFile);
		probePage->initIterator();

		addTupleIOs(probePage->getTupleCount());
		incrementPageIOs();

		return true;
	}

	void SHARPJoin::cleanup()
	{
		for (auto &hashTable : inputHashTables)
		{
			hashTable->clear();
		}

		inputHashTables.clear();
	}

}