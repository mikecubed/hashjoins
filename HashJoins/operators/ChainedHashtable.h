/*
FILE:			chainedhashtable.h
PROVIDES: 		An in-memory hash table class that using separate chaining (buckets) to resolve collisions.
				Supports multiple keys with the same value.  Note that the hash entrys only contain a pointer to the data
C CODE BY:		Michael Henderson
CREATION DATE:	January 2008
BASED ON:		unity.operators.ChainedHashTable.java
PROGRAM BY:     Ramon Lawrence (ramon.lawrence@ubc.ca)
*/
#ifndef chainedhashtable_h__
#define chainedhashtable_h__

#include "../relational/Tuple.h"
#include "../io/Page.h"
//#include "hashentry.h"

#include <list>
#include <vector>
#include <memory>
#include <string>

namespace db 
{
	class ChainedHashtable
	{
	public:
		//ChainedHashtable(int size, std::shared_ptr<Page> p);
		/**
		\brief Designated initializer
		A chained hash table implementation for tuples.

		\param size The number of buckets for the hash table
		\param keyIdx The attribute indices for the tuples key
		*/
		ChainedHashtable(int size, const std::vector<int>& keyIdx);
		~ChainedHashtable();

		/**
		\brief Remove all tuples from the buckets
		*/
		void clear();

		/**
		Insert a tuple into the hash table.  This will calculate the hash for the tuple based on
		the key

		\param tuple The tuple to add to the hash table.
		*/
		void insert(std::shared_ptr<Tuple> tuple);

		void addPage(std::shared_ptr<Page> p);

		std::list<std::shared_ptr<Tuple>>& getBucket(int key);
		std::list<std::shared_ptr<Tuple>>& getBucket(const std::string& key);
		std::list<std::shared_ptr<Tuple>>& getBucket(std::shared_ptr<Tuple> key, const std::vector<int>& keyIdx);
		std::vector<std::shared_ptr<Tuple>> find(int key);
		std::vector<std::shared_ptr<Tuple>> find(const std::string& key);
		std::vector<std::shared_ptr<Tuple>> find(std::shared_ptr<Tuple> key, const std::vector<int>& keyIdx);

		void printBucket(int index);
		void printBuckets();

		int getProbes() {return probes;}
		int getProbesAvoided() {return probesAvoided;}
		int searches;
	private:
		std::vector<std::list<std::shared_ptr<Tuple>>> buckets;
		std::vector<int> keyIndex;
		int currBucket;
		int tableSize;

		int probes;
		int probesAvoided;

//		std::list<HashEntry<K,D>>::iterator it;
		std::list<std::shared_ptr<Page>> pages; 
	};



}

#endif
