//
//  TextFileScan.cpp
//  hashjoins
//
//  Created by Michael Henderson on 05-07-2012.
//  Copyright (c) 2012 VeriCorder Technology Inc. All rights reserved.
//

#include "TextFileScan.h"
#include "../io/FileManager.h"

namespace db {
	TextFileScan::TextFileScan(const std::string &inName, std::shared_ptr<Relation> rel, std::string separators) 
	: Operator(std::vector<std::shared_ptr<Operator>>(), 0),
	inFileName(inName),
	fieldSeparators(separators),
	inFile(nullptr),
	inputRelation(rel)
	{
	}
	
	TextFileScan::~TextFileScan()
	{}
	
	bool TextFileScan::init()
	{
		inFile = db::openTextInputFile(inFileName.c_str());
		
		if (inFile)
			return true;
		
		return false;
	}
	
	std::shared_ptr<Tuple> TextFileScan::next(std::shared_ptr<Tuple> tuple)
	{
		if (!tuple)
			tuple = std::shared_ptr<Tuple>(new Tuple(inputRelation));

		const char *tokens = nullptr;
		if (fieldSeparators.length() > 0)
			tokens = fieldSeparators.c_str();

		if (tuple->readText(inFile, tokens))
		{
			this->incrementTuplesRead();
			this->incrementTuplesOutput();
			return tuple;
		}
		return nullptr;
	}
	
	bool TextFileScan::hasNext()
	{
		return db::fileEndOfFile(inFile) == 0;
	}
	
	bool TextFileScan::close()
	{
		db::closeFile(inFile);
		inFile = nullptr;
		return true;
	}
	
}