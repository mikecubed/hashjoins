#include "PageFileScan.h"
#include "../io/FileManager.h"
#include "../relational/Relation.h"
#include "../relational/Tuple.h"

namespace db
{
	PageFileScan::PageFileScan(const std::string& inName, std::shared_ptr<Relation> rel, short inPageSize)
		:Operator(std::vector<std::shared_ptr<Operator>>(),0),
		inputFileName(inName),
		inputRelation(rel),
		pageSize(inPageSize),
		inputFile(nullptr)
	{
		page = std::shared_ptr<Page>(new Page(pageSize, inputRelation));
		setOutputRelation(inputRelation);
	}

	PageFileScan::~PageFileScan()
	{
		//printf("destroying page file scan for file: %s\n", inputFileName.c_str());
	}

	bool PageFileScan::init()
	{
		//Operator::init(); // pure virutal

		inputFile = openInputFile(inputFileName.c_str());
		if (inputFile &&
			page->read(inputFile) > 0)
		{
			page->initIterator();
			return true;
		}
		return false;
	}

	std::shared_ptr<Tuple> PageFileScan::next(std::shared_ptr<Tuple> tuple)
	{
		if (!page->hasNext())
		{
			page->read(inputFile);
			page->initIterator();
			if (!page->hasNext())
				return nullptr;
		}

		if (!tuple)
			tuple = std::shared_ptr<Tuple>(new Tuple(inputRelation));

		tuple = page->next(tuple);

		incrementTuplesRead();
		incrementTuplesOutput();

		return tuple;
	}

	bool PageFileScan::close()
	{
		if (inputFile)
		{
			closeFile(inputFile);
			inputFile = nullptr;
		}
		return true;
	}
}
