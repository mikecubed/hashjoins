#ifndef ATTRIBUTEBITMAP_H
#define ATTRIBUTEBITMAP_H

#include "AttributeMap.h"
#include <vector>

namespace db {

	class AttributeBitMap :
		public AttributeMap
	{
	public:
		AttributeBitMap(unsigned int size);
		virtual ~AttributeBitMap(void);

		virtual void setValueForHash(unsigned int value, unsigned int hash);
		virtual unsigned int valueForHash(unsigned int hash);

	private:
		std::vector<bool> bitmap;
	};
}

#endif // !ATTRIBUTEBITMAP_H