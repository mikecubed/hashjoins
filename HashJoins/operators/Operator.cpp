#include "Operator.h"

namespace db 
{

	Operator::Operator(const std::vector<std::shared_ptr<Operator>> &inputs,int bufferSize)
		:input(inputs),
		BUFFER_SIZE(bufferSize),
		tuplesOutput(0),
		tuplesRead(0),
		pagesRead(0),
		internalTupleIOs(0),
		internalPageIOs(0),
		outputRelation(nullptr)
	{
	}


	Operator::~Operator(void)
	{
	}

	void Operator::setStartTime(clock_t tm) {startTime = tm;}

	void Operator::setOutputRelation(std::shared_ptr<Relation> rel)
	{
		outputRelation = rel;
	}

	std::shared_ptr<Relation> Operator::getOutputRelation() { return outputRelation; }

	// Tuple and page counter methods
	void Operator::incrementTuplesOutput() { tuplesOutput++; }
	void Operator::incrementTuplesRead() {tuplesRead++;}
	void Operator::incrementPagesRead() {pagesRead++;}
	void Operator::incrementTupleIOs() {internalTupleIOs++;}
	void Operator::incrementPageIOs() {internalPageIOs++;}

	void Operator::addTuplesRead(int i) { tuplesRead += i; }
	void Operator::addPagesRead(int i) { pagesRead += i; }
	void Operator::addTuplesOutput(int i) { tuplesOutput += i; }
	void Operator::addTupleIOs(int i) {internalTupleIOs += i; }
	void Operator::addPageIOs(int i) { internalPageIOs += i; }

	int Operator::getTuplesOutput() {return tuplesOutput;}
	int Operator::getTuplesRead() {return tuplesRead;}
	int Operator::getPagesRead() {return pagesRead;}
	int Operator::getTupleIOs() {return internalTupleIOs;}
	int Operator::getPageIOs() {return internalPageIOs;}
}