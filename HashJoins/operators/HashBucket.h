/*
FILE:			hashbucket.h
PROVIDES: 		The hash bucket data structure and associated functions for the pagehashtable
				each bucket holds a linked list of pages
C CODE BY:		Michael Henderson
CREATION DATE:	January 2008
BASED ON:		unity.operators.PageHashTable.java
PROGRAM BY:     Ramon Lawrence (ramon.lawrence@ubc.ca)
CREATION DATE:  April 2004
MODIFIED:		
*/
#ifndef hashbucket_h__
#define hashbucket_h__

#include "../io/Page.h"
#include "../relational/Tuple.h"
#include "../predicates/EquijoinPredicate.h"
#include "ChainedHashtable.h"

#include <cstdio>
#include <vector>
#include <list>
#include <memory>
#include <string>

namespace db {
	class PageHashTable;

	typedef enum {
		BucketStateEmpty =  0,					// No pages are currently allocated to the bucket
		BucketStateExpanding,					// The bucket has pages and can add new ones
		BucketStateFrozen, 						// The bucket is frozen with its current number of pages (usually 1).
	} BucketState;

	class HashBucket 
	{
	public:
		HashBucket(int bucketNum, short pageSize, std::shared_ptr<Relation> rel);
		~HashBucket();

		BucketState getState();
		void setState(BucketState newState);
		int getPageCount();
		int getTupleCount();
		std::string getFileName();
		std::list<std::shared_ptr<Page>> getPageList();
		void clearPageList(); 
		void flush(PageHashTable &pht);
		void close(PageHashTable &pht);
		void SHARPClose(PageHashTable &pht);
		void purge(PageHashTable &pht);
		void init(PageHashTable &pht, BucketState state);
		bool add(PageHashTable &pht, std::shared_ptr<Tuple> t);
		std::shared_ptr<ChainedHashtable> buildHashTable(PredicateKeyType keyType, std::vector<int> keyIdx);
		std::vector<std::shared_ptr<Tuple>> probeHashTable(int key);
		std::vector<std::shared_ptr<Tuple>> probeHashTable(const std::string& key);
		std::vector<std::shared_ptr<Tuple>> probeHashTable(std::shared_ptr<Tuple> key, const std::vector<int>& keyIdx);

	private:
		std::list<std::shared_ptr<Page>> pages;		///< list of memory resident pages
		std::shared_ptr<Relation> relation;			///< relation for tuples in the pages
		BucketState state;							///< state of bucket
		short pageSize;								///< size of pages in memory
		int tupCnt;									///< # of tuples in bucket
		int pageCnt;								///< # of pages in bucket
		int resPageCnt;								///< # of bucket pages resident in memory
		std::string fileName;						///< Name of file storing overflow pages of bucket
		FILE *outFile;								///< File descriptor (maybe NULL) for bucket file
		int bucketNumber;							///< Bucket Number (identifier)
		std::shared_ptr<ChainedHashtable> hashTable;///< In-memory hash table (not always used)

		friend class PageHashTable;
	};

}
#endif // hashbucket_h__
