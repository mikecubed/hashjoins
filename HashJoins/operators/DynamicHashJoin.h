/*
FILE:			dynamichashjoin.h
PROVIDES: 		Contains code for performing a two-pass dynamic hash join in iterator format.
				"derived" from db_operator
C CODE BY:		Michael Henderson
CREATION DATE:	January 2008
BASED ON:		unity.operators.DynamicHashJoin.java
PROGRAM BY:     Ramon Lawrence (ramon.lawrence@ubc.ca)
*/
#ifndef dynamichashjoin_h__
#define dynamichashjoin_h__

#include "../predicates/EquijoinPredicate.h"
#include "../relational/Relation.h"
#include "PageHashTable.h"
#include "ChainedHashtable.h"
#include "../relational/Tuple.h"
#include "../io/Page.h"
#include "Operator.h"

#include <vector>
#include <cstdio>
#include <ctime>

namespace db
{
	class DynamicHashJoin : public Operator
	{
	public:
		DynamicHashJoin(std::vector<std::shared_ptr<Operator>> inputs,
						std::shared_ptr<EquijoinPredicate> predicate,
						int bufferSize,
						int numBuckets);

		virtual ~DynamicHashJoin();

		virtual bool init();
		virtual std::shared_ptr<Tuple> next(std::shared_ptr<Tuple> tuple);
		virtual bool close();
		int getFrozen() {return frozen;} // The number of frozen partitions
		int getNumBuckets() {return numBuckets; }

		int getProbes() {return probes;}
		int getProbesAvoided() {return probesAvoided;}
		int searches;
	private:
		// dynamic hash join specific variables
		int numBuckets;								// # of hash buckets
		std::shared_ptr<EquijoinPredicate> pred;	// equi-join comparison class
		std::shared_ptr<Relation> inner;			// schema of inner relation
		std::shared_ptr<Relation> outer;			// schema of outer relation

		// Hash tables
		std::shared_ptr<PageHashTable> innerHashTable;	// hash table for inner relation
		int frozen;									// # of buckets that have been frozen
		std::shared_ptr<PageHashTable> outerHashTable;	// hash table for the outer relation
		PredicateKeyType keyType;					// Type of hash: int, string
		std::vector<int> keyIdxInner;				// Index of key attributes for inner relation
		std::vector<int> keyIdxOuter;				// Index of key attributes for outer relation
		std::size_t numAttrs;						// Number of attributes in key

		// Iterator state variables
		bool processingFrozen;					// True if current processing frozen buckets (on disk) All of probe input table must have been read.
		int bucketNum;							// Stores the frozen bucket number currently processing
		bool processingProbe;					// true if processing tuples created by probing build table with a probe tuple
		std::vector<std::shared_ptr<Tuple>> matches;				// stores the build table tuples that matched a probe
		int curLoc;								// current tuple start processing at in matches
		std::shared_ptr<Tuple> probeTuple;		// Current probe tuple that we are processing
		std::shared_ptr<Page> probePage;		// Current probe page that we are processing
		ChainedHashtable *hashtables[1000]; 

		// Used for processing frozen buckets
		FILE *probeFile;
		bool leftBuild;							// True if left input is build input (when processing frozen disk bucket)
		std::shared_ptr<ChainedHashtable> hashTable;	// Hash table for build input of frozen bucket

		// timing related variables
		clock_t savedTime;						// Used to time outer partition and frozen partition steps
		int insertsAvoided;

		// private functions
		void purge();
		bool partitionInner(std::shared_ptr<Operator> inner);
		bool setupBucket();
		void cleanup();
		int probes;
		int probesAvoided;
		std::shared_ptr<Tuple> outputJoinTuple(std::shared_ptr<Tuple> left, std::shared_ptr<Tuple> right, std::shared_ptr<Tuple> output = nullptr);
	};
}



#endif // dynamichashjoin_h__
