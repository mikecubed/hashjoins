#include "DynamicHashJoin.h"
#include "ChainedHashtable.h"
#include "../io/FileManager.h"

#include <cstdlib>
#include <cstdio>
#include <cassert>
//TODO: Finish implementation of array types...

namespace db
{
	DynamicHashJoin::DynamicHashJoin(std::vector<std::shared_ptr<Operator>> inputs,
									std::shared_ptr<EquijoinPredicate> predicate,
									int theBufferSize,
									int theNumBuckets)
	:Operator(inputs, theBufferSize),
	numBuckets(theNumBuckets),
	pred(predicate),
	probeFile(nullptr),
	keyType(PredicateKeyTypeUnknown),
	inner(nullptr),
	outer(nullptr),
	numAttrs(0),
	bucketNum(0),
	processingFrozen(false),
	processingProbe(false),
	hashTable(nullptr),
	innerHashTable(nullptr),
	outerHashTable(nullptr),
	searches(0)
	{

	}

	DynamicHashJoin::~DynamicHashJoin()
	{

	}

	bool DynamicHashJoin::init()
	{
		assert(input.size() == 2);
		if ( ! input[1]->init() ||
			 ! input[0]->init() )
		{
			printf("Unable to init input to dhj");
			return false;
		}



		numAttrs = pred->getNumAttributes();
		keyIdxInner = pred->getRelation1Locs();
		keyIdxOuter = pred->getRelation2Locs();

		inner = input[0]->getOutputRelation();
		outer = input[1]->getOutputRelation();

		std::shared_ptr<Relation> outputRel (new Relation(*inner, *outer));
		setOutputRelation(outputRel);

		if (numAttrs > 1)
		{
			keyType = PredicateKeyTypeObject;
		}
		else if (inner->getAttributeType(keyIdxInner[0]) == DB_TYPE_INT)
		{
			keyType = PredicateKeyTypeInt;
		}
		else if (inner->getAttributeType(keyIdxInner[0]) == DB_TYPE_STRING)
		{
			keyType = PredicateKeyTypeString;
		}

		innerHashTable = std::shared_ptr<PageHashTable>(new PageHashTable(numBuckets,DB_PAGE_SIZE_DEFAULT,BUFFER_SIZE,input[0]->getOutputRelation()));
		outerHashTable = std::shared_ptr<PageHashTable>(new PageHashTable(numBuckets,DB_PAGE_SIZE_DEFAULT,BUFFER_SIZE,input[1]->getOutputRelation()));
		frozen = 0;


		// Build innerHashTable
		//printf("Reading and partitioning build table.\n");

		clock_t tmpTime = clock();
		partitionInner(input[0]);

		//printf("Finished partitioning build table: %ld\n", clock() - tmpTime);

		// Initialize the buckets in the outerHashTable that are frozen
		for (int idx = 0; idx < frozen; ++idx) {
			outerHashTable->initBucket(idx, BucketStateFrozen);
		}
		
		// Initialize iterator state variables
		processingFrozen = false;
		processingProbe = false;
		leftBuild = true;


		// alloc a tuple
		std::size_t dataSize = outer->computeTupleSize() + outer->getNumAttributes() * 2 + 4;
		char *data = new char[dataSize];

		probeTuple = std::shared_ptr<Tuple>(new Tuple(outer, data));


		// alloc a page
		probePage = std::shared_ptr<Page>(new Page(DB_PAGE_SIZE_DEFAULT, outer));

		savedTime = clock();
		insertsAvoided = 0;

		return true;
	}

	bool DynamicHashJoin::close()
	{
		//printf("Inserts avoided: %d\n", insertsAvoided);
		cleanup();
		for (auto &in : input)
			in->close();

		return true;
	}

	std::shared_ptr<Tuple> DynamicHashJoin::next(std::shared_ptr<Tuple> tuple)
	{
		while (true)
		{
outer:
			while (processingProbe)
			{
				// Processing join tuples from a previous probe into the build table
				if (curLoc >= static_cast<int>(matches.size()))
				{
					processingProbe = false;
					matches.clear();
				}
				else
				{
					auto& tmp = matches[curLoc];
					curLoc++;
					
					if (leftBuild)
					{
						return outputJoinTuple(tmp, probeTuple, tuple);
					} else {
						//tmp.setRelation(outer);
						return outputJoinTuple(probeTuple, tmp, tuple);
					}
				}
			} // end processingProbe
// outer:
			if (processingFrozen) {
				while (probePage->hasNext())
				{
					std::vector<int> currentIdx = keyIdxOuter;

					probeTuple = probePage->next(probeTuple);

					if (!leftBuild) {
						currentIdx = keyIdxInner;
					} /*else {
						currentIdx = keyIdxOuter;
					}*/

					if (keyType == PredicateKeyTypeInt)
					{
						matches = hashTable->find(probeTuple->getInt(currentIdx[0]));
					}
					else if (keyType == PredicateKeyTypeString)
					{
						matches = hashTable->find(probeTuple->getString(currentIdx[0]));
					} 
					else
					{
						matches = hashTable->find(probeTuple, currentIdx);
					}

					curLoc = 0;
					processingProbe = true;

					goto outer; 
				}

				// finished processing a page of probe tuples - read next one if possible
				if (probePage->read(probeFile) > 0) { 
					probePage->initIterator();
					incrementPageIOs();
					addTupleIOs(probePage->getTupleCount());
				} else {
					closeFile(probeFile);
					probeFile = nullptr;
					if (bucketNum == frozen) {// Process all frozen buckets done join!
						cleanup();// Also called in close() but done here to make sure temp files are deleted ASAP
						return NULL;
					}
					bucketNum++;// Process next bucket
					if (!setupBucket()) {
						cleanup(); // Also called in close() but done here to make sure temp files are deleted ASAP
						return NULL;
					}
				}
				// end processingFrozen
			} else {
				// Processing input tuples from probe relation
				//std::shared_ptr<Tuple> tmp(nullptr);
				while ((probeTuple = input[1]->next(probeTuple))) {
					int bucket;
					incrementTupleIOs();

					if (keyType == PredicateKeyTypeInt) {
						bucket = outerHashTable->getHashLocation(probeTuple->getInt(keyIdxOuter[0]));
					} else if (keyType == PredicateKeyTypeString) {
						bucket = outerHashTable->getHashLocation(probeTuple->getString(keyIdxOuter[0]));
					} else {
						bucket = outerHashTable->getHashLocation(probeTuple, keyIdxOuter);
					}

					if (bucket < frozen) {
						// Joining to a frozen bucket

						if (keyType == PredicateKeyTypeInt) {
							outerHashTable->insert(probeTuple->getInt(keyIdxOuter[0]), probeTuple);
						} else if (keyType == PredicateKeyTypeString) {
							outerHashTable->insert(probeTuple->getString(keyIdxOuter[0]), probeTuple);
						} else {
							outerHashTable->insert(probeTuple, keyIdxOuter);
						}

						// As insert may cause a frozen page to now be flushed
						addTupleIOs(outerHashTable->getTupleIOs());
						addPageIOs(outerHashTable->getPageIOs());
					} else {
						// Joining with an in-memory bucket
						// Probe hash table to determine matches
						insertsAvoided++;

						if (keyType == PredicateKeyTypeInt) {
							matches = innerHashTable->probeHashTable(bucket,probeTuple->getInt(keyIdxOuter[0]));
						} else if (keyType == PredicateKeyTypeString) {
							matches = innerHashTable->probeHashTable(bucket, probeTuple->getString(keyIdxOuter[0]));
						} else {
							matches = hashtables[bucket]->find(probeTuple, keyIdxOuter);// innerHashTable->probeHashTable(bucket, probeTuple, keyIdxOuter);
						}

						curLoc = 0;
						processingProbe = true;

						goto outer; // Break out to outer loop to check if probe was successful and process result
					}
				}

				//printf("Completed outer partition: %lu\n", clock() - savedTime);

				// Processed all input tuples from probe relation - now switch to processing frozen
				//printf("Joining frozen buckets.\n");
				input[1]->close();
				outerHashTable->close(BucketStateFrozen);// Flush and close all frozen buckets to disk

				addTupleIOs(outerHashTable->getTupleIOs());
				addPageIOs(outerHashTable->getPageIOs());
				
				processingFrozen = true;
				bucketNum = 0;
				
				//for (int ii = frozen; ii < numBuckets; ++ii) {
				//	//int probes = innerHashTable->getProbeCount(ii);
				//	//int avoided = innerHashTable->getAvoidedProbeCount(ii);

				//	//printf("bucket %d numprobes %d probesavoided %d total %d\n", ii, probes, avoided, (probes + avoided));
				//	searches += innerHashTable->getSearchCount(ii);
				//}

				if (!setupBucket()) {		// Determines correct build/probe inputs
					return NULL;								// Join finished as no more non-empty buckets left to join
				}


			} // end processing tuple from probe relation
		} // end while loop
		return NULL;
	}

	void DynamicHashJoin::purge()
	{
		innerHashTable->purge(frozen++);
	}

	bool DynamicHashJoin::partitionInner(std::shared_ptr<Operator> innerOp)
	{
		if (!innerOp)
			return false;

		char *tdata = new char[(inner->computeTupleSize() + 2 * inner->getNumAttributes() + DB_TUPLE_FIRST_OFFSET)];
		std::shared_ptr<Tuple> tuple(new Tuple(inner));
		tuple->setData(tdata, true);

		std::shared_ptr<Tuple> tmp(nullptr);
		while ((tmp = innerOp->next(tuple)))
		{
			incrementTuplesRead();
			bool mustPurge = false;
			if (keyType == PredicateKeyTypeInt) {
				mustPurge = innerHashTable->insert(tuple->getInt(keyIdxInner[0]), tuple);
			} else if (keyType == PredicateKeyTypeString) {
				mustPurge = innerHashTable->insert(tuple->getString(keyIdxInner[0]), tuple);
			} else {
				mustPurge = innerHashTable->insert(tuple,keyIdxInner);
			}

			// As insert may cause a frozen page to now be flushed
			addTupleIOs(innerHashTable->getTupleIOs());
			addPageIOs(innerHashTable->getPageIOs());

			if (mustPurge)
			{
				purge();

				addTupleIOs(innerHashTable->getTupleIOs());
				addPageIOs(innerHashTable->getPageIOs());
			}
		}

		innerOp->close();
		// flush all frozen buckets
		innerHashTable->close(BucketStateFrozen);

		addTupleIOs(innerHashTable->getTupleIOs());
		addPageIOs(innerHashTable->getPageIOs());
		clock_t startTime = clock();
		//printf("Building hash tables for memory resident buckets.  # of frozen buckets: %d # of buckets: %d  Tuples: %d\n", frozen, numBuckets,getTuplesRead());
		for (int ii=frozen; ii < numBuckets; ii++)
		{
			hashtables[ii] = innerHashTable->buildHashTable(ii,keyType,keyIdxInner).get();
		//	printf("bucket %d: %d tuples\n",ii, innerHashTable->getTupleCount(ii));
		}
		//clock_t diff = clock() - startTime;

		//printf("Time to build hash tables: %d\n", diff);
		return true;
	}

	bool DynamicHashJoin::setupBucket()
	{
		int numLeft, numRight, numTuples;

		// Find a pair of buckets which both have some tuples in them
		while (true)
		{
			//if (hashTable.get())
			//{
			//	//	int probes = hashTable->getProbes();
			//	//	int avoided = hashTable->getProbesAvoided();

			//	//	printf("bucket %d numprobes %d probesavoided %d total %d\n", bucketNum - 1, probes, avoided, (probes + avoided));
			//	searches += hashTable->searches;
			//	hashTable = nullptr;
			//}

			if (bucketNum >= frozen)
				// Could not find a pair of non-empty frozen buckets to join
				return false;

			numLeft = innerHashTable->getTupleCount(bucketNum);
			numRight = outerHashTable->getTupleCount(bucketNum);

			if (numLeft > 0 && numRight > 0)
				break;

			bucketNum++;
		}

		//printf("Performing join of bucket: %d\n",bucketNum);
		
		FILE *buildFile;
		if (numRight < numLeft) {
			buildFile = openInputFile(outerHashTable->getFileName(bucketNum).c_str());
			probeFile = openInputFile(innerHashTable->getFileName(bucketNum).c_str());
			numTuples = numRight;
			leftBuild = false;

			probePage = std::shared_ptr<Page>( new Page(DB_PAGE_SIZE_DEFAULT, inner));
		} else {
			std::string innerFileName = innerHashTable->getFileName(bucketNum); 
			std::string outerFileName = outerHashTable->getFileName(bucketNum);
			buildFile = openInputFile(innerFileName.c_str());
			probeFile = openInputFile(outerFileName.c_str());
			numTuples = numLeft;
			leftBuild = true;

			probePage = std::shared_ptr<Page>( new Page(DB_PAGE_SIZE_DEFAULT, outer));
		}

		std::shared_ptr<Tuple> t(nullptr);
		std::shared_ptr<Page> p(nullptr);
		std::shared_ptr<Relation> tRel(nullptr);
		if (leftBuild)
		{
			tRel = inner;
			p = std::shared_ptr<Page>(new Page(DB_PAGE_SIZE_DEFAULT, inner));

			hashTable = std::shared_ptr<ChainedHashtable>(new ChainedHashtable(numTuples, keyIdxInner));
		}
		else
		{
			tRel = outer;
			p = std::shared_ptr<Page>(new Page(DB_PAGE_SIZE_DEFAULT, outer));
			hashTable = std::shared_ptr<ChainedHashtable>(new ChainedHashtable(numTuples,keyIdxOuter));
		}

		int numPagesRead = 0;
		while (p->read(buildFile) > 0)
		{
			numPagesRead++;

			p->initIterator();
			hashTable->addPage(p);
			while (p->hasNext())
			{
				t = std::shared_ptr<Tuple>(new Tuple(tRel));
				p->next(t);
				hashTable->insert(t);
			}

			// create a new page for reading into
			p = std::shared_ptr<Page>(new Page(DB_PAGE_SIZE_DEFAULT, p->getRelation()));
		}

		closeFile(buildFile);
		buildFile = nullptr;

		addTupleIOs(numTuples);
		addPageIOs(numPagesRead);

		//printf("Created hash table: %d Size: %d\n", bucketNum, numTuples);

		probePage->read(probeFile);
		probePage->initIterator();

		addTupleIOs(probePage->getTupleCount());
		incrementPageIOs();

		return true;
	}

	void DynamicHashJoin::cleanup()
	{
		innerHashTable->clear();
		outerHashTable->clear();
		hashTable = nullptr;
	}

	std::shared_ptr<Tuple> DynamicHashJoin::outputJoinTuple(std::shared_ptr<Tuple> left, std::shared_ptr<Tuple> right, std::shared_ptr<Tuple> output)
	{
		std::vector<std::shared_ptr<Tuple>> tuples;
		tuples.push_back(left);
		tuples.push_back(right);

		incrementTuplesOutput();
		return Tuple::join(tuples, getOutputRelation(),output);
	}
}
