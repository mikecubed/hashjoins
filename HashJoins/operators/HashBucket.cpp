#include "HashBucket.h"
#include "PageHashTable.h"
#include "../io/FileManager.h"
#include "../relational/Attribute.h"

#include <cstdio>
#include <cstdlib>
#include <cassert>


#include <sstream>

namespace db
{
	HashBucket::HashBucket(int bucketNum, short thePageSize, std::shared_ptr<Relation> rel)
		:bucketNumber(bucketNum),
		pageSize(thePageSize),
		relation(rel),
		state(BucketStateEmpty),
		tupCnt(0),
		pageCnt(0),
		resPageCnt(0),
		fileName(""),
		outFile(nullptr)
	{
	}

	HashBucket::~HashBucket()
	{

	}

	BucketState HashBucket::getState()
	{
		return state;
	}

	void HashBucket::setState(BucketState newState)
	{
		state = newState;
	}

	int HashBucket::getPageCount()
	{
		return pageCnt;
	}

	int HashBucket::getTupleCount()
	{
		return tupCnt;
	}

	std::string HashBucket::getFileName()
	{
		return fileName;
	}

	std::list<std::shared_ptr<Page>> HashBucket::getPageList()
	{
		return pages;
	}

	void HashBucket::clearPageList()
	{
		pages.clear();
		tupCnt = 0;
		pageCnt = 0;
		resPageCnt = 0;
		state = BucketStateEmpty;

		if (fileName != "")
		{
			deleteFile(fileName.c_str());
		}
	}

	void HashBucket::SHARPClose(PageHashTable &pht)
	{
		// flush a bucket and close it but don't clear its memory yet...
		// needed for SHARP join

		std::stringstream bucketHint;
		bucketHint << "bucket" << bucketNumber;
		fileName = createTempFileName(bucketHint.str());
		outFile = openOutputFile(fileName.c_str());

		assert(outFile && "temporary output file was not opened");

		for (auto& page : pages)
		{
			page->write(outFile);
			pht.incrementPageIOs();
			pht.addTupleIOs(page->getTupleCount());
		}
		closeFile(outFile);
	}

	void HashBucket::flush(PageHashTable &pht)
	{
		for (auto& page : pages)
		{
			page->write(outFile);
			pht.incrementPageIOs();
			pht.addTupleIOs(page->getTupleCount());
		}

		pages.clear();
		resPageCnt = 0;
	}

	void HashBucket::close(PageHashTable &pht)
	{
		flush(pht);
		closeFile(outFile);
	}

	void HashBucket::purge(PageHashTable &pht)
	{
		std::stringstream bucketHint;
		bucketHint << "bucket" << bucketNumber;
		fileName = createTempFileName(bucketHint.str());
		outFile = openOutputFile(fileName.c_str());

		assert(outFile && "temporary output file was not opened");

		if (pages.size() > 0)
		{
			auto lastPage = pages.back();
			pages.pop_back();
			pht.usedPages -= (resPageCnt - 1);

			for (auto page : pages)
			{
				page->write(outFile);
				pht.incrementPageIOs();
				pht.addTupleIOs(page->getTupleCount());
			}

			pages.clear();
			pages.push_back(lastPage);
		}
		else
		{
			// flushed an empty partition
			auto p = std::shared_ptr<Page>(new Page(pageSize, relation));
			pages.push_back(p);
			pht.usedPages++;
			pageCnt = 1;
		}

		resPageCnt = 1;
		state = BucketStateFrozen;
	}

	void HashBucket::init(PageHashTable &pht, BucketState theState)
	{
		std::stringstream bucketHint;
		bucketHint << "bucket" << bucketNumber;
		fileName = createTempFileName(bucketHint.str());
		outFile = openOutputFile(fileName.c_str());

		state = theState;
		if (BucketStateFrozen == state)
		{
			auto page = std::shared_ptr<Page>(new Page(pageSize,relation));
			pages.push_back(page);
			pageCnt = 1;
			resPageCnt = 1;
			pht.usedPages ++;
		}
	}

	bool HashBucket::add(PageHashTable &pht, std::shared_ptr<Tuple> t)
	{
		bool mustPurge = false;
		auto page = std::shared_ptr<Page>(nullptr);

		switch (state)
		{
		case BucketStateEmpty:
			{
				page = std::shared_ptr<Page>(new Page(pageSize, relation));
				pages.push_back(page);
				pageCnt = 1;
				resPageCnt = 1;
				pht.usedPages++;
				state = BucketStateExpanding;
			}
			break;
		case BucketStateFrozen:
			{
				page = pages.back();
				if (!page->hasSpace(t))
				{
					pht.incrementPageIOs();
					pht.addTupleIOs(page->getTupleCount());
					page->flush(outFile);
				}
			}
			break;
		case BucketStateExpanding:
			{
				page = pages.back();

				if (!page->hasSpace(t))
				{
					page = std::shared_ptr<Page> (new Page(pageSize,relation));
					pages.push_back(page);
					pageCnt++;
					resPageCnt++;
					pht.usedPages++;
				}

				if (pht.usedPages > pht.maxPages)
				{
					mustPurge = true;
				}
			}
			break;
		default:
			// nothing
			break;
		}

		page->addTuple(t);
		tupCnt++;

		return mustPurge;
	}

	std::shared_ptr<ChainedHashtable> HashBucket::buildHashTable(PredicateKeyType keyType, std::vector<int> keyIdx)
	{
		hashTable = std::shared_ptr<ChainedHashtable>(new ChainedHashtable(tupCnt, keyIdx));

		if (BucketStateEmpty == state)
		{
			return hashTable;
		}

		int tcount = 0;
		for (auto& page : pages)
		{
			for (int tupleIdx = 0; tupleIdx < page->getTupleCount(); ++tupleIdx)
			{
				std::shared_ptr<Tuple> t(new Tuple(relation));
				page->getTuple(tupleIdx, t);
				hashTable->insert(t);
				++tcount;
			}
		}

		return hashTable;
	}

	std::vector<std::shared_ptr<Tuple>> HashBucket::probeHashTable(int key)
	{
		return hashTable->find(key);
	}

	std::vector<std::shared_ptr<Tuple>> HashBucket::probeHashTable(const std::string& key)
	{
		return hashTable->find(key);
	}

	std::vector<std::shared_ptr<Tuple>> HashBucket::probeHashTable(std::shared_ptr<Tuple> key, const std::vector<int>& keyIdx)
	{
		return hashTable->find(key, keyIdx);
	}

}
