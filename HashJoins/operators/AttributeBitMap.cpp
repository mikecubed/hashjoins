#include "AttributeBitMap.h"

namespace db
{
	AttributeBitMap::AttributeBitMap(unsigned int numEntries)
		:AttributeMap(numEntries),
		bitmap(std::vector<bool>(size, 0))
	{
	}

	AttributeBitMap::~AttributeBitMap(void)
	{
	}

	// note that we do not need the value for the bitmap
	void AttributeBitMap::setValueForHash(unsigned int value, unsigned int hash)
	{
		int idx = (hash + 1) % size;

		bitmap[idx] = true;
	}

	unsigned int AttributeBitMap::valueForHash(unsigned int hash)
	{
		int idx = (hash + 1) % size;

		return bitmap[idx];
	}
}
