
#include "PageHashTable.h"
#include "../util/hashfunc.h"

#include <cstdlib>
#include <cstdio>
#include <cassert>

//TODO: Make sure we are using pagesize
namespace db 
{
	
	PageHashTable::PageHashTable(int tableSize, short inPageSize, int inMaxPages, std::shared_ptr<Relation> r)
		:pageSize(inPageSize),
		maxPages(inMaxPages),
		usedPages(0),
		relation(r)
	{
		tupleSize = relation->computeTupleSize();
		assert(tupleSize > 0);
		tuplesPerPage = pageSize / tupleSize;

		for (int i = 0; i < tableSize; ++i)
		{
			buckets.push_back(std::shared_ptr<HashBucket>(new HashBucket(i,pageSize, relation)));
		}

		tupleIOs = 0;
		pageIOs = 0;
	}

	PageHashTable::~PageHashTable()
	{

	}

	void PageHashTable::clear()
	{
		for (auto& bucket : buckets)
		{
			bucket->clearPageList();
		}
	}

	void PageHashTable::flush(BucketState state)
	{
		tupleIOs = 0;
		pageIOs = 0;

		for (auto& bucket : buckets)
		{
			if (bucket->getState() == state)
			{
				bucket->flush(*this);
			}
		}
	}

	void PageHashTable::SHARPClose()
	{
		tupleIOs = 0;
		pageIOs = 0;

		for (auto& bucket : buckets)
		{
			if (bucket->getState() == BucketStateExpanding)
			{
				bucket->SHARPClose(*this);
			}
		}
	}

	void PageHashTable::close(BucketState state)
	{
		tupleIOs = 0;
		pageIOs = 0;

		for (auto& bucket : buckets)
		{
			if (bucket->getState() == state)
			{
				bucket->close(*this);
			}
		}
	}

	void PageHashTable::purge(int bucketIdx)
	{
		tupleIOs = 0;
		pageIOs = 0;

		buckets[bucketIdx]->purge(*this);
	}

	int PageHashTable::getProbeCount(int bucket)
	{
		return buckets[bucket]->hashTable->getProbes();
	}

	int PageHashTable::getSearchCount(int bucket)
	{
		return buckets[bucket]->hashTable->searches;
	}

	int PageHashTable::getAvoidedProbeCount(int bucket)
	{

		return buckets[bucket]->hashTable->getProbesAvoided();
	}

	std::shared_ptr<ChainedHashtable> PageHashTable::buildHashTable(int bucketIdx, PredicateKeyType keyType, const std::vector<int>& keyIdx)
	{
		return buckets[bucketIdx]->buildHashTable(keyType, keyIdx);
	}

	int PageHashTable::getHashLocation(int key)
	{
		return hash(key, buckets.size());
	}

	int PageHashTable::getHashLocation(const std::string& key)
	{
		return hash(key, buckets.size());
	}

	int PageHashTable::getHashLocation(std::shared_ptr<Tuple> key, const std::vector<int>& keyIdx)
	{
		int hashValue;
		int retVal = hash(key, buckets.size(), keyIdx, hashValue);
		key->setHash(hashValue);
		return retVal;
	}

	int PageHashTable::getUsedPages()
	{
		return usedPages;
	}

	int PageHashTable::getPageCount(int bucket)
	{
		assert(bucket >= 0 && static_cast<std::size_t>(bucket) < buckets.size());

		return buckets[bucket]->getPageCount();
	}

	int PageHashTable::getTupleCount(int bucket)
	{
		assert(bucket >= 0 && static_cast<std::size_t>(bucket) < buckets.size());

		return buckets[bucket]->getTupleCount();
	}

	std::string PageHashTable::getFileName(int bucket)
	{
		assert(bucket >= 0 && static_cast<std::size_t>(bucket) < buckets.size());

		return buckets[bucket]->getFileName();
	}
	bool PageHashTable::insert(std::shared_ptr<Tuple> tuple, int bucket)
	{
		assert(bucket >= 0 && static_cast<std::size_t>(bucket) < buckets.size());

		tupleIOs = 0;
		pageIOs = 0;

		return buckets[bucket]->add(*this, tuple);
	}

	bool PageHashTable::insert(std::shared_ptr<Tuple> key, const std::vector<int>& keyIdx)
	{
		int bucket = getHashLocation(key,keyIdx);
		assert(bucket >= 0 && static_cast<std::size_t>(bucket) < buckets.size());

		tupleIOs = 0;
		pageIOs = 0;

		return buckets[bucket]->add(*this, key);
	}

	bool PageHashTable::insert(std::string key, std::shared_ptr<Tuple> t)
	{
		int bucket = getHashLocation(key);
		assert(bucket >= 0 && static_cast<std::size_t>(bucket) < buckets.size());

		tupleIOs = 0;
		pageIOs = 0;

		return buckets[bucket]->add(*this, t);
	}

	bool PageHashTable::insert(int key, std::shared_ptr<Tuple> t)
	{
		int bucket = getHashLocation(key);

		assert(bucket >= 0 && static_cast<std::size_t>(bucket) < buckets.size());

		tupleIOs = 0;
		pageIOs = 0;

		return buckets[bucket]->add(*this, t);
	}

	std::list<std::shared_ptr<Page>> PageHashTable::getPageList(int bucket)
	{
		assert(bucket >= 0 && static_cast<std::size_t>(bucket) < buckets.size());

		return buckets[bucket]->getPageList();
	}

	void PageHashTable::setBucketState(int bucket, BucketState state)
	{
		assert(bucket >= 0 && static_cast<std::size_t>(bucket) < buckets.size());
		buckets[bucket]->setState(state);
	}

	void PageHashTable::initBucket(int bucket, BucketState state)
	{
		assert(bucket >= 0 && static_cast<std::size_t>(bucket) < buckets.size());
		buckets[bucket]->init(*this, state);
	}

	std::vector<std::shared_ptr<Tuple>> PageHashTable::probeHashTable(int bucket, int key)
	{
		assert(bucket >= 0 && static_cast<std::size_t>(bucket) < buckets.size());
		return buckets[bucket]->probeHashTable(key);
	}

	std::vector<std::shared_ptr<Tuple>> PageHashTable::probeHashTable(int bucket, const std::string& key)
	{
		assert(bucket >= 0 && static_cast<std::size_t>(bucket) < buckets.size());
		return buckets[bucket]->probeHashTable(key);
	}

	std::vector<std::shared_ptr<Tuple>> PageHashTable::probeHashTable(int bucket, std::shared_ptr<Tuple> key, const std::vector<int>& keyIdx)
	{
		//assert(bucket >= 0 && static_cast<std::size_t>(bucket) < buckets.size());
		return buckets[bucket]->probeHashTable(key, keyIdx);
	}

	int PageHashTable::getTupleIOs()
	{
		return tupleIOs;
	}

	int PageHashTable::getPageIOs()
	{
		return pageIOs;
	}

	void PageHashTable::incrementTupleIOs()
	{
		tupleIOs++;
	}

	void PageHashTable::addTupleIOs(int numTuples)
	{
		tupleIOs += numTuples;
	}

	void PageHashTable::incrementPageIOs()
	{
		pageIOs++;
	}
}

