#ifndef ATTRIBUTEMAP_H
#define ATTRIBUTEMAP_H

namespace db {

	class AttributeMap
	{
	public:
		AttributeMap(unsigned int size);
		virtual ~AttributeMap(void);

		virtual void setValueForHash(unsigned int value, unsigned int hash)=0;
		virtual unsigned int valueForHash(unsigned int hash) = 0;

		int getSize();
	protected:
		unsigned int size; // number of entries...
	};

}

#endif //ATTRIBUTEMAP_H
