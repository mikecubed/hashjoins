#include "ChainedHashtable.h"
#include "../util/hashfunc.h"

#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cassert>

namespace db
{
	ChainedHashtable::ChainedHashtable(int size, const std::vector<int>& keyIdx)
		:buckets(std::vector<std::list<std::shared_ptr<Tuple>>>()),
		keyIndex(keyIdx),
		currBucket(0),
		pages(std::list<std::shared_ptr<Page>>()),
		probes(0),
		probesAvoided(0),
		searches(0)
	{
		tableSize = nextPrime(size);
		buckets.reserve(tableSize);
		for (int ii = 0; ii < tableSize; ++ii)
		{
			buckets.push_back(std::list<std::shared_ptr<Tuple>>());
		}
		
	}

	ChainedHashtable::~ChainedHashtable()
	{
		buckets.clear();
	}

	void ChainedHashtable::clear()
	{
		for (auto& bucket : buckets)
		{
			bucket.clear();
		}
	}

	void ChainedHashtable::insert(std::shared_ptr<Tuple> tuple)
	{
		int hashValue;
		int bucketIndex = hash(tuple, tableSize, keyIndex, hashValue);
		tuple->setHash(hashValue);
		buckets[bucketIndex].push_back(tuple);
	}

	void ChainedHashtable::printBuckets()
	{
		int bIdx = 0;
		printf("Tuple keys: \n");
		for (auto &bucket : buckets)
		{
			printBucket(bIdx++);
		}
	}

	void ChainedHashtable::printBucket(int index)
	{
		assert(index >= 0 && index < tableSize);

		auto &bucket = buckets[index];

		int tIdx = 0;
		for (auto &tuple : bucket)
		{
			// assuming int key...
			int tVal = tuple->getInt(keyIndex[0]);
			printf("[%d][%d] = %d\n", index, tIdx++, tVal);
		}

	}

	std::list<std::shared_ptr<Tuple>>& ChainedHashtable::getBucket(int key)
	{
		int bucketIdx = hash(key, tableSize);
		return buckets[bucketIdx];
	}

	std::list<std::shared_ptr<Tuple>>& ChainedHashtable::getBucket(const std::string& key)
	{
		int bucketIdx = hash(key, tableSize);
		return buckets[bucketIdx];
	}

	std::list<std::shared_ptr<Tuple>>& ChainedHashtable::getBucket(std::shared_ptr<Tuple> key, const std::vector<int>& keyIdx)
	{
		int bucketIdx = hash(key, tableSize, keyIdx);
		return buckets[bucketIdx];
	}

	std::vector<std::shared_ptr<Tuple>> ChainedHashtable::find(int key)
	{
		auto& bucket = getBucket(key);
		
		std::vector<std::shared_ptr<Tuple>> results;

		for (auto& tuple : bucket)
		{
			//Todo: optimize this special case
			int tVal = tuple->getInt(keyIndex[0]);
			if (tVal == key)
				results.push_back(tuple);
		}

		if(results.size() > 1)
		{

			printf("multi result: %d\n", results.size());
		}
		return results;
	}

	std::vector<std::shared_ptr<Tuple>> ChainedHashtable::find(const std::string& key)
	{
		int bucketIdx = hash(key,tableSize);
		auto& bucket = buckets[bucketIdx];

		std::vector<std::shared_ptr<Tuple>> results;
		for (auto& tuple : bucket)
		{
			if (tuple->getString(keyIndex[0]) == key)
				results.push_back(tuple);
		}

		return results;
	}

	bool tuplesMatch(std::shared_ptr<Tuple> keyTuple,std::shared_ptr<Relation> keyTupleRel, std::shared_ptr<Tuple> tuple,std::shared_ptr<Relation> tableTupleRel,const std::vector<int>& keyIndex, const std::vector<int>& keyIdx) 
	{
		bool matches = true;

		// compare all key parts
		for (std::size_t ii = 0; ii < keyIdx.size(); ++ii)
		{
			int keyTupleIdx = keyIdx[ii];
			int tableTupleIdx = keyIndex[ii];

			//if (!tuple->isNull(tableTupleIdx))
			{
				DB_TYPE attributeType = keyTupleRel->getAttributeType(keyTupleIdx);
				assert(attributeType == tableTupleRel->getAttributeType(tableTupleIdx));

				if (attributeType == DB_TYPE_INT)
				{
					if (keyTuple->getInt(keyTupleIdx) != tuple->getInt(tableTupleIdx))
					{
						matches = false;
						break;
					}
				}
				else if (attributeType == DB_TYPE_STRING)
				{
					if (keyTuple->getString(keyTupleIdx) != tuple->getString(tableTupleIdx))
					{
						matches = false;
						break;
					}
				}
				else
				{
					//TODO: implement other data types
					assert(0);
				}
			}
			//else
			//{
			//	if ( ! keyTuple->isNull(keyTupleIdx))
			//	{
			//		matches = false;
			//		break;
			//	}
			//}

		}
		return matches;
	}

	std::vector<std::shared_ptr<Tuple>> ChainedHashtable::find(std::shared_ptr<Tuple> keyTuple, const std::vector<int>& keyIdx)
	{
		searches++;
		int hashValue;
		int bucketIdx = hash(keyTuple,tableSize, keyIdx, hashValue);
		auto& bucket = buckets[bucketIdx];

		std::vector<std::shared_ptr<Tuple>> results;
		
		assert(keyIdx.size() == keyIndex.size());
		
		//std::shared_ptr<Relation> keyTupleRel = keyTuple->getRelation();
		//std::shared_ptr<Relation> tableTupleRel(nullptr);

		for (auto& tuple : bucket)
		{
			//if (!tableTupleRel)
			//	tableTupleRel = tuple->getRelation();

			
			// first check if the tuple hashes match so we don't need to check each join attribute
			// if we know it wont join
			if (hashValue == tuple->hash)//getHash())
			{
				probes++;
				//bool matches = tuplesMatch(keyTuple,keyTupleRel,tuple,tableTupleRel,keyIndex,keyIdx);
					//true;
				bool matches = true;

				// compare all key parts
				for (std::size_t ii = 0; ii < keyIdx.size(); ++ii)
				{
					int keyTupleIdx = keyIdx[ii];
					int tableTupleIdx = keyIndex[ii];

					//if (!tuple->isNull(tableTupleIdx))
					{
						//DB_TYPE attributeType = keyTupleRel->getAttributeType(keyTupleIdx);
						//assert(attributeType == tableTupleRel->getAttributeType(tableTupleIdx));
					
						//if (attributeType == DB_TYPE_INT)
						{
							if (keyTuple->getInt(keyTupleIdx) != tuple->getInt(tableTupleIdx))
							{
								matches = false;
								break;
							}
						}
						//else if (attributeType == DB_TYPE_STRING)
						//{
						//	if (keyTuple->getString(keyTupleIdx) != tuple->getString(tableTupleIdx))
						//	{
						//		matches = false;
						//		break;
						//	}
						//}
						//else
						//{
						//	//TODO: implement other data types
						//	assert(0);
						//}
					}
					//else
					//{
					//	if ( ! keyTuple->isNull(keyTupleIdx))
					//	{
					//		matches = false;
					//		break;
					//	}
					//}

				}

				if (matches)
					results.push_back(tuple);
			}
			else
			{

				probesAvoided++;
			}
		}

		return results;
	}

	void ChainedHashtable::addPage(std::shared_ptr<Page> p)
	{
		pages.push_back(p);
	}
}
