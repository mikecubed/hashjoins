
#include <vector>
#include "relational/Attribute.h"

namespace db {

	typedef enum TPCHSIZE_T
	{
		TPCHSize100M = 1,
		TPCHSize1G,
		TPCHSize10G
	} TPCHSize;

void convertTextFile(const std::string& inFile, const std::string& outFile, const std::vector<db::Attribute>& attrs1);
std::vector<db::Attribute> getCustomerAttrs();
std::vector<db::Attribute> getPartAttrs();
std::vector<Attribute> getPartSuppAttrs();
std::vector<db::Attribute> getOrderAttrs();
std::vector<db::Attribute> getLineitemAttrs();
int testCO();
int testOL();
int testCOL(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
int testCOL_rightDeep(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
int testCOL_GHT(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
int testCO_HTJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
int testCCC_HTJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
int testCCC_DHJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
int testOOO_HTJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
int testOOO(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
int testOOO_rightDeep(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
int testOPL_DHJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
int testOPL_SHARP(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
int testPPSL_DHJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
int testPPSL_HTJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize= TPCHSize100M);
int testOPSL_DHJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
int testOPSL_SHARP(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
int testOOL_DHJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
int testOOL_SHARP(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);

int testOL_DHJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
int testPSL_DHJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize = TPCHSize100M);
};
