
for ($algo = 0; $algo -le 2; $algo++)
{
    for ($memSize = 12000; $memSize -ge 1000; $memSize = $memSize - 1000)
    {
        for ($i=1; $i -le 5; $i++)
        {
            echo "Testing $algo, $memSize, $i"
            .\HashJoins.exe $algo 2 $memSize >> hashjoinstest.txt
        }
    }
}
