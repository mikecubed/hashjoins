
for ($algo = 0; $algo -le 2; $algo++)
{
    for ($memSize = 900; $memSize -ge 100; $memSize = $memSize - 100)
    {
        for ($i=1; $i -le 5; $i++)
        {
            echo "Testing $algo, $memSize, $i"
            .\HashJoins.exe $algo 2 $memSize >> genhashteams-smaller.csv
        }
    }
}
