#pragma once
/*
FILE:			equijoinpredicate.h
PROVIDES: 		Used by hash and merge based joins to express an equi-join between tuples on one or more pairs of attributes.
C CODE BY:		Michael Henderson
CREATION DATE:	September 2007
BASED ON:		unity.predicates.EquiJoinPredicate.java
PROGRAM BY:     Ramon Lawrence (ramon.lawrence@ubc.ca)
*/
#ifndef equijoinpredicate_h__
#define equijoinpredicate_h__

#include "../relational/Tuple.h"

#include <vector>
namespace db
{
	typedef enum
	{
		JoinCardinalityUnknown = 0,
		JoinCardinalityOneToOne,
		JoinCardinalityOneToMany,
		JoinCardinalityManyToOne,
		JoinCardinalityManyToMany
	} JoinCardinality;

	typedef enum PredicateKeyType : int
	{
		PredicateKeyTypeUnknown = 0,
		PredicateKeyTypeInt = 1,
		PredicateKeyTypeString,
		PredicateKeyTypeObject
	} PredicateKeyType;

	class EquijoinPredicate
	{
	public:
		EquijoinPredicate(std::vector<int> attr1Loc, std::vector<int> attr2Loc, PredicateKeyType keyType, JoinCardinality cardinality);
		~EquijoinPredicate(void);

		// function declarations
		//int db_equijoinpredicate_inversePredicate(db_equijoinpredicate *ejp, db_equijoinpredicate *inv); // take eqj and place inverse into inv
		//int db_equijoinpredicate_isLessThan(db_equijoinpredicate *ejp, db_tuple *t1, db_tuple *t2);
		//int db_equijoinpredicate_isGreaterThan(db_equijoinpredicate *ejp, db_tuple *t1, db_tuple *t2);
		//int db_equijoinpredicate_isEqual(db_equijoinpredicate *ejp, db_tuple *t1, db_tuple *t2);
		//int db_equijoinpredicate_compare(db_equijoinpredicate *ejp, db_tuple *t1, db_tuple *t2);

		// accessors
		std::size_t getNumAttributes();
		std::vector<int> getRelation1Locs();
		std::vector<int> getRelation2Locs();
		PredicateKeyType getKeyType();
		JoinCardinality getJoinCardinality();

		//void setJoinCardinality(JoinCardinality joinCard);


	private:
		std::vector<int> rel1AttrLocations;	///< Attribute indexes in relation 1
		std::vector<int> rel2AttrLocations;	///< Attribute indexes in relation 2
		PredicateKeyType keyType;			///< Type of key
		JoinCardinality cardinality;		///< Cardinality of the join. for 1:N - N side is relation 2, N:1 its rel 1
	};

}

#endif // equijoinpredicate_h__
