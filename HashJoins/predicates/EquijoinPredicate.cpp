#include "EquijoinPredicate.h"

namespace db
{
	EquijoinPredicate::EquijoinPredicate(std::vector<int> attr1Loc, std::vector<int> attr2Loc, PredicateKeyType inKeyType, JoinCardinality inCardinality)
		:rel1AttrLocations(attr1Loc),
		rel2AttrLocations(attr2Loc),
		keyType(inKeyType),
		cardinality(inCardinality)
	{
	}

	EquijoinPredicate::~EquijoinPredicate(void)
	{
	}

	std::size_t EquijoinPredicate::getNumAttributes()
	{
		return rel1AttrLocations.size();
	}

	std::vector<int> EquijoinPredicate::getRelation1Locs()
	{
		return rel1AttrLocations;
	}

	std::vector<int> EquijoinPredicate::getRelation2Locs()
	{
		return rel2AttrLocations;
	}

	PredicateKeyType EquijoinPredicate::getKeyType()
	{
		return keyType;
	}

	JoinCardinality EquijoinPredicate::getJoinCardinality()
	{
		return cardinality;
	}
}

