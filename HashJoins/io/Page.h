#ifndef DB_PAGE_H
#define DB_PAGE_H

#include "../relational/Tuple.h"
#include "../relational/Relation.h"

#include <cstdio>
#include <memory>

#define DB_PAGE_SIZE_DEFAULT 4096
#define DB_PAGE_FIRST_OFFSET 4
#define DB_PAGE_FREE_BYTE_OFFSET 2

namespace db
{

/* 
Note: Page memory is filled from the bottom up

block header (variable length starts at beginning of page memory:
# Bytes | Desc
	2	| # of tuples in page
	2	| offset for first free byte
 2 each | offset for each tuple
*/

	class Page
	{
	public:
		Page(short pageSize, std::shared_ptr<Relation> rel);
		~Page(void);

		void initIterator();
		bool hasNext();

		// pass in a tuple to avoid a new malloc
		// returns tuple on success, null on failure
		// will replace data in the tuple
		// NOTE: The tuple data will point inside the page.  If you need to keep the data after the page is gone you will need to copy the data to a new area of memory
		std::shared_ptr<Tuple> next(std::shared_ptr<Tuple> tuple);

		// statistical functions
		long long getReadCount();
		long long getWriteCount();
		long long getIOCount();
		void resetCounts();
		void incNumReads();
		void incNumWrites();

		//TODO: PageIterator if needed

		bool addTuple(std::shared_ptr<Tuple> t);
		int read(FILE *in);
		bool write(FILE *out); // write but not remove
		int flush(FILE *out); // write and remove (destruct tuples)
		int getTupleCount();
		int getFreeSpace();  // return amount of space available in the page
		int hasSpace(std::shared_ptr<Tuple> t); // can t fit in the page?
		bool getTuple(int i, std::shared_ptr<Tuple> t);
		std::shared_ptr<Relation> getRelation(); 
	private:
		char *data;								// Array of tuples
		short pageSize;							// size of a page
		int pageId;								// Unique page identifier
		std::shared_ptr<Relation> relation;		// Schema of tuples stored in this page
		//db_page *next;							// Next page (if used in linked list of pages)
		//db_page *prev;							// Previous page (if used in linked list of pages)
		int curPos;								// Current iterator position
		
		void clear();
		int getNextFreeByte();
	};

}

#endif // DB_PAGE_H
