/**
\file			FileManager.cpp

Provides functions to perform file operations such as opening and closing files.

\author Michael Henderson
\copyright 2012 Michael Henderson
*/
#ifndef filemanager_h__
#define filemanager_h__

#include <cstdio>
#include <string>

namespace db
{
	static const int MAX_PATHNAME_LENGTH = 256;
	// open file will return a FILE pointer or NULL
	FILE *openTextInputFile(const char *fname);
	FILE *openTextOutputFile(const char *fname);
	FILE *openInputFile(const char *fname);
	FILE *openOutputFile(const char *fname);
	FILE *appendOutputFile(const char *fname);
	int fileEndOfFile(FILE *fp);

	void closeFile(FILE *fp);

	// expects outBuffer to be 257 (MAX_PATHNAME_LENGTH + 1) bytes
	// returns 0 on failure
	int getFileName(char *pathName, char *outBuffer);
	int getPath(char *pathName, char *outBuffer);
	std::string createTempFileName(std::string hint);

	int deleteFile(const char *fname);

}

#endif // filemanager_h__

