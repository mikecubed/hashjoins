#include "Page.h"
#include "../util/convert.h"

#include <cstdio>
#include <string>

namespace db
{
	static int g_lastPageId = 0;
	static long long g_numReads = 0;
	static long long g_numWrites = 0;

	int Page::getNextFreeByte()
	{
		if (data)
			return byteToShortOffset(data, 2);
		return -1;
	}

	Page::Page(short ps, std::shared_ptr<Relation> rel)
		:relation (rel),
		data(nullptr),
		pageSize(ps),
		pageId(g_lastPageId++),
		curPos(0)
	{
		data = new char[pageSize];

		if (data)
		{
			shortToByteOffset(0, data, 0); // # of tuples
			shortToByteOffset(pageSize - 1, data, 2); // first free byte
		}
		else
			throw std::string("No Memory fail!");
	}

	Page::~Page(void)
	{
		//printf("destroying page %d\n", pageId);
		if (data)
			delete data;
	}

	void Page::initIterator()
	{
		curPos = 0;
	}

	bool Page::hasNext()
	{
		if (data)
			return (curPos < getTupleCount());

		return false;
	}

	// pass in a tuple struct to avoid a new malloc
	// returns true on success, false on failure
	// NOTE: The tuple data will point inside the page.  If you need to keep the data after the page is gone you will need to copy the data to a new area of memory
	std::shared_ptr<Tuple> Page::next(std::shared_ptr<Tuple> tuple)
	{
		if (data && hasNext())
		{
			if (!tuple)
				tuple = std::shared_ptr<Tuple>(new Tuple(relation));
			int offset = DB_PAGE_FIRST_OFFSET + (curPos  *2);
			std::size_t dataByteIdx = byteToShortOffset(data,offset);
			tuple->setData(&data[dataByteIdx]);
			curPos++;
			return tuple;
		}
		return nullptr;
	}

	// statistical functions
	long long Page::getReadCount() {return g_numReads;}
	long long Page::getWriteCount() {return g_numWrites;}
	long long Page::getIOCount() {return g_numWrites + g_numReads;}
	void Page::resetCounts()
	{
		g_numReads = g_numWrites = 0;
	}

	void Page::incNumReads() {g_numReads++;}
	void Page::incNumWrites() {g_numWrites++;}

	//TODO: PageIterator if needed

	bool Page::addTuple(std::shared_ptr<Tuple> t)
	{
		if (data && t && t->data)
		{
			std::size_t tupSize = t->getSize();
			std::size_t offset = getNextFreeByte() - tupSize;
			short int numTuples = (short)getTupleCount();
			if ((tupSize < 1) || getFreeSpace() <  tupSize ) {
				// Not enough room for the tuple or tuple contains no data
				return false;
			}

			// add the tuple data to the page
			memcpy(&data[offset], t->data, tupSize);

			// add the offset for the tuple
			shortToByteOffset((short)offset,data,numTuples*2 + DB_PAGE_FIRST_OFFSET);

			// increment # of tuples
			shortToByteOffset(numTuples+1, data, 0);

			// update the offset for next free byte
			shortToByteOffset((short)(offset - 1), data,2);
			return true;
		}
		return false;
	}

	int Page::read(FILE *in)
	{
		if (in && data) {
			int readBytes = (int)fread(data,pageSize,1,in);

			if (readBytes == 1) {	
				return getTupleCount();
			} 

			if (!feof(in)) {
				// some other error occurred 
				readBytes = ferror ( in);
				perror("db_page_read");
			}
			else
			{
				data[0] = 0;
				data[1] = 0;
			}

			return readBytes;
		}
		return 0;
	}

	bool Page::write(FILE *out) // write but not remove
	{
		if (data && out) {
			if ( fwrite(data,sizeof(char),pageSize,out) == (size_t)pageSize) {
				//FIXME: do we want to flush the output buffer here or not?
				//fflush(out);
				return true;
			}	
			// else 
			fprintf(stderr,"db_page_write: ERROR failed to write page to disk\n");
		}
		return false;
	}

	void Page::clear()
	{
		if (data)
		{
			shortToByteOffset(0,data, 0);
			shortToByteOffset(pageSize -1, data, 2);
		}
	}

	int Page::flush(FILE *out) // write and remove (destruct tuples)
	{
		if (data)
		{
			bool retVal = write(out);
			clear();
			return retVal;
		}
		return false;
	}

	int Page::getTupleCount()
	{
		if (data)
			return byteToShortOffset(data, 0);
		return 0;
	}

	int Page::getFreeSpace()  // return amount of space available in the page
	{
		if (data) {
			int offset = byteToShortOffset(data,2);
			return offset - (getTupleCount() * 2 + DB_PAGE_FIRST_OFFSET);
		}
		return 0;
	}

	int Page::hasSpace(std::shared_ptr<Tuple> t) // can t fit in the page?
	{
		if (data && t) {
			return (getFreeSpace() >= (int)(t->getSize()) + 2);
		}
		return 0;
	}

	bool Page::getTuple(int i,std::shared_ptr<Tuple> t)
	{
		if (data && i < getTupleCount() && i >= 0) {

			if (!t)
			{
				t = std::shared_ptr<Tuple>(new Tuple(relation));
			}
			t->setData(&data[byteToShortOffset(data,DB_PAGE_FIRST_OFFSET + (i * 2))]);
			
			return true;
		}
		return false;
	}

	std::shared_ptr<Relation> Page::getRelation()
	{
		return relation;
	}
}
