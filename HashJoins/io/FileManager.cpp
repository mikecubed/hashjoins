#include "FileManager.h"

#include <ctime>
#include <cstring>
#include <cstdlib>
#include <sstream>

namespace db
{
	FILE *openTextInputFile(const char *fname)
	{
		return fopen(fname, "r");
	}

	FILE *openInputFile(const char *fname)
	{
		return fopen(fname, "rb");
	}

	FILE *openTextOutputFile(const char *fname)
	{
		return fopen(fname,"w");
	}

	FILE *openOutputFile(const char *fname)
	{
		return fopen(fname,"wb");
	}

	FILE *appendOutputFile(const char *fname)
	{
		return fopen(fname,"a");
	}

	void closeFile(FILE *fp)
	{
		if (fp != NULL) {
			fflush(fp);
			fclose(fp);
		}
	}

	// expects outBuffer to be 257 (MAX_PATHNAME_LENGTH + 1) bytes
	// returns 0 on failure
	int getFileName(char *pathName, char *outBuffer)
	{
		if (pathName != NULL && outBuffer != NULL) {
			int idx = static_cast<int>(strlen(pathName));
			while (idx >= 0) {
				if (pathName[idx] == '/') {
					break;
				}
				idx--;
			}
			if (idx < 0)
				idx = 0;
			// copy the file name into the buffer
			strcpy(outBuffer,pathName + (idx + 1));
			return 1;
		}
		return 0;
	}

	int getPath(char *pathName, char *outBuffer)
	{
		if (pathName != NULL && outBuffer != NULL) {
			int idx = (int)strlen(pathName);
			while (idx >= 0) {
				if (pathName[idx] == '/') {
					break;
				}
				idx--;
			}
			// if the filename doesn't contain the path...
			if (idx < 0) {
				outBuffer[0] = '\0';
				return 1;
			}

			// copy the path into the buffer
			strncpy(outBuffer,pathName, (idx + 1));
			outBuffer[idx + 1] = '\0';
			return 1;
		}
		return 0;
	}

	std::string createTempFileName(std::string hint)
	{
		static unsigned short variance = 0; // since using a higher precision clock measurement is not portable...

		clock_t ticks = clock();


		if (hint.length() < 1)
		{
			hint = "tmp";
		}

		std::stringstream fileStream;
		fileStream << "/tmp/hashjoins/" << hint << "_" << ticks << "_" << variance << ".dat";
		std::string fileName = fileStream.str();
		variance++;

		return fileName;
	}

	int deleteFile(const char *fname)
	{
		return remove(fname);
	}

	int fileEndOfFile(FILE *fp)
	{
		if (fp != NULL) {
			return feof(fp);
		}
		return 1; // default to end of file
	}

	int testFileManager()
	{
		const char *fname = "/tmp/EarlyJoin/tmp.txt";
		FILE *fp = openOutputFile(fname);
		if (fp){
			closeFile(fp);
			system("pause");

			return deleteFile(fname);
		}
		return 0;
	}
}
