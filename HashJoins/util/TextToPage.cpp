#include "TextToPage.h"
#include "../operators/TextFileScan.h"
#include "../io/FileManager.h"
#include "../io/Page.h"

#include <cstdio>
#include <cstring>

namespace db
{

	TextToPage::TextToPage(std::string inFileName, std::string outFileName, std::shared_ptr<Relation> arelation)
	:relation(arelation),
	inFileName(inFileName),
	outFileName(outFileName),
	outFile(nullptr)
	{
	}
	
	
	TextToPage::~TextToPage(void)
	{
	}

	int TextToPage::count()
	{
		if (!relation)
		{
			printf("ERROR: no relation provided to TextToPage.  Unable to convert");
			return false;
		}

		std::size_t tDataSize = relation->computeTupleSize() + 2*relation->getNumAttributes() + DB_TUPLE_FIRST_OFFSET;
		std::shared_ptr<Tuple> tuple(new Tuple(relation));

		tuple->setData(new char[tDataSize], true);

		std::shared_ptr<TextFileScan> tfs(new TextFileScan(inFileName, relation,"\n\r\t\f|"));

		if(!tfs->init())
		{
			printf("Unable to initialize text file scan operator");
			return false;
		}

		int count = 0;
		std::shared_ptr<Tuple> tmp(nullptr);
		while ((tmp = tfs->next(tuple)))
		{
			count++;
		}

		tfs->close();
		return count;
	}

	int TextToPage::convert()
	{
		if (!relation)
		{
			printf("ERROR: no relation provided to TextToPage.  Unable to convert");
			return 0;
		}

		outFile = openOutputFile(outFileName.c_str());

		if (!outFile)
			return false;

		std::size_t tDataSize = relation->computeTupleSize() + 2*relation->getNumAttributes() + DB_TUPLE_FIRST_OFFSET;
		std::shared_ptr<Tuple> tuple(new Tuple(relation));
		
		tuple->setData(new char[tDataSize], true);
		
		std::shared_ptr<Page> page(new Page(DB_PAGE_SIZE_DEFAULT, relation));

		std::shared_ptr<TextFileScan> tfs(new TextFileScan(inFileName, relation,"\n\r\t\f|"));
		
		if(!tfs->init())
		{
			printf("Unable to initialize text file scan operator");
			return 0;
		}

		int count = 0;
		std::shared_ptr<Tuple> tmp(nullptr);
		while ((tmp = tfs->next(tuple)))
		{
			count++;
			if (! page->hasSpace(tmp))
			{
				page->flush(outFile);
			}

			page->addTuple(tmp);
		}

		// write the last page
		if (page->getTupleCount() > 0)
		{
			page->flush(outFile);
		}

		tfs->close();
		return count;
	}

}
