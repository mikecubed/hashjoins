#define _CRTDBG_MAP_ALLOC

#include "convert.h"

#include <cstring>
#include <climits>
#include <cstdio>
#include <cassert>

namespace db
{
	using namespace std;

void shortToByte(short int sh, char *ba)						// Short
{
	shortToByteOffset(sh,ba,0);
}

void intToByte(int i, char *ba) 						// Integer
{
	intToByteOffset(i, ba, 0);
}

// this is potentially unsafe 
// You have to make sure ba is big enough to hold s
void stringToByte(char *s, char *ba)						// String
{
	// do not use strcpy because it will add the null to the byte array 
	// so do a bytewise copy
	size_t i;
	size_t strcnt = strlen(s);
	for (i = 0; i < strcnt; ++i) {
		ba[i] = s[i];
	}
}


/*
Methods to convert from byte arrays
*/

int byteToInt(char *ba)						// Integer
{
	return byteToIntOffset(ba, 0);
}

short int byteToShort(char *ba) 					// Short
{
	return byteToShortOffset(ba, 0);
}

// must pass in a char array at least len+1 bytes
void byteToString(char *ba, int len, char *str) 		// String
{
	// copy each character from ba and add a null at pos len (char len + 1)
	byteToStringOffset(ba, 0, len, str);
}

// use toString for a BigDecimal 
//BigDecimal toBigDecimal(byte ba[], int len) 		// String


int byteToIntOffset(char *ba, std::size_t offset)						// Integer
{
	if (ba != NULL) {
		int rval;
		memcpy(&rval, &ba[offset], sizeof(int));
		// for testing
		//assert(rval >= 0 && rval <= 600000);
		return rval;
	}
	return -1; // this should never happen ...
}

short int byteToShortOffset(char *ba, std::size_t offset) 					// Short
{
	if (ba != NULL) {
		short int rval;
		memcpy(&rval, &ba[offset], sizeof(short int));
		return rval;
	}
	return -1;
}

void byteToStringOffset(char *ba, std::size_t offset, std::size_t len, char *str) 		// String
{
	// copy each character from ba
	std::size_t i;
	for (i = 0; i < len; ++i) {
		str[i] = ba[i+offset];
	}
	str[len] = 0;
	
}

// use tostring
//BigDecimal toBigDecimal(byte ba[], int offset, int len) 		// String

void shortToByteOffset(short int sh, char *ba, std::size_t offset)						// Short
{	
	if (ba != NULL) {
		memcpy(&ba[offset], &sh, sizeof(short int));
	}
}

void intToByteOffset(int i, char *ba, std::size_t offset) 
{
	if (ba != NULL) {
		memcpy(&ba[offset], &i, sizeof(int));
	}

}

}
// Testing

using namespace db;

int testConvert()
{
	int i = INT_MIN, value, pass = 1;
	unsigned int j = 0, jvalue;
	char ba[4];
	short int si = SHRT_MIN, svalue;
	unsigned short int usi = 0, usvalue;

	// test some border cases
	intToByte(i, ba);
	value = byteToInt(ba);
	assert(i == value);

	i = INT_MAX;

	intToByte(i, ba);
	value = byteToInt(ba);
	assert(i == value);

	i = 0;

	intToByte(i, ba);
	value = byteToInt(ba);
	assert(i == value);

	i = -3333;

	intToByte(i, ba);
	value = byteToInt(ba);
	assert(i == value);

	i = 3333;

	intToByte(i, ba);
	value = byteToInt(ba);
	assert(i == value);

	// now the unsigned cases
	j = 0; // smallest unsigned value possible

	intToByte(j, ba);
	jvalue = byteToInt(ba);
	assert(j == jvalue);

	j = UINT_MAX; // smallest unsigned value possible

	intToByte(j, ba);
	jvalue = byteToInt(ba);
	assert(j == jvalue);

	si = SHRT_MIN;
	shortToByte(si, ba);
	svalue = byteToShort(ba);
	assert( svalue == si);

	si = 0;
	shortToByte(si, ba);
	svalue = byteToShort(ba);
	assert( svalue == si);

	si = SHRT_MAX;
	shortToByte(si, ba);
	svalue = byteToShort(ba);
	assert( svalue == si);

	usi = 0;
	shortToByte(usi, ba);
	usvalue = byteToShort(ba);
	assert( usvalue == usi);

	usi = USHRT_MAX;
	shortToByte(usi, ba);
	usvalue = byteToShort(ba);
	assert( usvalue == usi);

	return pass;
}