/*
FILE:			convert.h
PROVIDES: 		Contains functions for converting primative types to byte arrays 
C CODE BY:		Michael Henderson
CREATION DATE:	November 2007
MODIFIED:		February 2008
BASED ON:		unity.util.Convert.java
PROGRAM BY:     Ramon Lawrence (ramon.lawrence@ubc.ca)
*/
#ifndef convert_h__
#define convert_h__

#include <cstring>

namespace db
{

void shortToByte(short int sh, char *ba);							// Short
void intToByte(int i, char *ba); 									// Integer
// this is unneeded and potentially unsafe (s is already a byte array)
// You have to make sure ba is big enough to hold s
void stringToByte(char *s, char *ba);								// String

/*
Methods to convert from byte arrays
*/

int byteToInt(char *ba);												// Integer
short int byteToShort(char *ba); 									// Short

// must pass in a char array at least len+1 bytes
void byteToString(char *ba, std::size_t len, char *str); 					// String

// use toString for a BigDecimal 
//BigDecimal toBigDecimal(byte ba[], int len) 							// String


int byteToIntOffset(char *ba, std::size_t offset);							// Integer
short byteToShortOffset(char *ba, std::size_t offset); 						// Short

void byteToStringOffset(char *ba, std::size_t offset, std::size_t len, char *str); 	// String
// use tostring
//BigDecimal toBigDecimal(byte ba[], int offset, int len) 				// String

void shortToByteOffset(short int sh, char *ba, std::size_t offset);
void intToByteOffset(int i, char *ba, std::size_t offset);

}

#endif // convert_h__
