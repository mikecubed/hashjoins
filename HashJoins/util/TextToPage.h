#ifndef DB_TEXTTOPAGE_H
#define DB_TEXTTOPAGE_H

#include <string>
#include <memory>

#include "../relational/Relation.h"

namespace db
{

	class TextToPage
	{
	public:
		TextToPage(std::string inFileName, std::string outFileName, std::shared_ptr<Relation> relation);
		~TextToPage(void);
		
		int convert();
		int count();
	private:
		std::string inFileName;
		std::string outFileName;
		
		FILE *outFile;
		std::shared_ptr<Relation> relation;
	};
}

#endif //DB_TEXTTOPAGE_H
