#include "hashfunc.h"
#include "../relational/Relation.h"
#include "../relational/Attribute.h"

#include <cassert>

namespace db
{

	int hash(std::string key, int tableSize)
	{
		int hashVal = 0, i;
		int keylength = (int)(key.size()); 
	
		for( i = 0; i < keylength; i++ )
			hashVal = 37 * hashVal + key[i];

		hashVal %= tableSize;
		if( hashVal < 0 )
			hashVal += tableSize;

		return hashVal;
	}

	int hash(int key, int tableSize)
	{
		return key % tableSize;
	}

	int hash(std::shared_ptr<Tuple> key, int tableSize, const std::vector<int>& keyIdx)
	{
		assert(key);

		int hashValue = 0;
		std::shared_ptr<Relation> rel = key->getRelation();
		for (auto idx : keyIdx)
		{
			int value = 0;

			if (!key->isNull(idx))
			{
				switch(rel->getAttributeType(idx))
				{

				case DB_TYPE_INT:
					{
						int keyInt = key->getInt(idx);
						value = hash(keyInt, tableSize);
					}
					break;
				case DB_TYPE_STRING:
					{
						value = hash(key->getString(idx), tableSize);
					}
					break;
				default:
					{
						//TODO: all other possible data types...
					}
				} 
			}

			hashValue ^= value; 
		}

		return hashValue % tableSize;
	}

	int hash(std::shared_ptr<Tuple> key, int tableSize, const std::vector<int>& keyIdx, int &fullHashValue)
	{
		assert(key);
	
		fullHashValue = 0;
		int hashValue = 0;
		std::shared_ptr<Relation> rel = key->getRelation();
		for (auto idx : keyIdx)
		{
			int value = 0;

			if (!key->isNull(idx))
			{
				switch(rel->getAttributeType(idx))
				{

				case DB_TYPE_INT:
					{
						int keyInt = key->getInt(idx);
						fullHashValue ^= keyInt;
						value = hash(keyInt, tableSize);
					}
					break;
				case DB_TYPE_STRING:
					{
						value = hash(key->getString(idx), tableSize);
					}
					break;
				default:
					{
						//TODO: all other possible data types...
					}
				} 
			}

			hashValue ^= value; 
		}

		return hashValue % tableSize;
	}

	//
	int nextPrime(int n)
	{	
		if( n % 2 == 0 )
			n++;

		while(!isPrime( n ))
			n += 2;
		return n;
	}


	bool isPrime( int n )
	{
		int i;
		if( n == 2 || n == 3 )
			return true;

		if( n == 1 || n % 2 == 0 )
			return false;

		for( i = 3; (i * i) <= n; i += 2 )
			if( n % i == 0 )
				return false;
		return true;
	}

}
