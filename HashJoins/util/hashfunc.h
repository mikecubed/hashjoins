/*
FILE:			hashfunc.h
PROVIDES: 		Hashing functions for use with hashing operators
C CODE BY:		Michael Henderson
CREATION DATE:	November 2007
MODIFIED:		February 2008
BASED ON:		unity.util.HashFunc.java
PROGRAM BY:     Ramon Lawrence (ramon.lawrence@ubc.ca)
*/
#ifndef hashfunc_h__
#define hashfunc_h__

#include <string>
#include "../relational/Tuple.h"

namespace db
{
	int hash(int key, int tableSize);
	int hash(std::string key, int tableSize);
	int hash(std::shared_ptr<Tuple> key, int tableSize, const std::vector<int>& keyIdx);
	int hash(std::shared_ptr<Tuple> key, int tableSize, const std::vector<int>& keyIdx, int &hashValue);
	int nextPrime(int n); 
	bool isPrime(int n);
}
#endif // hashfunc_h__
