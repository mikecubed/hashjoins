for ($memSize = 7000; $memSize -ge 1000; $memSize = $memSize - 1000)
{
    for ($i=1; $i -le 5; $i++)
    {
        echo "Testing DHJ, $memSize, $i"
        .\HashJoins.exe 0 2 $memSize >> sharpOPL.csv
    }
}

for ($memSize = 7000; $memSize -ge 1000; $memSize = $memSize - 1000)
{
    for ($i=1; $i -le 5; $i++)
    {
        echo "Testing SHARP, $memSize, $i"
        .\HashJoins.exe 2 2 $memSize >> sharpOPL.csv
    }
}
