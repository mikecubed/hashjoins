#include "relational/Attribute.h"
#include "relational/Relation.h"
#include "relational/Tuple.h"
#include "operators/PageFileScan.h"
#include "operators/DynamicHashJoin.h"
#include "operators/HashTeamJoin.h"
#include "operators/GeneralizedHashTeamJoin.h"
#include "operators/SHARPJoin.h"
#include "predicates/EquijoinPredicate.h"
#include "io/FileManager.h"
#include "util/TextToPage.h"
#include "Joins.h"

#include <vector>
#include <memory>
#include <cassert>

using namespace std;
namespace db {

	int partByteSize(TPCHSize size)
	{
		switch (size)
		{
		case TPCHSize100M:
			return 2912256;
		case TPCHSize1G:
			return 29134848;
		case TPCHSize10G:
			return 291270656;
		default:
			return -1;
		}
	}

	int partSuppByteSize(TPCHSize size)
	{
		switch (size)
		{
		case TPCHSize100M:
			return 13119488;
		case TPCHSize1G:
			return 131170304;
		case TPCHSize10G:
			return 1311760384;
		default:
			return -1;
		}
	}

	int orderByteSize(TPCHSize size)
	{
		switch (size)
		{
		case TPCHSize100M:
			return 20545536;
		case TPCHSize1G:
			return 205414400;
		case TPCHSize10G:
			return 2054246400;
		default:
			return -1;
		}
	}

	long long lineitemByteSize(TPCHSize size)
	{
		switch (size)
		{
		case TPCHSize100M:
			return 97742848;
		case TPCHSize1G:
			return 977539072;
		case TPCHSize10G:
			return 9773707264;
		default:
			return -1;
		}
	}

	int partTupleSize(TPCHSize size)
	{
		switch (size)
		{
		case TPCHSize100M:
			return 20000;
		case TPCHSize1G:
			return 200000;
		case TPCHSize10G:
			return 2000000;
		default:
			return -1;
		}
	}

	int partSuppTupleSize(TPCHSize size)
	{
		switch (size)
		{
		case TPCHSize100M:
			return 80000;
		case TPCHSize1G:
			return 800000;
		case TPCHSize10G:
			return 8000000;
		default:
			return -1;
		}
	}

	int orderTupleSize(TPCHSize size)
	{
		switch (size)
		{
		case TPCHSize100M:
			return 150000;
		case TPCHSize1G:
			return 1500000;
		case TPCHSize10G:
			return 15000000;
		default:
			return -1;
		}
	}

	int lineitemTupleSize(TPCHSize size)
	{
		switch (size)
		{
		case TPCHSize100M:
			return 600000;
		case TPCHSize1G:
			return 6000002;
		case TPCHSize10G:
			return 59986052;
		default:
			return -1;
		}
	}

	int customerTupleSize(TPCHSize size)
	{
		switch (size)
		{
		case TPCHSize100M:
			return 15000;
		case TPCHSize1G:
			return 150000;
		case TPCHSize10G:
			return 1500000;
		default:
			return -1;
		}
	}

	void convertTextFile(const std::string& inFile, const std::string& outFile, const vector<Attribute>& attrs1)
	{
		shared_ptr<Relation> r1( new Relation(attrs1));

		TextToPage ttp(inFile, outFile, r1);

		printf("Converting %s to binary page file\n", inFile.c_str() );

		int count = ttp.convert();

		printf("Finished conversion to %s. %d rows returned.\n", outFile.c_str(), count);

	}

	std::vector<Attribute> getCustomerAttrs()
	{
		vector<Attribute> attrs1;
		attrs1.push_back( Attribute("c_custkey", DB_TYPE_INT, sizeof(int)) );
		attrs1.push_back( Attribute("c_name", DB_TYPE_STRING, 26) );
		attrs1.push_back( Attribute("c_address", DB_TYPE_STRING, 41) );
		attrs1.push_back( Attribute("c_nationkey", DB_TYPE_INT, sizeof(int)) );
		attrs1.push_back( Attribute("c_phone", DB_TYPE_STRING, 16) );
		attrs1.push_back( Attribute("c_acctbal", DB_TYPE_STRING, 18) );
		attrs1.push_back( Attribute("c_mktsegment", DB_TYPE_STRING, 11) );
		attrs1.push_back( Attribute("c_comment", DB_TYPE_STRING, 118) );
		return attrs1;
	}

	std::vector<Attribute> getPartAttrs()
	{
		vector<Attribute> attrs;
		// sizes come from tpc-h spec
		attrs.push_back( Attribute("p_partkey", DB_TYPE_INT, sizeof(int)));
		attrs.push_back( Attribute("p_name", DB_TYPE_STRING, 56));
		attrs.push_back( Attribute("p_mfgr", DB_TYPE_STRING, 26));
		attrs.push_back( Attribute("p_brand", DB_TYPE_STRING, 11));
		attrs.push_back( Attribute("p_type", DB_TYPE_STRING, 26));
		attrs.push_back( Attribute("p_size", DB_TYPE_INT, sizeof(int)));
		attrs.push_back( Attribute("p_container", DB_TYPE_STRING, 11));
		attrs.push_back( Attribute("p_retailprice", DB_TYPE_STRING, 18));
		attrs.push_back( Attribute("p_comment", DB_TYPE_STRING, 24));
		return attrs;
	}

	std::vector<Attribute> getPartSuppAttrs()
	{
		vector<Attribute> attrs;
		// sizes come from tpc-h spec
		attrs.push_back( Attribute("ps_partkey", DB_TYPE_INT, sizeof(int)));
		attrs.push_back( Attribute("ps_suppkey", DB_TYPE_INT, sizeof(int)));
		attrs.push_back( Attribute("ps_availqty", DB_TYPE_INT, sizeof(int)));
		attrs.push_back( Attribute("ps_supplycost", DB_TYPE_STRING, 18));
		attrs.push_back( Attribute("ps_comment", DB_TYPE_STRING, 200));
		return attrs;
	}


	std::vector<Attribute> getOrderAttrs()
	{
		vector<Attribute> attrs2;
		attrs2.push_back( Attribute("o_orderkey", DB_TYPE_INT, sizeof(int)) );
		attrs2.push_back( Attribute("o_custkey", DB_TYPE_INT, sizeof(int)) );
		attrs2.push_back( Attribute("o_orderstatus", DB_TYPE_STRING, 2) );
		attrs2.push_back( Attribute("o_totalprice", DB_TYPE_STRING, 18) );
		attrs2.push_back( Attribute("o_orderdate", DB_TYPE_STRING, 12) );
		attrs2.push_back( Attribute("o_orderpriority", DB_TYPE_STRING, 16) );
		attrs2.push_back( Attribute("o_clerk", DB_TYPE_STRING, 16) );
		attrs2.push_back( Attribute("o_shippriority", DB_TYPE_INT, sizeof(int)) );
		attrs2.push_back( Attribute("o_comment", DB_TYPE_STRING, 80) );

		return attrs2;
	}

	std::vector<Attribute> getLineitemAttrs()
	{
		vector<Attribute> attrs3;
		attrs3.push_back( Attribute("l_orderkey", DB_TYPE_INT, sizeof(int)) );
		attrs3.push_back( Attribute("l_partkey", DB_TYPE_INT, sizeof(int)) );
		attrs3.push_back( Attribute("l_suppkey", DB_TYPE_INT, sizeof(int)) );
		attrs3.push_back( Attribute("l_linenumber", DB_TYPE_INT, sizeof(int)) );
		attrs3.push_back( Attribute("l_quantity", DB_TYPE_STRING, 18) );
		attrs3.push_back( Attribute("l_extendedprice", DB_TYPE_STRING, 18) );
		attrs3.push_back( Attribute("l_discount", DB_TYPE_STRING, 18) );
		attrs3.push_back( Attribute("l_tax", DB_TYPE_STRING, 18) );
		attrs3.push_back( Attribute("l_returnflag", DB_TYPE_STRING, 2) );
		attrs3.push_back( Attribute("l_linestatus", DB_TYPE_STRING, 2) );
		attrs3.push_back( Attribute("l_shipdate", DB_TYPE_STRING, 12) );
		attrs3.push_back( Attribute("l_commitdate", DB_TYPE_STRING, 12) );
		attrs3.push_back( Attribute("l_receiptdate", DB_TYPE_STRING, 12) );
		attrs3.push_back( Attribute("l_shipinstruct", DB_TYPE_STRING, 26) );
		attrs3.push_back( Attribute("l_shipmode", DB_TYPE_STRING, 11) );
		attrs3.push_back( Attribute("l_comment", DB_TYPE_STRING, 45) );
		return attrs3;
	}

	int testCO()
	{
		vector<Attribute> attrs1 = getCustomerAttrs();
		shared_ptr<Relation> custRel(new Relation(attrs1));

		vector<Attribute> attrs2 = getOrderAttrs();
		shared_ptr<Relation> ordersRel(new Relation(attrs2));

		short pageSize = DB_PAGE_SIZE_DEFAULT;
		int BUFFER_SIZE = 250;
		int NUM_PARTITIONS = 60;

		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/customer.page", custRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);

		vector<int> idx1(1,0);
		vector<int> idx2(1,1);

		shared_ptr<EquijoinPredicate> ep(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeInt, JoinCardinalityUnknown));

		shared_ptr<DynamicHashJoin> dhj(new DynamicHashJoin(ops,ep, BUFFER_SIZE, NUM_PARTITIONS));


		FILE *out = openOutputFile("customer_order.txt");

		if (out == NULL) {
			fprintf(stderr, "ERROR: testJoins failed to open output file\n");
			return -1;
		}

		clock_t startTime = clock();

		if (!dhj->init())
		{
			printf("Unable to init dhj");
			return -1;
		}

		shared_ptr<Relation> outputRel = dhj->getOutputRelation();

		std::size_t outDataSize = outputRel->computeTupleSize() + 2*outputRel->getNumAttributes() + DB_TUPLE_FIRST_OFFSET;

		shared_ptr<Tuple> t(new Tuple(outputRel));

		t->setData(new char[outDataSize], true);

		std::shared_ptr<Tuple> tmp(nullptr);
		int count = 0;
		while ((tmp = dhj->next(t)) != NULL) {
			//		tmp->writeText(out);
			count++;

			if (count % 10000 == 0 ){//|| (count % 100 == 0 && count < 10000)) {
				printf("# of results: %d At time: %ld\n",dhj->getTuplesOutput(), clock() - startTime);
			}

		}

		closeFile(out);
		printf("# of results: %d At time: %ld\n",dhj->getTuplesOutput(), clock() - startTime);
		dhj->close();

		return 0;
	}

	int testCO_HTJ()
	{
		vector<Attribute> attrs1;
		attrs1.push_back( Attribute("c_custkey", DB_TYPE_INT, sizeof(int)) );
		attrs1.push_back( Attribute("c_name", DB_TYPE_STRING, 25) );
		attrs1.push_back( Attribute("c_address", DB_TYPE_STRING, 40) );
		attrs1.push_back( Attribute("c_nationkey", DB_TYPE_INT, sizeof(int)) );
		attrs1.push_back( Attribute("c_phone", DB_TYPE_STRING, 15) );
		attrs1.push_back( Attribute("c_acctbal", DB_TYPE_STRING, 18) );
		attrs1.push_back( Attribute("c_mktsegment", DB_TYPE_STRING, 10) );
		attrs1.push_back( Attribute("c_comment", DB_TYPE_STRING, 117) );

		shared_ptr<Relation> custRel(new Relation(attrs1));

		vector<Attribute> attrs2;
		attrs2.push_back( Attribute("o_orderkey", DB_TYPE_INT, sizeof(int)) );
		attrs2.push_back( Attribute("o_custkey", DB_TYPE_INT, sizeof(int)) );
		attrs2.push_back( Attribute("o_orderstatus", DB_TYPE_STRING, 1) );
		attrs2.push_back( Attribute("o_totalprice", DB_TYPE_STRING, 18) );
		attrs2.push_back( Attribute("o_orderdate", DB_TYPE_STRING, 10) );
		attrs2.push_back( Attribute("o_orderpriority", DB_TYPE_STRING, 15) );
		attrs2.push_back( Attribute("o_clerk", DB_TYPE_STRING, 15) );
		attrs2.push_back( Attribute("o_shippriority", DB_TYPE_INT, sizeof(int)) );
		attrs2.push_back( Attribute("o_comment", DB_TYPE_STRING, 79) );

		shared_ptr<Relation> ordersRel(new Relation(attrs2));

		short pageSize = DB_PAGE_SIZE_DEFAULT;
		int BUFFER_SIZE = 500;
		int NUM_PARTITIONS = 60;

		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/customer.page", custRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);

		vector<int> idx1(1,0);
		vector<int> idx2(1,1);

		shared_ptr<EquijoinPredicate> ep(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeInt, JoinCardinalityUnknown));
		std::vector<std::shared_ptr<EquijoinPredicate>> preds;
		preds.push_back(ep);

		shared_ptr<HashTeamJoin> htj(new HashTeamJoin(ops,preds, BUFFER_SIZE, NUM_PARTITIONS));


		FILE *out = openOutputFile("customer_order.txt");

		if (out == NULL) {
			fprintf(stderr, "ERROR: testJoins failed to open output file\n");
			return -1;
		}

		clock_t startTime = clock();

		if (!htj->init())
		{
			printf("Unable to init dhj");
			return -1;
		}

		shared_ptr<Relation> outputRel = htj->getOutputRelation();

		std::size_t outDataSize = outputRel->computeTupleSize() + 2*outputRel->getNumAttributes() + DB_TUPLE_FIRST_OFFSET;

		shared_ptr<Tuple> t(new Tuple(outputRel));

		t->setData(new char[outDataSize], true);

		std::shared_ptr<Tuple> tmp(nullptr);
		int count = 0;
		while ((tmp = htj->next(t)) != NULL) {
			//		tmp->writeText(out);
			count++;

			if (count % 10000 == 0 ){//|| (count % 100 == 0 && count < 10000)) {
				printf("# of results: %d At time: %ld\n",htj->getTuplesOutput(), clock() - startTime);
			}

		}

		closeFile(out);
		printf("# of results: %d At time: %ld\n",htj->getTuplesOutput(), clock() - startTime);
		htj->close();

		return 0;
	}

	int testOL()
	{
		vector<Attribute> attrs1 = getLineitemAttrs();
		shared_ptr<Relation> lineitemRel(new Relation(attrs1));

		vector<Attribute> attrs2 = getOrderAttrs();
		shared_ptr<Relation> ordersRel(new Relation(attrs2));

		short pageSize = DB_PAGE_SIZE_DEFAULT;
		int BUFFER_SIZE = 500;
		int NUM_PARTITIONS = 120;

		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/lineitem.page", lineitemRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);

		vector<int> idx1(1,0);
		vector<int> idx2(1,0);

		shared_ptr<EquijoinPredicate> ep(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeInt, JoinCardinalityUnknown));

		shared_ptr<DynamicHashJoin> dhj(new DynamicHashJoin(ops,ep, BUFFER_SIZE, NUM_PARTITIONS));


		FILE *out = openOutputFile("order_lineitem.txt");

		if (out == NULL) {
			fprintf(stderr, "ERROR: testJoins failed to open output file\n");
			return -1;
		}

		clock_t startTime = clock();

		if (!dhj->init())
		{
			printf("Unable to init dhj");
			return -1;
		}

		shared_ptr<Relation> outputRel = dhj->getOutputRelation();

		std::size_t outDataSize = outputRel->computeTupleSize() + 2*outputRel->getNumAttributes() + DB_TUPLE_FIRST_OFFSET;

		shared_ptr<Tuple> t(new Tuple(outputRel));

		t->setData(new char[outDataSize], true);

		std::shared_ptr<Tuple> tmp(nullptr);
		int count = 0;
		while ((tmp = dhj->next(t)) != NULL) {
			//	tmp->writeText(out);
			count++;
			if (count % 10000 == 0 ){//|| (count % 100 == 0 && count < 10000)) {
				printf("# of results: %d At time: %ld\n",dhj->getTuplesOutput(), clock() - startTime);
			}

		}

		closeFile(out);
		printf("# of results: %d At time: %ld\n",dhj->getTuplesOutput(), clock() - startTime);
		dhj->close();

		return 0;
	}

	/**
	Test dynamic hash join

	\param ops Three input operators
	\param preds Equijoin predicates for the relations.  Expects a predicate for the result of the first join to give to the second.
	\param BUFFER_SIZE Size in pages for each join
	\param NUM_PARTITIONS The number of partitions for each join
	\param leftDeep Defaults to true, set to false to have the results of the first join on the right side of the second join

	\returns 0 on success
	*/
	int testDHJ(const std::vector<std::shared_ptr<Operator>>& ops, const std::vector<std::shared_ptr<EquijoinPredicate>>& preds, int BUFFER_SIZE, int NUM_PARTITIONS, bool leftDeep = true)
	{

		// we assume 3 inputs
		assert(ops.size() == 3);
		assert(preds.size() == 2);

		std::vector<shared_ptr<Operator>> first;
		shared_ptr<EquijoinPredicate> ep = nullptr;

		if (leftDeep)
		{
			first.push_back(ops[0]);
			first.push_back(ops[1]);
			ep = preds[0];
		}
		else
		{
			first.push_back(ops[1]);
			first.push_back(ops[2]);
			ep = preds[1];
		}
		shared_ptr<DynamicHashJoin> dhj(new DynamicHashJoin(first,ep, BUFFER_SIZE, NUM_PARTITIONS));

		vector<shared_ptr<Operator>> ops2;
		shared_ptr<EquijoinPredicate> ep2 = nullptr;
		if (leftDeep)
		{
			ops2.push_back(dhj);
			ops2.push_back(ops[2]);
			ep2 = preds[1];
		}
		else
		{
			ops2.push_back(ops[0]);
			ops2.push_back(dhj);
			ep2 = preds[0];
		}

		shared_ptr<DynamicHashJoin> dhj2(new DynamicHashJoin(ops2,ep2, BUFFER_SIZE, NUM_PARTITIONS));

		//FILE *out = openOutputFile("customer_order_lineitem.txt");

		//if (out == NULL) {
		//	fprintf(stderr, "ERROR: testJoins failed to open output file\n");
		//	return -1;
		//}


		clock_t startTime = clock();

		if (!dhj2->init())
		{
			printf("Unable to init dhj\n");
			return -1;
		}

		int diff = clock() - startTime;

		shared_ptr<Relation> outputRel = dhj2->getOutputRelation();

		std::size_t outDataSize = outputRel->computeTupleSize() + 2*outputRel->getNumAttributes() + DB_TUPLE_FIRST_OFFSET;

		shared_ptr<Tuple> t(new Tuple(outputRel));

		t->setData(new char[outDataSize], true);

		std::shared_ptr<Tuple> tmp(nullptr);
		int count = 0;
		//printf("starting lineitem scan!\n");
		while ((tmp = dhj2->next(t)) != NULL) {
			//tmp->writeText(out);
			tmp->getSize();
		}

		printf("DHJ%d,%d,%d,%d,%d,(%d,%d),(%d,%d), %d\n",leftDeep,clock()-startTime,(dhj->getPageIOs() + dhj2->getPageIOs()), BUFFER_SIZE, dhj2->getTuplesOutput() ,dhj->getFrozen(), dhj->getNumBuckets(), dhj2->getFrozen(), dhj2->getNumBuckets(), diff);
		//closeFile(out);
		dhj2->close();

		return 0;
	}

	int testHTJ(const std::vector<std::shared_ptr<Operator>>& ops, const std::vector<std::shared_ptr<EquijoinPredicate>>& preds, int BUFFER_SIZE, int NUM_PARTITIONS)
	{
		shared_ptr<HashTeamJoin> htj(new HashTeamJoin(ops,preds, BUFFER_SIZE, NUM_PARTITIONS));

		clock_t startTime = clock();

		if (!htj->init())
		{
			printf("Unable to init hash teams");
			return -1;
		}

		shared_ptr<Relation> outputRel = htj->getOutputRelation();

		std::size_t outDataSize = outputRel->computeTupleSize() + 2*outputRel->getNumAttributes() + DB_TUPLE_FIRST_OFFSET;

		shared_ptr<Tuple> t(new Tuple(outputRel));

		t->setData(new char[outDataSize], true);

		std::shared_ptr<Tuple> tmp(nullptr);
		int count = 0;
		while ((tmp = htj->next(t)) != NULL) {
			tmp->getSize();
		}

		printf("Hash Teams,%d,%d,%d, %d,(%d,%d)\n",clock()-startTime,htj->getTuplesOutput(),htj->getPageIOs(), BUFFER_SIZE, htj->getFrozen(), htj->getNumBuckets());
		htj->close();

		return 0;
	}

	int testGHTJ(const std::vector<std::shared_ptr<Operator>>& ops, const std::vector<std::shared_ptr<EquijoinPredicate>>& preds, int BUFFER_SIZE, int NUM_PARTITIONS)
	{
		shared_ptr<GeneralizedHashTeamJoin> htj(new GeneralizedHashTeamJoin(ops,preds, BUFFER_SIZE, NUM_PARTITIONS));

		clock_t startTime = clock();

		if (!htj->init())
		{
			printf("Unable to init hash teams");
			return -1;
		}

		shared_ptr<Relation> outputRel = htj->getOutputRelation();

		std::size_t outDataSize = outputRel->computeTupleSize() + 2*outputRel->getNumAttributes() + DB_TUPLE_FIRST_OFFSET;

		shared_ptr<Tuple> t(new Tuple(outputRel));

		t->setData(new char[outDataSize], true);

		std::shared_ptr<Tuple> tmp(nullptr);
		int count = 0;
		while ((tmp = htj->next(t)) != NULL) {
			tmp->getSize();
		}

		printf("Generalized Hash Teams,%d,%d,%d,%d,(%d,%d),%d\n",clock()-startTime,htj->getTuplesOutput(),htj->getPageIOs(), BUFFER_SIZE, htj->getFrozen(), htj->getNumBuckets(), htj->getFalseDrops());
		htj->close();

		return 0;
	}

	int testSHARP(const std::vector<std::shared_ptr<Operator>>& ops, const std::vector<std::shared_ptr<EquijoinPredicate>>& preds, int BUFFER_SIZE, int NUM_PARTITIONS)
	{
		shared_ptr<SHARPJoin> htj(new SHARPJoin(ops,preds, BUFFER_SIZE, NUM_PARTITIONS));

		clock_t startTime = clock();

		if (!htj->init())
		{
			printf("Unable to init SHARP\n");
			return -1;
		}

		shared_ptr<Relation> outputRel = htj->getOutputRelation();

		std::size_t outDataSize = outputRel->computeTupleSize() + 2*outputRel->getNumAttributes() + DB_TUPLE_FIRST_OFFSET;

		shared_ptr<Tuple> t(new Tuple(outputRel));

		t->setData(new char[outDataSize], true);

		std::shared_ptr<Tuple> tmp(nullptr);
		int count = 0;
		while ((tmp = htj->next(t)) != NULL) {
			tmp->getSize();
		}
		std::vector<int> numP = htj->getNumPartitions();
		std::vector<int> numF = htj->getFrozenPartitions();
		printf("SHARP,%d,%d,%d,%d,(%d, %d),(%d,%d)\n",clock()-startTime,htj->getPageIOs(), BUFFER_SIZE, htj->getTuplesOutput(), numF[0], numP[0], numF[1], numP[1]);

		htj->close();

		return 0;
	}

	int testCOL_rightDeep(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		vector<Attribute> attrs1 = getCustomerAttrs();
		shared_ptr<Relation> custRel(new Relation(attrs1));

		vector<Attribute> attrs2 = getOrderAttrs();
		shared_ptr<Relation> ordersRel(new Relation(attrs2));

		vector<Attribute> attrs3 = getLineitemAttrs();
		shared_ptr<Relation> lineitemRel(new Relation(attrs3));

		short pageSize = DB_PAGE_SIZE_DEFAULT;
		//int BUFFER_SIZE = 2500;
		//int NUM_PARTITIONS = 2;

		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/customer.page", custRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));
		shared_ptr<PageFileScan> r3Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/lineitem.page", lineitemRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);
		ops.push_back(r3Scan);

		vector<int> idx1(1,0);
		vector<int> idx2(1,1);

		shared_ptr<EquijoinPredicate> ep(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeInt, JoinCardinalityUnknown));

		vector<int> idx3(1,0);
		vector<int> idx4(1,0);
		shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx3, idx4, PredicateKeyTypeInt, JoinCardinalityUnknown));

		vector<shared_ptr<EquijoinPredicate>> preds;
		preds.push_back(ep);
		preds.push_back(ep2);

		return testDHJ(ops, preds, BUFFER_SIZE, NUM_PARTITIONS, false);
	}

	int testCOL(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		vector<Attribute> attrs1 = getCustomerAttrs();
		shared_ptr<Relation> custRel(new Relation(attrs1));

		vector<Attribute> attrs2 = getOrderAttrs();
		shared_ptr<Relation> ordersRel(new Relation(attrs2));

		vector<Attribute> attrs3 = getLineitemAttrs();
		shared_ptr<Relation> lineitemRel(new Relation(attrs3));

		short pageSize = DB_PAGE_SIZE_DEFAULT;
		//int BUFFER_SIZE = 2500;
		//int NUM_PARTITIONS = 2;

		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/customer.page", custRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));
		shared_ptr<PageFileScan> r3Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/lineitem.page", lineitemRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);
		ops.push_back(r3Scan);

		vector<int> idx1(1,0);
		vector<int> idx2(1,1);

		shared_ptr<EquijoinPredicate> ep(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeInt, JoinCardinalityUnknown));

		vector<int> idx3(1,8);
		vector<int> idx4(1,0);
		shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx3, idx4, PredicateKeyTypeInt, JoinCardinalityUnknown));

		vector<shared_ptr<EquijoinPredicate>> preds;
		preds.push_back(ep);
		preds.push_back(ep2);

		return testDHJ(ops, preds, BUFFER_SIZE, NUM_PARTITIONS);
	}

	int testCOL_GHT(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		vector<Attribute> attrs1 = getCustomerAttrs();
		shared_ptr<Relation> custRel(new Relation(attrs1, customerTupleSize(tpchSize)));
		
		vector<Attribute> attrs2 = getOrderAttrs();
		shared_ptr<Relation> ordersRel(new Relation(attrs2, orderTupleSize(tpchSize)));
		
		vector<Attribute> attrs3 = getLineitemAttrs();
		shared_ptr<Relation> lineitemRel(new Relation(attrs3, lineitemTupleSize(tpchSize)));

		short pageSize = DB_PAGE_SIZE_DEFAULT;

		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/customer.page", custRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));
		shared_ptr<PageFileScan> r3Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/lineitem.page", lineitemRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);
		ops.push_back(r3Scan);

		vector<int> idx1(1,0);
		vector<int> idx2(1,1);
		vector<int> idx3(1,0);
		vector<int> idx4(1,0);

		shared_ptr<EquijoinPredicate> ep(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeInt, JoinCardinalityUnknown));
		shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx3, idx4, PredicateKeyTypeInt, JoinCardinalityUnknown));
		vector<shared_ptr<EquijoinPredicate>> preds;
		preds.push_back(ep);
		preds.push_back(ep2);

		return testGHTJ(ops,preds,BUFFER_SIZE,NUM_PARTITIONS);
	}

	int testCCC_HTJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		short pageSize = DB_PAGE_SIZE_DEFAULT;

		vector<Attribute> attrs1 = getCustomerAttrs();
		shared_ptr<Relation> custRel(new Relation(attrs1));

		shared_ptr<PageFileScan> r0Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/customer.page", custRel, pageSize));
		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/customer.page", custRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/customer.page", custRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r0Scan);
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);

		vector<int> idx0(1,0);
		vector<int> idx1(1,0);
		vector<int> idx2(1,0);

		shared_ptr<EquijoinPredicate> ep1(new EquijoinPredicate(idx0,idx1,PredicateKeyTypeInt, JoinCardinalityUnknown));
		shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeInt, JoinCardinalityUnknown));
		std::vector<std::shared_ptr<EquijoinPredicate>> preds;
		preds.push_back(ep1);
		preds.push_back(ep2);

		return testHTJ(ops, preds, BUFFER_SIZE, NUM_PARTITIONS);
	}

	int testCCC_DHJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		short pageSize = DB_PAGE_SIZE_DEFAULT;

		vector<Attribute> attrs = getCustomerAttrs();
		shared_ptr<Relation> custRel(new Relation(attrs));

		shared_ptr<PageFileScan> r0Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/customer.page", custRel, pageSize));
		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/customer.page", custRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/customer.page", custRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r0Scan);
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);

		vector<int> idx1(1,0);
		vector<int> idx2(1,0);

		shared_ptr<EquijoinPredicate> ep(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeInt, JoinCardinalityUnknown));

		vector<int> idx3(1,0);
		vector<int> idx4(1,0);
		shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx3, idx4, PredicateKeyTypeInt, JoinCardinalityUnknown));

		std::vector<shared_ptr<EquijoinPredicate>> preds;
		preds.push_back(ep);
		preds.push_back(ep2);

		return testDHJ(ops,preds, BUFFER_SIZE, NUM_PARTITIONS);
	}

	int testOOO_HTJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		vector<Attribute> attrs2 = getOrderAttrs();

		shared_ptr<Relation> ordersRel(new Relation(attrs2, orderTupleSize(tpchSize), orderByteSize(tpchSize)));

		short pageSize = DB_PAGE_SIZE_DEFAULT;

		shared_ptr<PageFileScan> r0Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));
		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r0Scan);
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);

		vector<int> idx0(1,0);
		vector<int> idx1(1,0);
		vector<int> idx2(1,0);

		shared_ptr<EquijoinPredicate> ep1(new EquijoinPredicate(idx0,idx1,PredicateKeyTypeInt, JoinCardinalityUnknown));
		shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeInt, JoinCardinalityUnknown));
		std::vector<std::shared_ptr<EquijoinPredicate>> preds;
		preds.push_back(ep1);
		preds.push_back(ep2);

		return testHTJ(ops, preds, BUFFER_SIZE, NUM_PARTITIONS);
	}

	int testOOO(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		vector<Attribute> attrs2 = getOrderAttrs();

		shared_ptr<Relation> ordersRel(new Relation(attrs2, orderTupleSize(tpchSize), orderByteSize(tpchSize)));

		short pageSize = DB_PAGE_SIZE_DEFAULT;

		shared_ptr<PageFileScan> r0Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));
		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order1.page", ordersRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order2.page", ordersRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r0Scan);
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);

		vector<int> idx1(1,0);
		vector<int> idx2(1,0);

		shared_ptr<EquijoinPredicate> ep(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeInt, JoinCardinalityUnknown));

		vector<int> idx3(1,0);
		vector<int> idx4(1,0);

		shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx3, idx4, PredicateKeyTypeInt, JoinCardinalityUnknown));

		vector<shared_ptr<EquijoinPredicate>> preds;
		preds.push_back(ep);
		preds.push_back(ep2);
		return testDHJ(ops, preds, BUFFER_SIZE, NUM_PARTITIONS);
	}

	int testOOO_rightDeep(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		vector<Attribute> attrs2 = getOrderAttrs();

		shared_ptr<Relation> ordersRel(new Relation(attrs2, orderTupleSize(tpchSize), orderByteSize(tpchSize)));

		short pageSize = DB_PAGE_SIZE_DEFAULT;

		shared_ptr<PageFileScan> r0Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));
		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r0Scan);
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);

		vector<int> idx1(1,0);
		vector<int> idx2(1,0);

		shared_ptr<EquijoinPredicate> ep(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeInt, JoinCardinalityUnknown));

		vector<int> idx3(1,0);
		vector<int> idx4(1,0);

		shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx3, idx4, PredicateKeyTypeInt, JoinCardinalityUnknown));

		vector<shared_ptr<EquijoinPredicate>> preds;
		preds.push_back(ep);
		preds.push_back(ep2);
		return testDHJ(ops, preds, BUFFER_SIZE, NUM_PARTITIONS, false);
	}

	//int testOPL_DHJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	//{
	//	vector<Attribute> attrs = getPartAttrs();
	//	shared_ptr<Relation> partRel(new Relation(attrs, partTupleSize(tpchSize), partByteSize(tpchSize)));

	//	vector<Attribute> attrs2 = getOrderAttrs();
	//	shared_ptr<Relation> ordersRel(new Relation(attrs2, orderTupleSize(tpchSize), orderByteSize(tpchSize)));

	//	vector<Attribute> attrs3 = getLineitemAttrs();
	//	shared_ptr<Relation> lineitemRel(new Relation(attrs3, lineitemTupleSize(tpchSize), lineitemByteSize(tpchSize)));


	//	short pageSize = DB_PAGE_SIZE_DEFAULT;

	//	shared_ptr<PageFileScan> r0Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));
	//	shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/part.page", partRel, pageSize));
	//	shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/lineitem.page", lineitemRel, pageSize));

	//	vector<shared_ptr<Operator>> ops;
	//	ops.push_back(r1Scan);
	//	ops.push_back(r2Scan);
	//	ops.push_back(r0Scan);

	//	vector<int> idx0(1,0);
	//	vector<int> idx01(1,1);
	//	vector<int> idx1(1,10);
	//	vector<int> idx2(1,0);

	//	shared_ptr<EquijoinPredicate> ep1(new EquijoinPredicate(idx0,idx01,PredicateKeyTypeInt, JoinCardinalityUnknown));
	//	shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeInt, JoinCardinalityUnknown));

	//	vector<shared_ptr<EquijoinPredicate>> preds;
	//	preds.push_back(ep1);
	//	preds.push_back(ep2);

	//	return testDHJ(ops,preds,BUFFER_SIZE,NUM_PARTITIONS);
	//}

	int testOPL_DHJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		vector<Attribute> attrs = getPartAttrs();
		shared_ptr<Relation> partRel(new Relation(attrs, partTupleSize(tpchSize), partByteSize(tpchSize)));

		vector<Attribute> attrs2 = getOrderAttrs();
		shared_ptr<Relation> ordersRel(new Relation(attrs2, orderTupleSize(tpchSize), orderByteSize(tpchSize)));

		vector<Attribute> attrs3 = getLineitemAttrs();
		shared_ptr<Relation> lineitemRel(new Relation(attrs3, lineitemTupleSize(tpchSize), lineitemByteSize(tpchSize)));


		short pageSize = DB_PAGE_SIZE_DEFAULT;

		shared_ptr<PageFileScan> r0Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));
		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/part.page", partRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/lineitem.page", lineitemRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r0Scan);
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);
		
		vector<int> idx0(1,0);
		vector<int> idx01(1,9);
		vector<int> idx1(1,0);
		vector<int> idx2(1,1);

		shared_ptr<EquijoinPredicate> ep1(new EquijoinPredicate(idx0,idx01,PredicateKeyTypeInt, JoinCardinalityUnknown));
		shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeInt, JoinCardinalityUnknown));

		vector<shared_ptr<EquijoinPredicate>> preds;
		preds.push_back(ep1);
		preds.push_back(ep2);

		return testDHJ(ops,preds,BUFFER_SIZE,NUM_PARTITIONS, false);
	}

	int testOPL_SHARP(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		vector<Attribute> attrs = getPartAttrs();
		shared_ptr<Relation> partRel(new Relation(attrs, partTupleSize(tpchSize), partByteSize(tpchSize)));

		vector<Attribute> attrs2 = getOrderAttrs();
		shared_ptr<Relation> ordersRel(new Relation(attrs2, orderTupleSize(tpchSize), orderByteSize(tpchSize)));

		vector<Attribute> attrs3 = getLineitemAttrs();
		shared_ptr<Relation> lineitemRel(new Relation(attrs3, lineitemTupleSize(tpchSize), lineitemByteSize(tpchSize)));

		short pageSize = DB_PAGE_SIZE_DEFAULT;

		shared_ptr<PageFileScan> r0Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));
		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/part.page", partRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/lineitem.page", lineitemRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r0Scan);
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);

		vector<int> idx0(1,0);
		vector<int> idx1(1,0);
		vector<int> idx2(1,1);

		shared_ptr<EquijoinPredicate> ep1(new EquijoinPredicate(idx0,idx1,PredicateKeyTypeInt, JoinCardinalityUnknown));
		shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeInt, JoinCardinalityUnknown));
		std::vector<std::shared_ptr<EquijoinPredicate>> preds;
		preds.push_back(ep1);
		preds.push_back(ep2);

		return testSHARP(ops, preds, BUFFER_SIZE, NUM_PARTITIONS);
	}

	int testPSL_DHJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		vector<Attribute> attrs = getPartSuppAttrs();
		shared_ptr<Relation> partSuppRel(new Relation(attrs, partSuppTupleSize(tpchSize), partSuppByteSize(tpchSize)));

		vector<Attribute> attrs3 = getLineitemAttrs();
		shared_ptr<Relation> lineitemRel(new Relation(attrs3, lineitemTupleSize(tpchSize), lineitemByteSize(tpchSize)));

		short pageSize = DB_PAGE_SIZE_DEFAULT;

		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/partsupp.page", partSuppRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/lineitem.page", lineitemRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);

		// partsupp to lineitem
		vector<int> idx1;
		idx1.push_back(0);
		idx1.push_back(1);

		vector<int> idx2;//(1,1);
		idx2.push_back(1);
		idx2.push_back(2);

		shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeObject, JoinCardinalityUnknown));

		shared_ptr<DynamicHashJoin> dhj(new DynamicHashJoin(ops,ep2, BUFFER_SIZE, NUM_PARTITIONS));

		clock_t startTime = clock();

		if (!dhj->init())
		{
			printf("Unable to init dhj\n");
			return -1;
		}

		int diff = clock() - startTime;

		shared_ptr<Relation> outputRel = dhj->getOutputRelation();

		std::size_t outDataSize = outputRel->computeTupleSize() + 2*outputRel->getNumAttributes() + DB_TUPLE_FIRST_OFFSET;

		shared_ptr<Tuple> t(new Tuple(outputRel));

		t->setData(new char[outDataSize], true);

		std::shared_ptr<Tuple> tmp(nullptr);
		int count = 0;
		//printf("starting lineitem scan!\n");
		long long sum = 0;
		while ((tmp = dhj->next(t)) != NULL) {
			//tmp->writeText(out);
			sum += tmp->getSize();
		}

		printf("DHJ%d,%d,%d,%d,%d,(%d,%d), %d, %d\n",1,clock()-startTime,(dhj->getPageIOs()), BUFFER_SIZE, dhj->getTuplesOutput() ,dhj->getFrozen(), dhj->getNumBuckets(), diff, dhj->searches);
		//closeFile(out);
		dhj->close();
		//printf("DHJ%d,%d,%d,%d,%d,(%d,%d), %d\n",1,clock()-startTime,(dhj->getPageIOs()), BUFFER_SIZE, dhj->getTuplesOutput() ,dhj->getFrozen(), dhj->getNumBuckets(), diff);
		return 0;
	}

	int testOL_DHJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		vector<Attribute> attrs2 = getOrderAttrs();
		shared_ptr<Relation> ordersRel(new Relation(attrs2, orderTupleSize(tpchSize), orderByteSize(tpchSize)));

		vector<Attribute> attrs3 = getLineitemAttrs();
		shared_ptr<Relation> lineitemRel(new Relation(attrs3, lineitemTupleSize(tpchSize), lineitemByteSize(tpchSize)));


		short pageSize = DB_PAGE_SIZE_DEFAULT;

		shared_ptr<PageFileScan> r0Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/lineitem.page", lineitemRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r0Scan);
		ops.push_back(r2Scan);

		vector<int> idx0(1,0);
		vector<int> idx01(1,0);

		shared_ptr<EquijoinPredicate> ep1(new EquijoinPredicate(idx0,idx01,PredicateKeyTypeInt, JoinCardinalityUnknown));
		shared_ptr<DynamicHashJoin> dhj(new DynamicHashJoin(ops,ep1, BUFFER_SIZE, NUM_PARTITIONS));

		clock_t startTime = clock();

		if (!dhj->init())
		{
			printf("Unable to init dhj\n");
			return -1;
		}

		int diff = clock() - startTime;

		shared_ptr<Relation> outputRel = dhj->getOutputRelation();

		std::size_t outDataSize = outputRel->computeTupleSize() + 2*outputRel->getNumAttributes() + DB_TUPLE_FIRST_OFFSET;

		shared_ptr<Tuple> t(new Tuple(outputRel));

		t->setData(new char[outDataSize], true);

		std::shared_ptr<Tuple> tmp(nullptr);
		int count = 0;
		//printf("starting lineitem scan!\n");
		while ((tmp = dhj->next(t)) != NULL) {
			//tmp->writeText(out);
			tmp->getSize();
		}

		printf("DHJ%d,%d,%d,%d,%d,(%d,%d), %d\n",1,clock()-startTime,(dhj->getPageIOs()), BUFFER_SIZE, dhj->getTuplesOutput() ,dhj->getFrozen(), dhj->getNumBuckets(), diff);
		//closeFile(out);
		dhj->close();

		return 0;
	}

	int testPPSL_DHJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		vector<Attribute> attrs = getPartSuppAttrs();
		shared_ptr<Relation> partSuppRel(new Relation(attrs, partSuppTupleSize(tpchSize), partSuppByteSize(tpchSize)));

		vector<Attribute> attrs2 = getPartAttrs();
		shared_ptr<Relation> partRel(new Relation(attrs2, partTupleSize(tpchSize), partByteSize(tpchSize)));

		vector<Attribute> attrs3 = getLineitemAttrs();
		shared_ptr<Relation> lineitemRel(new Relation(attrs3, lineitemTupleSize(tpchSize), lineitemByteSize(tpchSize)));


		short pageSize = DB_PAGE_SIZE_DEFAULT;

		shared_ptr<PageFileScan> r0Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/part.page", partRel, pageSize));
		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/partsupp.page", partSuppRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/lineitem.page", lineitemRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r0Scan);
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);

		vector<int> idx0(1,0);
		vector<int> idx01(1,0);

		// partsupp to lineitem
		vector<int> idx1;
		idx1.push_back(0);
		idx1.push_back(1);

		vector<int> idx2;//(1,1);
		idx2.push_back(1);
		idx2.push_back(2);

		shared_ptr<EquijoinPredicate> ep1(new EquijoinPredicate(idx0,idx01,PredicateKeyTypeInt, JoinCardinalityUnknown));
		shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeObject, JoinCardinalityUnknown));

		vector<shared_ptr<EquijoinPredicate>> preds;
		preds.push_back(ep1);
		preds.push_back(ep2);

		return testDHJ(ops,preds,BUFFER_SIZE,NUM_PARTITIONS, false);
	}

	int testOPSL_DHJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		vector<Attribute> attrs = getPartSuppAttrs();
		shared_ptr<Relation> partRel(new Relation(attrs, partSuppTupleSize(tpchSize), partSuppByteSize(tpchSize)));

		vector<Attribute> attrs2 = getOrderAttrs();
		shared_ptr<Relation> ordersRel(new Relation(attrs2, orderTupleSize(tpchSize), orderByteSize(tpchSize)));

		vector<Attribute> attrs3 = getLineitemAttrs();
		shared_ptr<Relation> lineitemRel(new Relation(attrs3, lineitemTupleSize(tpchSize), lineitemByteSize(tpchSize)));


		short pageSize = DB_PAGE_SIZE_DEFAULT;

		shared_ptr<PageFileScan> r0Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));
		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/partsupp.page", partRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/lineitem.page", lineitemRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r0Scan);
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);

		vector<int> idx0(1,0);
		vector<int> idx01(1,5);
		
		// partsupp to lineitem
		vector<int> idx1;
		idx1.push_back(0);
		idx1.push_back(1);

		vector<int> idx2;//(1,1);
		idx2.push_back(1);
		idx2.push_back(2);

		shared_ptr<EquijoinPredicate> ep1(new EquijoinPredicate(idx0,idx01,PredicateKeyTypeInt, JoinCardinalityUnknown));
		shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeObject, JoinCardinalityUnknown));

		vector<shared_ptr<EquijoinPredicate>> preds;
		preds.push_back(ep1);
		preds.push_back(ep2);

		return testDHJ(ops,preds,BUFFER_SIZE,NUM_PARTITIONS, false);
	}

	int testOPSL_SHARP(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		vector<Attribute> attrs = getPartSuppAttrs();
		shared_ptr<Relation> partRel(new Relation(attrs, partSuppTupleSize(tpchSize), partSuppByteSize(tpchSize)));

		vector<Attribute> attrs2 = getOrderAttrs();
		shared_ptr<Relation> ordersRel(new Relation(attrs2, orderTupleSize(tpchSize), orderByteSize(tpchSize)));

		vector<Attribute> attrs3 = getLineitemAttrs();
		shared_ptr<Relation> lineitemRel(new Relation(attrs3, lineitemTupleSize(tpchSize), lineitemByteSize(tpchSize)));

		short pageSize = DB_PAGE_SIZE_DEFAULT;

		shared_ptr<PageFileScan> r0Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));
		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/partsupp.page", partRel, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/lineitem.page", lineitemRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r0Scan);
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);

		// order-lineitem
		vector<int> idx0(1,0);
		vector<int> idx1(1,0);

		// ps-li
		vector<int> idx2;
		idx2.push_back(0);
		idx2.push_back(1);

		vector<int> idx3;
		idx3.push_back(1);
		idx3.push_back(2);
		
		shared_ptr<EquijoinPredicate> ep1(new EquijoinPredicate(idx0,idx1,PredicateKeyTypeInt, JoinCardinalityUnknown));
		shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx2,idx3,PredicateKeyTypeObject, JoinCardinalityUnknown));
		std::vector<std::shared_ptr<EquijoinPredicate>> preds;
		preds.push_back(ep1);
		preds.push_back(ep2);

		return testSHARP(ops, preds, BUFFER_SIZE, NUM_PARTITIONS);
	}

	int testOOL_DHJ(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		vector<Attribute> attrs = getOrderAttrs();
		shared_ptr<Relation> ordersRel2(new Relation(attrs, orderTupleSize(tpchSize), orderByteSize(tpchSize)));

		vector<Attribute> attrs2 = getOrderAttrs();
		shared_ptr<Relation> ordersRel(new Relation(attrs2, orderTupleSize(tpchSize), orderByteSize(tpchSize)));

		vector<Attribute> attrs3 = getLineitemAttrs();
		shared_ptr<Relation> lineitemRel(new Relation(attrs3, lineitemTupleSize(tpchSize), lineitemByteSize(tpchSize)));


		short pageSize = DB_PAGE_SIZE_DEFAULT;

		shared_ptr<PageFileScan> r0Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));
		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order1.page", ordersRel2, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/lineitem.page", lineitemRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r0Scan);
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);

		vector<int> idx0(1,0);
		vector<int> idx01(1,0);

		// partsupp to lineitem
		vector<int> idx1(1,0);
		vector<int> idx2(1,0);//(1,1);

		shared_ptr<EquijoinPredicate> ep1(new EquijoinPredicate(idx0,idx01,PredicateKeyTypeInt, JoinCardinalityUnknown));
		shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx1,idx2,PredicateKeyTypeObject, JoinCardinalityUnknown));

		vector<shared_ptr<EquijoinPredicate>> preds;
		preds.push_back(ep1);
		preds.push_back(ep2);

		return testDHJ(ops,preds,BUFFER_SIZE,NUM_PARTITIONS, false);
	}

	int testOOL_SHARP(int BUFFER_SIZE, int NUM_PARTITIONS, TPCHSize tpchSize)
	{
		vector<Attribute> attrs = getPartSuppAttrs();
		shared_ptr<Relation> ordersRel2(new Relation(attrs, orderTupleSize(tpchSize), orderByteSize(tpchSize)));

		vector<Attribute> attrs2 = getOrderAttrs();
		shared_ptr<Relation> ordersRel(new Relation(attrs2, orderTupleSize(tpchSize), orderByteSize(tpchSize)));

		vector<Attribute> attrs3 = getLineitemAttrs();
		shared_ptr<Relation> lineitemRel(new Relation(attrs3, lineitemTupleSize(tpchSize), lineitemByteSize(tpchSize)));

		short pageSize = DB_PAGE_SIZE_DEFAULT;

		shared_ptr<PageFileScan> r0Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order.page", ordersRel, pageSize));
		shared_ptr<PageFileScan> r1Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/order1.page", ordersRel2, pageSize));
		shared_ptr<PageFileScan> r2Scan = shared_ptr<PageFileScan>(new PageFileScan("data/input/lineitem.page", lineitemRel, pageSize));

		vector<shared_ptr<Operator>> ops;
		ops.push_back(r0Scan);
		ops.push_back(r1Scan);
		ops.push_back(r2Scan);

		// order-lineitem
		vector<int> idx0(1,0);
		vector<int> idx1(1,0);

		// ps-li
		//vector<int> idx2;
		//idx2.push_back(0);
		//idx2.push_back(1);

		//vector<int> idx3;
		//idx3.push_back(1);
		//idx3.push_back(2);

		shared_ptr<EquijoinPredicate> ep1(new EquijoinPredicate(idx0,idx1,PredicateKeyTypeInt, JoinCardinalityUnknown));
		shared_ptr<EquijoinPredicate> ep2(new EquijoinPredicate(idx0,idx0,PredicateKeyTypeObject, JoinCardinalityUnknown));
		std::vector<std::shared_ptr<EquijoinPredicate>> preds;
		preds.push_back(ep1);
		preds.push_back(ep2);

		return testSHARP(ops, preds, BUFFER_SIZE, NUM_PARTITIONS);
	}
}
