for ($memSize = 400; $memSize -ge 100; $memSize = $memSize - 100)
{
    for ($i=1; $i -le 5; $i++)
    {
        echo "Testing $algo, $memSize, $i"
        .\HashJoins.exe 0 2 $memSize >> genhashteams-smaller.csv
    }
}

for ($memSize = 900; $memSize -ge 100; $memSize = $memSize - 100)
{
    for ($i=1; $i -le 5; $i++)
    {
        echo "Testing $algo, $memSize, $i"
        .\HashJoins.exe 1 2 $memSize >> genhashteams-smaller.csv
    }
}

for ($memSize = 900; $memSize -ge 100; $memSize = $memSize - 100)
{
    for ($i=1; $i -le 5; $i++)
    {
        echo "Testing $algo, $memSize, $i"
        .\HashJoins.exe 2 2 $memSize >> genhashteams-smaller.csv
    }
}