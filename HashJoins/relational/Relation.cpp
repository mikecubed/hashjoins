#include "Relation.h"

namespace db
{
	Relation::Relation()
		:attributes(std::vector<Attribute>())
	{

	}

	Relation::Relation(const std::vector<Attribute> &attribs, unsigned int aTupleEstimate, unsigned long long theRelationByteSize)
		:attributes(attribs),
		tupleEstimate(aTupleEstimate),
		relationByteSize(theRelationByteSize)
	{

	}
	
	Relation::Relation(const Relation &rel)
		:attributes(rel.attributes),
		tupleEstimate(rel.tupleEstimate),
		relationByteSize(rel.relationByteSize)
	{

	}

	Relation::Relation(const Relation &l, const Relation &r)
		:attributes(l.attributes),
		tupleEstimate(0),
		relationByteSize(0)
	{
		attributes.insert(attributes.end(), r.attributes.begin(), r.attributes.end());
	}

	Relation::Relation(const std::vector<std::shared_ptr<Relation>>& rels)
		:tupleEstimate(0),
		relationByteSize(0)
	{
		for (auto &rel : rels)
		{
			attributes.insert(attributes.end(), rel->attributes.begin(), rel->attributes.end());
		}
	}

	Relation::~Relation(void)
	{
	}

	std::size_t Relation::computeTupleSize() const
	{
		std::size_t size = 0;
		for (Attribute a : attributes)
			size += a.getLength();
		return size;
	}

	DB_TYPE Relation::getAttributeType(std::size_t index) const
	{
		if (index < attributes.size())
			return attributes[index].getType();
		
		return DB_TYPE_INVALID;
	}

	void Relation::setAttributeType(std::size_t index, DB_TYPE type)
	{
		if (index < attributes.size())
			attributes[index].setType(type);
	}

	std::size_t Relation::getNumAttributes() const
	{
		return attributes.size();
	}

	const Attribute * const Relation::getAttribute(std::size_t index) const
	{
		if (index < attributes.size())
			return &attributes[index];
		return nullptr;
	}

	void Relation::setAttribute(std::size_t index, const Attribute &a)
	{
		if (index < attributes.size())
			attributes[index] = a;
	}

	unsigned int Relation::getTupleEstimate()
	{
		return tupleEstimate;
	}

	unsigned long long Relation::getRelationByteSize()
	{
		return relationByteSize;
	}
}