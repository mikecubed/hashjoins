#include "Attribute.h"
#include "../util/convert.h"

#include <cstdio>
#include <cstring>

namespace db
{

Attribute::Attribute(std::string name, DB_TYPE type, int length)
	:name(name), type(type), length(length)
{
}


Attribute::~Attribute(void)
{
}

std::string Attribute::getName() const
{
	return name;
}

int Attribute::getLength() const
{
	return length;
}

DB_TYPE Attribute::getType() const
{
	return type;
}

void Attribute::setType(DB_TYPE t)
{
	this->type = t;
}

// file reads (return 0 on failure)
int attribute::readFileInt(FILE *fp, int *out)
{
	size_t intsRead = 0;
	if (!fp) return -1;
	intsRead = fread(out,4,1,fp);
	if (intsRead != 1) 
		return 0;
	else
		return 1;
}


int attribute::readFileSmallInt(FILE *fp, short int *out)
{
	size_t shortsRead;
	if (!fp) return -1;
	shortsRead = fread(out,sizeof(short),1,fp);
	if (shortsRead !=1) 
		return 0;
	return 1;
}

// expects outBuffer to be 257 bytes (256 bytes + null)
int attribute::readFileString(FILE *fp, char *outBuffer)
{
	size_t charsRead;
	unsigned char numChars;
	if (!fp) return 0;
	// get the number of characters in the string
	fread(&numChars, sizeof(unsigned char), 1,fp);

	//printf("Number of chars in the string: %d\n",numChars);
	charsRead = fread(outBuffer,sizeof(char), numChars, fp);
	// add the null terminator
	outBuffer[numChars] = 0;
	if (numChars != charsRead) {
		fprintf(stderr, "Couldn't read in the correct number of characters in the string\n");
		return 0;
	}
	return 1;

}

// file writes (return 0 on failure)
// TODO: Add support for more types
int attribute::writeFile(FILE *fp, int attrType, void *data)
{
	unsigned char size;
	unsigned char length = 1;
	if (!fp) {
		return 0;
	}

	switch(attrType) 
	{
	case DB_TYPE_INT:
		size = sizeof(int);	
		break;
	case DB_TYPE_SMALLINT:
		size = sizeof(short int);
		break;
	case DB_TYPE_STRING:
	case DB_TYPE_CHAR:
		size = sizeof(char);
		length = (unsigned char)strlen((char *)data);
		// output the length of the string
		fwrite(&length,1,1,fp);
		break;
	default:
		return 0;
	}

	size = (unsigned char)fwrite(data,size,length,fp);
	if (size != length) {
		// yikes, something bad happened
		return 0;
	}
	return 1;

}

// buffer reads (return -1 on failure)
int attribute::readByteInt(char *inBuffer, int offset)
{
	if (inBuffer != NULL) {
		return byteToIntOffset(inBuffer, offset);
	}
	return -1; // fails should never get here
}

short int attribute::readByteSmallInt(char *inBuffer, int offset)
{
	if (inBuffer != NULL) {
		return byteToShortOffset(inBuffer, offset);
	}
	return (short)-1;
}

// expects outBuffer to be 256 bytes
int attribute::readByteString(char *inBuffer, int offset, char *outBuffer)
{
	if (inBuffer != NULL && outBuffer != NULL) {
		int length = inBuffer[offset];
		if (length < 0)
		{
			printf("ERROR: invalid offset in buffer\n");
			outBuffer[0] = '\0';
			return 0;
		}
		byteToStringOffset(inBuffer,offset+1,length,outBuffer);
		return 1;
	}
	return 0;
}

// buffer write (return -1 on failure)
int attribute::writeByte(char *outbuffer, int offset, int attrType, void *data)
{
	if (outbuffer != NULL && data != NULL) {
		if (attrType == DB_TYPE_INT) {
			//int tmp = *(int *)data;
			memcpy(outbuffer + offset, data, sizeof(int));
			// TODO: do we want to hard code this to 4?
			return sizeof(int);
		}

		if (attrType == DB_TYPE_SMALLINT) {
			//short int tmp = (short)atoi(data);
			memcpy(outbuffer+offset, data, sizeof(short int));
			return sizeof(short int);
		}

		if (attrType == DB_TYPE_STRING || attrType == DB_TYPE_CHAR) {
			// direct copy of the string
			size_t len = strlen(static_cast<const char *>(data));
			outbuffer[offset] = (char)len;
			memcpy(outbuffer+offset+1,data,len);
			return (int)len+1;
		}



		// TODO: Add support for more types
		printf("Type %d not supported.\n", attrType);
	}
	return -1;
}

// attribute byte size
// returns the size of an attribute or -1 on failure
//int attribute::getByteSize(int attrType, void *data);

}
