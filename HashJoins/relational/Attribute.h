#ifndef DB_ATTRIBUTE_H
#define DB_ATTRIBUTE_H

#include <string>


namespace db 
{

	typedef enum 
	{
		DB_TYPE_INVALID = 0,
		DB_TYPE_CHAR = 1,
		DB_TYPE_DECIMAL = 3,
		DB_TYPE_INT = 4,
		DB_TYPE_SMALLINT = 5,
		DB_TYPE_FLOAT = 6,
		DB_TYPE_DOUBLE = 8,
		DB_TYPE_STRING = 12,
		DB_TYPE_DATE = 91,
		DB_TYPE_TIMESTAMP = 93,
		DB_TYPE_BLOB = 2004
	} DB_TYPE;

	class Attribute
	{
	public:
		Attribute(std::string name, DB_TYPE type, int length);
		~Attribute(void);

		std::string getName() const;
		DB_TYPE getType() const;
		void setType(DB_TYPE type);
		int getLength() const;
	private:
		std::string name;
		DB_TYPE type;
		int  length;
	};

	namespace attribute {
		// file reads (return 0 on failure)
		int readFileInt(FILE *fp, int *out);
		int readFileSmallInt(FILE *fp, short int *out);

		// expects outBuffer to be 256 bytes
		int readFileString(FILE *fp, char *outBuffer);

		// file writes (return 0 on failure)
		int writeFile(FILE *fp, int attrType, void *data);

		// buffer reads 
		int readByteInt(char *inBuffer, int offset);
		short int readByteSmallInt(char *inBuffer, int offset);

		// expects outBuffer to be 256 bytes
		int readByteString(char *inBuffer, int offset, char *outBuffer);

		// buffer write (return 0 on failure)
		int writeByte(char *outbuffer, int offset, int attrType, void *data);

		// attribute byte size
		// returns the size of an attribute or 0 on failure
		int getByteSize(int attrType, void *data);
	}
}

#endif // DB_ATTRIBUTE_H
