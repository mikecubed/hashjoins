#include "Tuple.h"

#include "Attribute.h"
#include "../util/convert.h"
#include <cmath>
#include <cassert>

namespace db
{
	Tuple::Tuple(std::shared_ptr<Relation> r)
		:relation(r),
		data(nullptr),
		buffer(nullptr),
		firstOffset(DB_TUPLE_FIRST_OFFSET),
		maxDataSize(r->computeTupleSize()),
		ownsMemory(false)
	{
	}

	Tuple::Tuple(std::shared_ptr<Relation> r, char *data) // use data for tuple to avoid an allocation
		:relation(r),
		data(data),
		buffer(nullptr),
		firstOffset(DB_TUPLE_FIRST_OFFSET),
		maxDataSize(r->computeTupleSize()),
		ownsMemory(false)
	{
	}

	// copy constructor
	Tuple::Tuple(const Tuple &lval)
		:relation(lval.relation),
		data(lval.data),
		ownsMemory(lval.ownsMemory),
		buffer(nullptr),
		firstOffset(lval.firstOffset),
		maxDataSize(lval.maxDataSize)
	{
		// if the copied tuple owns its memory we need to copy it
		if (ownsMemory && lval.data)
		{
			setData(new char [maxDataSize], true);

			memcpy(data, lval.data, maxDataSize);
		}
	}

	// move constructor
	Tuple::Tuple(Tuple &&rvalue)
		:relation(rvalue.relation),
		data(rvalue.data),
		ownsMemory(rvalue.ownsMemory),
		buffer(rvalue.buffer),
		firstOffset(rvalue.firstOffset),
		maxDataSize(rvalue.maxDataSize)
	{
		if (rvalue.ownsMemory)
		{
			rvalue.data = nullptr;
			rvalue.ownsMemory = false;
		}

		rvalue.buffer = nullptr;
	}

	Tuple::~Tuple(void)
	{
		if (data && ownsMemory)
			delete data;

		data = nullptr;

		if (buffer)
			delete buffer;

		buffer = nullptr;
	}

	int Tuple::getInt(std::size_t index) const
	{
		if (data)
			return attribute::readByteInt(data, getOffset(index));

		return 0; // this shouldn't happen
	}

	char *Tuple::getData()
	{
		return data;
	}

	std::string Tuple::getString(std::size_t index) const
	{
		if (data)
		{
			char outBuffer[1024];
			attribute::readByteString(data, getOffset(index), outBuffer);

			return std::string(outBuffer);
		}

		return "";
	}

	std::size_t Tuple::computeMaxTupleSize(std::shared_ptr<Relation> rel) const
	{
		// FIXME: should we use first offset or the constant?
		return rel->computeTupleSize() + (2 * rel->getNumAttributes()) + DB_TUPLE_FIRST_OFFSET;
		//return rel.computeTupleSize() + (2 * rel.getNumAttributes()) + firstOffset;
	}

	std::shared_ptr<Tuple> Tuple::join(const std::deque<std::shared_ptr<Tuple>>& tuples, std::shared_ptr<Relation> rel, std::shared_ptr<Tuple> outTuple)
	{
		std::vector<std::shared_ptr<Tuple>> tvec;
		tvec.insert(tvec.begin(),tuples.begin(), tuples.end());
		return Tuple::join(tvec, rel, outTuple);
	}

	std::shared_ptr<Tuple> Tuple::join(const std::vector<std::shared_ptr<Tuple>>& tuples, std::shared_ptr<Relation> rel, std::shared_ptr<Tuple> outTuple)
	{
		if (!outTuple)
			outTuple = std::shared_ptr<Tuple> (new Tuple(rel));
		
		std::size_t totalSize = 0, numFields = 0;
		for (std::shared_ptr<Tuple> t : tuples)
		{
			totalSize += t->getDataSize();
		}

		numFields = rel->getNumAttributes();

		totalSize += (numFields * 2) + DB_TUPLE_FIRST_OFFSET;


		if (static_cast<std::size_t>(outTuple->maxDataSize) < totalSize || outTuple->data == nullptr)
			outTuple->setData(new char[totalSize], true);
		
		outTuple->data[0] = 0;
		outTuple->data[1] = static_cast<char>(numFields);
		db::shortToByteOffset(static_cast<short>(totalSize), outTuple->data, 2);
		outTuple->firstOffset = 4;

		std::size_t tFirstData = outTuple->firstOffset + (2 * numFields);
		std::size_t firstIndex = 0, curDataLength = 0, outAttribIndex = 0, tupleLength = 0;
		for (std::shared_ptr<Tuple> t : tuples)
		{
			firstIndex = tFirstData + curDataLength;
			std::size_t tOffset0 = t->getOffset(0);
			tupleLength = t->getDataSize();

			assert(tOffset0 >= 0 && tOffset0 < tupleLength && t->data[1] > 0);

			for (int curAttribIndex = 0; curAttribIndex < t->data[1]; ++curAttribIndex) { 
				if (t->isNull(curAttribIndex)) {
					outTuple->setIsNull(outTuple->firstOffset + (outAttribIndex*2));
				} else {
					short int offset = (short int) (firstIndex + t->getOffset(curAttribIndex) - tOffset0);
					assert(offset >= 0 && tOffset0 < tupleLength + firstIndex);

					db::shortToByteOffset(offset,outTuple->data, outTuple->firstOffset + (outAttribIndex * 2));
				}
				outAttribIndex++;
			}

			memcpy(&outTuple->data[firstIndex], &t->data[tOffset0], tupleLength);
			curDataLength += tupleLength;
		}
		return outTuple;
	}

	std::size_t Tuple::getSize() const
	{
		return byteToShortOffset(data, 2);
	}

	std::size_t Tuple::getDataSize() const
	{
		if (data)
			return db::byteToShortOffset(data, 2) - firstOffset;
		return 0;
	}

	// TODO: db_tuple_filterValues

	std::size_t Tuple::numValues() const
	{
		if (data)
			return data[1];
		return 0;
	}

	std::shared_ptr<Relation> Tuple::getRelation() const
	{
		return relation;
	}

	unsigned short int Tuple::getFirstOffset() const
	{
		if (data)
			return byteToShortOffset(data, firstOffset);
		return 0;
	}

	unsigned short int Tuple::getOffset(std::size_t index) const
	{
		if (data)
			return byteToShortOffset(data, (firstOffset + index*2));
		return 0;
	}

	void Tuple::setIsNull(std::size_t index)  // NOTE: setIsNull deviates from the java version
	{
		if (data)
			shortToByteOffset(0, data, DB_TUPLE_FIRST_OFFSET + 2*index);
	}

	void Tuple::setOffset(std::size_t index, unsigned int offsetVal)
	{
		if (data)
		{
			// Determine correct offset
			int count = 0;		
			for (unsigned int c=0; c < index; c++)
				if (!isNull(c))
					count++;

			int offsetData = (int) (4+ceil(data[1]/8.0)+count*2);
			data[offsetData] = (char) (offsetVal/256);
			data[offsetData+1] = (char) (offsetVal%256);
		}
	}

	bool Tuple::isNull(std::size_t index) const
	{
		if (data)
		{
			if (byteToShortOffset(data, DB_TUPLE_FIRST_OFFSET + 2*index) == 0)
			{
				return true;
			}
		}
		return false;

	}

	bool Tuple::operator==(const Tuple& rhs) const
	{
		// TODO: implement operator== on relation
		if (relation != rhs.relation)
			return false;

		std::size_t numVals = numValues();
		if (numVals != rhs.numValues())
			return false;

		for (std::size_t idx = 0; idx < numVals; ++idx)
		{
			bool curNull = isNull(idx);
			if (curNull != rhs.isNull(idx))
				return false;

			if (!curNull)
			{
				switch (relation->getAttributeType(idx))
				{
				case DB_TYPE_INT:
					if (getInt(idx) != rhs.getInt(idx))
						return false;
					break;
				case DB_TYPE_STRING:
					if (getString(idx) != rhs.getString(idx))
						return false;
					break;
					// TODO: other data types
				default:
					break;
				}
			}
		}

		return true;
	}

	void Tuple::setData(char *newData, bool takeOwnership /* = false */)
	{
		if (data && ownsMemory)
		{
			delete[] data;
			data = nullptr;
		}

		data = newData;

		ownsMemory = (data!= nullptr && takeOwnership);

	}

	// TODO: other accessors

	// I/O Functions

	//FIXME: How are NULL attributes represented in the text file?
	//FIXME: Current code assumes the data array is preallocated
	int Tuple::readText(FILE *fp, const char *tokens)
	{
		if (data != nullptr && fp != nullptr)
		{
			char *tok = nullptr;

			short num_attr = (short)relation->getNumAttributes();
			int buff_size = DB_TUPLE_BUFFER_SIZE;

			if (buffer == nullptr) {
				buffer = new char[buff_size];
			}

			if(buffer != nullptr && (fgets(buffer, static_cast<int>(maxDataSize), fp) == buffer)) {

				short int currOffset = (short)(4+(2*num_attr)), idx = 0, totalSize = (short)(4 + 2*num_attr);
				data[0] = 0;
				data[1] = (char)num_attr;
				firstOffset = DB_TUPLE_FIRST_OFFSET;

				// process the tokens
				if (tokens == NULL)
					tokens = "| \t\n\r\f"; // default tokens
				
				// tokenize the string
				tok = strtok(buffer,tokens);

				while (tok != NULL) {
					// convert token to data
					DB_TYPE attrType = relation->getAttributeType(idx);

					if (attrType == DB_TYPE_INVALID) {
						fprintf(stderr, "there are too many tokens, the relation does not match the input?\n");
						break;
					}

					// write the offset into the array
					shortToByteOffset(currOffset,data,firstOffset+(2*idx));

					if (attrType == DB_TYPE_INT) {
						int val = atoi(tok);
						totalSize += 4;
						attribute::writeByte(data,currOffset,attrType,&val);
						currOffset += 4;
					} else {
						// FIXME: Java code currently only supports INT type and treats everything else as a string
						short attSize = (short)strlen(tok) + 1;
						totalSize = totalSize + attSize;
						attribute::writeByte(data,currOffset,attrType,tok);
						currOffset = currOffset + attSize;
					}
					++idx;
					// get the next token
					tok = strtok(NULL, tokens);
				}
				// now write data length into the buffer
				shortToByteOffset(totalSize, data, 2);
				return 1;
			}
		}
		return 0;
	}

	int Tuple::read(FILE *fp)
	{
		if (fp != NULL) {
			char tmp[4];
			int recordSize;
			size_t val = fread(tmp,1,4,fp);

			if (val != 4) {
				fprintf(stderr,"db_tuple_read unable to read recordSize from disk\n");
				return 0;
			}

			// TODO: Shouldn't we be taking the 3rd and 4th byte as a short instead ? (is this way faster?)
			recordSize = byteToInt(tmp) % 65536;

			if(data == nullptr)  {
				data = new char[recordSize];
				ownsMemory = true;
			}

			if (data == nullptr) {
				fprintf(stderr,"db_tuple_read data pointer is NULL\n");
				return 0;
			}
			memcpy(data,tmp,4);

			// read in the rest of the tuple
			val = fread(data + 4, 1, recordSize-4,fp);

			if ((int)val != recordSize-4) {
				fprintf(stderr, "db_tuple_read unable to read all data from disk\n");
				return 0;
			}

			firstOffset = DB_TUPLE_FIRST_OFFSET;
			return 1;
		}
		return 0;
	}

	int Tuple::write(FILE *fp)
	{
		if (data && fp) {
			size_t cnt = fwrite(data,1,byteToShortOffset(data,2),fp);
			if ((int)cnt != byteToShortOffset(data, 2)) {
				fprintf(stderr,"db_tuple_write unable to write all the data to disk\n");
				return 0;
			}
			return (int)cnt;
		}
		return 0;
	}

	// not implemented
	int Tuple::writeText(FILE *fp)
	{
		if (data && fp) {
			char outputtext[1024];

			std::size_t numAttrs = relation->getNumAttributes();

			for (std::size_t ii = 0; ii < numAttrs; ++ii)
			{
				const char *separtator = "|";

				if (ii == numAttrs - 1)
					separtator = "";

				DB_TYPE attrType = relation->getAttributeType(ii);
				switch (attrType)
				{
				case DB_TYPE_INT:
					{
						int curInt = getInt(ii);
						sprintf(outputtext, "%d%s", curInt,separtator);
						break;
					}
				case DB_TYPE_STRING:
					{
						std::string curString = getString(ii);
						sprintf(outputtext, "%s%s", curString.c_str(), separtator);
						break;
					}
				default:
					outputtext[0] = '|';
					outputtext[1] = '\0';
					break;
				}

				fwrite(outputtext, strlen(outputtext), 1, fp);
			}
			fwrite("\n",strlen("\n"),1,fp);


			return 1;
		}
		
		return 0; 
	}

}
