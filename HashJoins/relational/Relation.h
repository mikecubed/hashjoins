#ifndef DB_RELATION_H
#define DB_RELATION_H

#include <vector>
#include <memory>

#include "Attribute.h"

namespace db
{
	class Relation
	{
	public:
		Relation();
		Relation(const std::vector<Attribute> &attribs, unsigned int tupleEstimate = 0, unsigned long long relationByteSize = 0);
		Relation(const Relation &rel);
		Relation(const Relation &l, const Relation& r);
		Relation(const std::vector<std::shared_ptr<Relation>>& rels);

		~Relation(void);

		// getter/setters
		DB_TYPE getAttributeType(std::size_t index) const;
		void setAttributeType(std::size_t index, DB_TYPE type);
		std::size_t getNumAttributes() const;

		// TODO: do this a better way
		const Attribute * const getAttribute(std::size_t index) const;
		void setAttribute(std::size_t index, const Attribute &a);

		std::size_t computeTupleSize() const;

		unsigned int getTupleEstimate();		// Estimated number of tuples in the relation
		unsigned long long getRelationByteSize();		// Estimated size in bytes of the relation

	private:
		std::vector<Attribute> attributes;
		unsigned int tupleEstimate;
		unsigned long long relationByteSize;
	};
}

#endif // DB_RELATION_H