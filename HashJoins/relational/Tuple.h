#ifndef DB_TUPLE_H
#define DB_TUPLE_H

#include <vector>
#include <deque>
#include <memory>

#include "Relation.h"

namespace db
{
	//defines
	#define DB_TUPLE_FIRST_OFFSET 4
	#define DB_TUPLE_BUFFER_SIZE 4096

	/*
 * Tuple format:
 * 1 byte 	- header
 * 1 byte 	- number of fields
 * 2 bytes	- size of data (in bytes)
 * offset array (2 bytes per field (short value)) (if field is null, offset is 0)
 * fields encoded in bytes
 */

	class Tuple
	{
	public:
		Tuple(std::shared_ptr<Relation> r);
		Tuple(std::shared_ptr<Relation> r, char *data); // use data for tuple to avoid an allocation
		Tuple(const Tuple& tuple);
		Tuple(Tuple &&rvalue);

		~Tuple(void);

		int getInt(std::size_t index) const;
		std::string getString(std::size_t index) const;
		std::size_t computeMaxTupleSize(std::shared_ptr<Relation> rel) const;
		
		static std::shared_ptr<Tuple> join(const std::vector<std::shared_ptr<Tuple>>& tuples, std::shared_ptr<Relation> rel, std::shared_ptr<Tuple> outTuple = nullptr);
		static std::shared_ptr<Tuple> join(const std::deque<std::shared_ptr<Tuple>>& tuples, std::shared_ptr<Relation> rel, std::shared_ptr<Tuple> outTuple = nullptr);

		std::size_t getSize() const;
		std::size_t getDataSize() const;

		// TODO: db_tuple_filterValues

		std::size_t numValues() const;
		std::shared_ptr<Relation> getRelation() const;
		unsigned short int getFirstOffset() const;
		unsigned short int getOffset(std::size_t index) const;
		void setIsNull(std::size_t index);  // NOTE: setIsNull deviates from the java version
		void setOffset(std::size_t index, unsigned int offsetVal);

		bool isNull(std::size_t index) const;

		bool operator==(const Tuple& rhs) const; 
		// TODO: other accessors

		// I/O Functions
		int readText(FILE *fp, const char *tokens);
		int read(FILE *fp);

		int write(FILE *fp);
		int writeText(FILE *fp);

		friend class Page;
		//friend class HashBucket;

		char *getData();
		void setData(char *data, bool takeOwnership = false);

		int getHash() { return hash; }
		void setHash(int inHash) { hash = inHash; }
		int hash;

	private:
		char *data;				// Data as a byte array
		std::size_t maxDataSize;		// we need to keep track of the maximum size for the tuple data
		std::shared_ptr<Relation> relation;		// Relational Schema describing tuple format
		unsigned char firstOffset;
		char *buffer;			// buffer for reading text
		bool ownsMemory;		// if we own the memory we need to delete it.
		
	};
}
#endif // DB_TUPLE_H
