#include "relational/Attribute.h"
#include "relational/Relation.h"
#include "relational/Tuple.h"
#include "operators/PageFileScan.h"
#include "operators/DynamicHashJoin.h"
#include "operators/HashTeamJoin.h"
#include "operators/GeneralizedHashTeamJoin.h"
#include "operators/SHARPJoin.h"
#include "predicates/EquijoinPredicate.h"
#include "io/FileManager.h"
#include "util/TextToPage.h"
#include "Joins.h"

#include <vector>
#include <memory>

using namespace std;
using namespace db;

void convertCustomer(const std::string& inFile, const std::string& outFile)
{
	vector<Attribute> attrs1 = getCustomerAttrs();
	convertTextFile(inFile, outFile, attrs1);
}

void convertOrders(const std::string& inFile, const std::string& outFile)
{
	vector<Attribute> attrs1 = getOrderAttrs();
	convertTextFile(inFile,outFile,attrs1);
}

void convertlineitem(const std::string& inFile, const std::string& outFile)
{
	vector<Attribute> attrs1 = getLineitemAttrs();
	convertTextFile(inFile,outFile,attrs1);
}

int countLineitem(const std::string& inFile)
{
	vector<Attribute> attrs1 = getLineitemAttrs();
	shared_ptr<Relation> r1( new Relation(attrs1));

	TextToPage ttp(inFile, "", r1);

	printf("Counting tuples in %s\n", inFile.c_str() );

	int tups = ttp.count();

	printf("%s has %d lines.\n", inFile.c_str(), tups);
	return tups;
}

void convertPartSupp(const std::string& inFile, const std::string& outFile)
{
	vector<Attribute> attrs1 = getPartSuppAttrs();
	convertTextFile(inFile,outFile,attrs1);
}

int countPartSupp(const std::string& inFile)
{
	vector<Attribute> attrs1 = getPartSuppAttrs();
	shared_ptr<Relation> r1( new Relation(attrs1));

	TextToPage ttp(inFile, "", r1);

	printf("Counting tuples in %s\n", inFile.c_str() );

	int tups = ttp.count();

	printf("%s has %d lines.\n", inFile.c_str(), tups);
	return tups;
}

void convertPart(const std::string& inFile, const std::string& outFile)
{
	vector<Attribute> attrs = getPartAttrs();
	convertTextFile(inFile,outFile,attrs);
}

void testOOOJoins(int numberJoinIterations, TPCHSize size)
{
	int memFactor = 1;

	if (size == TPCHSize1G)
		memFactor = 10;
	else if (size == TPCHSize10G)
		memFactor = 100;

	int maxSize = 12000 * memFactor;
	int skipSize = 12000 * memFactor;

	//printf("Joining Order, Order and Order:\n");
	for (int memSize = maxSize; memSize > 0; memSize -= skipSize)
		for (int ii = 0; ii  < numberJoinIterations; ++ii)
			testOOO(memSize, 20, size);
		
	//printf("Joining Order, Order and Order right deep plan:\n");
	//for (int memSize = maxSize; memSize > 0; memSize -= skipSize)
	//	for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//		testOOO_rightDeep(memSize, 20, size);
	//

	//// double up for HT
	//maxSize *= 2;
	//skipSize *= 2;
	//printf("HT Joining Order, Order and Order HTJ:\n");
	//for (int memSize = maxSize; memSize > 0; memSize -= skipSize)
	//	for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//		testOOO_HTJ(memSize, 20, size);
}

void testCOLJoins(int numberJoinIterations, TPCHSize size)
{
	int memFactor = 1;

	if (size == TPCHSize1G)
		memFactor = 10;
	else if (size == TPCHSize10G)
		memFactor = 100;

	int maxSize = 1000 * memFactor;
	int skipSize = 100 * memFactor;

	////printf("Joining Customer, Order and Lineitem:\n");
	for (int memSize = maxSize; memSize > 0; memSize -= skipSize)
		for (int ii = 0; ii  < numberJoinIterations; ++ii)
			testCOL(memSize, 20, size);

	////printf("Joining Customer, Order and Lineitem:\n");
	for (int memSize = maxSize; memSize > 0; memSize -= skipSize)
		for (int ii = 0; ii  < numberJoinIterations; ++ii)
			testCOL_rightDeep(memSize, 20, size);
	// double up for GHT
	maxSize *= 2;
	skipSize *= 2;
	//printf("GHT Joining Customer, Order and Lineitem:\n");
	for (int memSize = maxSize; memSize > 0; memSize -= skipSize)
		for (int ii = 0; ii  < numberJoinIterations; ++ii)
			testCOL_GHT(memSize, 20, size);
}

void testOPLJoins(int numberJoinIterations, TPCHSize size)
{
	int memFactor = 1;

	if (size == TPCHSize1G)
		memFactor = 10;
	else if (size == TPCHSize10G)
		memFactor = 100;

	int maxSize = 6000 * memFactor;
	int skipSize = 1000 * memFactor;

	//printf("Joining Order, Part and Lineitem:\n");
	//for (int memSize = maxSize; memSize > 0; memSize -= skipSize)
	//	for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//		testOPL_DHJ(memSize, 20, size);

	// double up for GHT
	maxSize *= 2;
	skipSize *= 2;
	//printf("SHARP Joining Order, Part and Lineitem:\n");
	for (int memSize = maxSize; memSize > 0; memSize -= skipSize)
		for (int ii = 0; ii  < numberJoinIterations; ++ii)
			testOPL_SHARP(memSize, 20, size);
}

void testOOOJoins(int algo, TPCHSize size, int memorysize)
{
	int memFactor = 1;

	if (size == TPCHSize1G)
		memFactor = 10;
	else if (size == TPCHSize10G)
		memFactor = 100;

	int memSize = memorysize * memFactor;

	if (algo == 0) 
	{
		//DHJ L
		testOOO(memSize, 20, size);
	} else if (algo == 1) {
		//DHJ R
		testOOO_rightDeep(memSize, 20, size);
	} else if (algo == 2) {
		// hashteams
		testOOO_HTJ(memSize * 2, 20, size);
	}	
}

void testCOLJoins(int algo, TPCHSize size, int memorysize)
{
	int memFactor = 1;

	if (size == TPCHSize1G)
		memFactor = 10;
	else if (size == TPCHSize10G)
		memFactor = 100;

	int memSize = memorysize * memFactor;

	if (algo == 0) 
	{
		//DHJ L
		testCOL(memSize, 20, size);
	} else if (algo == 1) {
		//DHJ R
		testCOL_rightDeep(memSize, 20, size);
	} else if (algo == 2) {
		// gen hashteams
		testCOL_GHT(memSize*2, 20, size);
	}	
}

void testOPLJoins(int algo, TPCHSize size, int memorysize)
{
	int memFactor = 1;

	if (size == TPCHSize1G)
		memFactor = 10;
	else if (size == TPCHSize10G)
		memFactor = 100;

	int memSize = memorysize * memFactor;

	if (algo == 0) 
	{
		//DHJ L
		testOPL_DHJ(memSize, 20, size);
	} else if (algo == 1) {
		//DHJ R
		return;
	} else if (algo == 2) {
		// gen hashteams
		testOPL_SHARP(memSize*2, 20, size);
	}
}

void testPPSLJoins(int algo, TPCHSize size, int memorysize)
{
	int memFactor = 1;

	if (size == TPCHSize1G)
		memFactor = 10;
	else if (size == TPCHSize10G)
		memFactor = 100;

	int memSize = memorysize * memFactor;

	if (algo == 0) 
	{
		//DHJ L
		testPPSL_DHJ(memSize, 160, size);
	} else if (algo == 1) {
		//DHJ R
		return;
	} else if (algo == 2) {
		// hashteams
		//testPPSL_SHARP(memSize*2, 20, size);
	}
}

void testOPSLJoins(int algo, TPCHSize size, int memorysize)
{
	int memFactor = 1;

	if (size == TPCHSize1G)
		memFactor = 10;
	else if (size == TPCHSize10G)
		memFactor = 100;

	int memSize = memorysize * memFactor;

	if (algo == 0) 
	{
		//DHJ L
		testOPSL_DHJ(memSize, 20, size);
	} else if (algo == 1) {
		//DHJ R
		return;
	} else if (algo == 2) {
		// derp
		//testOPSL_SHARP(memSize*2, 20, size);
	}
}

void testOOLJoins(int algo, TPCHSize size, int memorysize)
{
	int memFactor = 1;

	if (size == TPCHSize1G)
		memFactor = 10;
	else if (size == TPCHSize10G)
		memFactor = 100;

	int memSize = memorysize * memFactor;

	if (algo == 0) 
	{
		//DHJ L
		testOOL_DHJ(memSize, 20, size);
	} else if (algo == 1) {
		//DHJ R
		return;
	} else if (algo == 2) {
		// gen hashteams
		testOOL_SHARP(memSize*2, 20, size);
	}
}

int main(int argc, char **argv)
{
	//int count = countLineitem("data/input/lineitem_pg.tbl");
	//printf("lineitem: %d", count);
	//return 0;
//	
//	if (argc < 4)
//		return -1;
//
//	int algo = atoi(argv[1]);
//	int dbsize = atoi(argv[2]);
//	int memsize = atoi(argv[3]);
//
//	TPCHSize size = TPCHSize100M;
//	if (dbsize>1)
//		size = TPCHSize10G;
//	else if (dbsize > 0)
//		size = TPCHSize1G;
//	//testOPLJoins(2,TPCHSize100M,12000);
//	//testPPSLJoins(0,TPCHSize1G, 10000);
//
//	//testOL_DHJ(1200000,20,TPCHSize10G);
//	
//	//testOOLJoins(2,TPCHSize1G, 12000);
//
//	//return 0;
//	if (algo >= 0 && algo < 3 &&
//		dbsize >= 0 && dbsize < 3 &&
//		memsize > 99 && memsize < 1200100)
//	{
//		//testOOOJoins(algo,size,memsize);
//		//testCOLJoins(algo,size,memsize);
//		//testOPLJoins(algo,size,memsize);
////		testPPSLJoins(algo,size, memsize);
//		testPSL_DHJ(memsize,400,TPCHSize1G);
//		//testOOLJoins(algo,size,memsize);
//		return 0;
//	}
//
	testPSL_DHJ(10000,20,TPCHSize1G);

	//testPPSLJoins(0,TPCHSize1G, 8000);
	//testPPSLJoins(0,TPCHSize1G, 1000);
	
	return -1;
	//convertCustomer("data/input/customer_pg.tbl","data/input/customer.page");
	//convertPart("data/input/part_pg.tbl", "data/input/part.page");
	//convertOrders("data/input/order_pg.tbl", "data/input/order.page");
	//convertlineitem("data/input/lineitem_pg.tbl", "data/input/lineitem.page");
	//convertPartSupp("data/input/partsupp_pg.tbl", "data/input/partsupp.page");
	//return 0;
	//return 0;
	//scanOrder();
	//printf("Joining Customer and Order:\n");
	//testCO();

	//printf("\nNow Joining Order and Lineitem:\n");
	//testOL();

//	printf("\nNow Joining Customer, Order, and Lineitem:\n");
//	testCOL();

	//printf("HT Joining Customer and Order:\n");
	//testCO_HTJ();

	//int numberJoinIterations = 10;

	//printf("DHJ Joining Customer, Customer and Customer:\n");
	//testCCC_DHJ(250,30);
	//testCCC_DHJ(250,30);
	//testCCC_DHJ(250,30);
	//testCCC_DHJ(250,30);
	//testCCC_DHJ(250,30);

	//printf("\nHT Joining Customer, Customer and Customer:\n");
	/*testCCC_HTJ(500,30);
	testCCC_HTJ(500,30);
	testCCC_HTJ(500,30);
	testCCC_HTJ(500,30);
	testCCC_HTJ(500,30);
*/
	//printf("Joining Order, Order and Order:\n");
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO(12000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO(10000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO(8000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO(6000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO(4000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO(2000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO(1000,30);




	//printf("Joining Order, Order and Order right deep plan:\n");
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO_rightDeep(12000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO_rightDeep(10000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO_rightDeep(8000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO_rightDeep(6000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO_rightDeep(4000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO_rightDeep(2000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO_rightDeep(1000,30);
	//
	//printf("HT Joining Order, Order and Order HTJ:\n");
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO_HTJ(12000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO_HTJ(10000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO_HTJ(8000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO_HTJ(6000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO_HTJ(4000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO_HTJ(2000,30);
	//for (int ii = 0; ii  < numberJoinIterations; ++ii)
	//	testOOO_HTJ(1000,30);

	testOOOJoins(3, TPCHSize10G);
	//testCOLJoins(2, TPCHSize1G);

	//testOPLJoins(10,TPCHSize1G);

	//printf("\nHT Joining Customer, Customer and Order:\n");
	//testCOL_GHT();

//	printf("\n Joining order, part, and lineitem:\n");
//	testOPL_DHJ();

//	printf("\n SHARP Joining order, part, and lineitem:\n");
//	testOPL_SHARP();

	return 0;
//	convertTextFile("data/input/input2.txt", "data/input/input2.page");
//	convertTextFile("data/input/input3.txt", "data/input/input3.page");
}
